package CSCI201_FinalProject_Scrabble;

import java.awt.Graphics;
import java.awt.Image;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import javax.imageio.ImageIO;
import javax.swing.ImageIcon;

//ONE INSTANCE OF SERVER THREAD RUNS FOR EACH CLIENT THAT CONNECTS
public class ServerThread extends Thread{

	//DATABASE CONNECTION
	private Connection conn;
	//SERVER THREAD COMPONENTS
	protected Socket socket;
	private Server server;
	protected ObjectInputStream serverInputStream;
	protected ObjectOutputStream serverOutputStream;
	private NetworkingMessage message;
	
	//SERVER THREAD CONSTRUCTOR
	public ServerThread(Socket socket, Server server) {
		try {
			//CONNECT TO DATABASE
			conn = null;
			try {
				Class.forName("com.mysql.jdbc.Driver");
				conn = DriverManager.getConnection("jdbc:mysql://localhost/scrabble?user=root&password=root&useSSL=false");
			} catch (SQLException sqle) {
				System.out.println ("SQLException: " + sqle.getMessage());
			} catch (ClassNotFoundException cnfe) {
				System.out.println("cnfe: " + cnfe.getMessage());
			}
			//INSTANTIATE SERVER THREAD COMPONENTS
			this.socket = socket;
			this.server = server;
			serverOutputStream = new ObjectOutputStream(this.socket.getOutputStream());
			serverInputStream = new ObjectInputStream(this.socket.getInputStream());
			this.start();
		} catch (IOException ioe) {
			System.out.println("ioe: " + ioe.getMessage());
		}
	}
	
	//INFINITE LOOP
	public void run() {	
		try {
			while(true) {
				message = (NetworkingMessage)serverInputStream.readObject();
				//System.out.println(message.getMessage());//FOR TESTING PURPOSES
				
				//LOGIN MESSAGE
				if (message.getMessageType().equals("login")){
					String[] parts = message.getMessage().split("\\|");
					String username = parts[0];
					String password = parts[1];
					
					//TELL SERVER GUI OF LOGIN MESSAGE ATTEMPT
					server.serverGUI.addMessage("Log-in attempt User: " + username + " Pass: " + password);
					
					try {
						//CHECK IF LOGIN CREDENTIALS ARE VALID
						PreparedStatement ps = conn.prepareStatement("SELECT * FROM Users WHERE username =? AND userpassword = ?");
						ps.setString(1,  username);
						ps.setString(2, password);
						ResultSet result = ps.executeQuery();
						
						while (result.next()){
							//LOGIN VALID
							boolean accountvalidated = result.getBoolean("accountvalidated");
							String firstname = result.getString("firstname");
							String lastname = result.getString("lastname");
							sendMessage(new NetworkingMessage("login", username + "|success|" + accountvalidated + "|" + firstname + "|" + lastname));
							server.serverGUI.addMessage("Log-in success User: " + username);
							result.close();
							return;
						}
						
						//LOGIN INVALID
						sendMessage(new NetworkingMessage("login", username + "|failure"));
						server.serverGUI.addMessage("Log-in failure User: " + username);
						result.close();
						return;
					} catch(SQLException sqle) {
						System.out.println("sqle: " + sqle.getMessage());
					} 		
				}
				//REGISTER MESSAGE
				else if (message.getMessageType().equals("register")){
					String[] parts = message.getMessage().split("\\|");
					String firstname = parts[0];
					String lastname = parts[1];
					String username = parts[2];
					String password = parts[3];
					boolean accountvalidated = false;
					String validationPassphrase = parts[4];
					String profilefilepath = "users\\Default.png";
					
					//TELL SERVER GUI OF REGISTER MESSAGE ATTEMPT
					server.serverGUI.addMessage("Register attempt User: " + firstname + " " + lastname + " Username: " + username + " Pass: " + password);
					
					try {						
						//CHECK IF USERNAME ALREADY EXISTS
						PreparedStatement ps = conn.prepareStatement("SELECT * FROM Users WHERE username =?");
						ps.setString(1,  username);
						ResultSet result = ps.executeQuery();
						
						while (result.next()){
							//REGISTER FAILURE
							sendMessage(new NetworkingMessage("register", username + "|failure|username exists"));
							server.serverGUI.addMessage("Register failure User: " + username + " already exists");
							result.close();
							return;
						}
						
						//TRY TO SEND VALIDATION PASSPHRASE EMAIL TO USER
						EmailSender mailer = new EmailSender();
				        boolean emailResult = mailer.sendEmailToUser(firstname, lastname, username, validationPassphrase);
						if (emailResult == false){
							//REGISTER FAILURE
							sendMessage(new NetworkingMessage("register", username + "|failure|email failure"));
							server.serverGUI.addMessage("Register failure User: " + username + " is not a valid email address and/or the user's machine is not connected to the internet.");
							result.close();
							return;
						}
						
						//INSERT USER INTO DATABASE
						ps = conn.prepareStatement("INSERT INTO Users(firstname, lastname, username, userpassword, accountvalidated, validationpassphrase, profilefilepath) "
								+ "VALUES(?,?,?,?,?,?,?)");
						ps.setString(1, firstname);
						ps.setString(2, lastname);
						ps.setString(3, username);
						ps.setString(4, password);
						ps.setBoolean(5, accountvalidated);
						ps.setString(6, validationPassphrase);
						ps.setString(7, profilefilepath);
						ps.executeUpdate();
						
						//CHECK IF USER WAS ADDED
						ps = conn.prepareStatement("SELECT * FROM Users WHERE username =?");
						ps.setString(1,  username);
						result = ps.executeQuery();
						
						while (result.next()){
							sendMessage(new NetworkingMessage("register", username + "|success|" + firstname + "|" + lastname));
							server.serverGUI.addMessage("Register success User: " + username);
							result.close();
							return;
						}
					} catch(SQLException sqle) {
						System.out.println("sqle: " + sqle.getMessage());
					} 
				}
				//VALIDATE MESSAGE
				else if (message.getMessageType().equals("validate")){
					String[] parts = message.getMessage().split("\\|");
					String username = parts[0];
					String passphrase = parts[1];
					String actualpassphrase = "";
					
					//TELL SERVER GUI OF VALIDATE MESSAGE ATTEMPT
					server.serverGUI.addMessage("Validate attempt User: " + username + " Passphrase: " + passphrase);
					
					try {						
						//GET CORRECT PASSPHRASE
						PreparedStatement ps = conn.prepareStatement("SELECT * FROM Users WHERE username =?");
						ps.setString(1,  username);
						ResultSet result = ps.executeQuery();
						
						while (result.next()){
							actualpassphrase = result.getString("validationpassphrase");
						}
						 
						//CHECK IF VALIDATION PASSPHRASE IS CORRECT
						if (actualpassphrase.equals(passphrase)){
							//UPDATE DATABASE
							ps = conn.prepareStatement("UPDATE Users SET accountvalidated = ? WHERE username = ?");
							ps.setBoolean(1, true);
							ps.setString(2, username);
							ps.executeUpdate();
							//SEND SUCCESS MESSAGE
							sendMessage(new NetworkingMessage("validate", "success"));
							server.serverGUI.addMessage("Validation success User: " + username);
							result.close();
							return;
						}
						else {
							//SEND FAILURE MESSAGE
							sendMessage(new NetworkingMessage("validate", "failure"));
							server.serverGUI.addMessage("Validation failure User: " + username);
							result.close();
							return;
						}
					} catch(SQLException sqle) {
						System.out.println("sqle: " + sqle.getMessage());
					} 
				}
				//RESEND EMAIL MESSAGE
				else if (message.getMessageType().equals("resendemail")){
					String username = message.getMessage();
					String firstname = "";
					String lastname = "";
					String validationPassphrase = "";
					
					//TELL SERVER GUI OF REGISTER MESSAGE ATTEMPT
					server.serverGUI.addMessage("Resend email attempt User: " + username);
					
					try {						
						//GET USER'S INFORMATION FROM DATABASE
						PreparedStatement ps = conn.prepareStatement("SELECT * FROM Users WHERE username =?");
						ps.setString(1,  username);
						ResultSet result = ps.executeQuery();
						
						while (result.next()){
							firstname = result.getString("firstname");
							lastname = result.getString("lastname");
							validationPassphrase = result.getString("validationpassphrase");
						}
						
						//TRY TO RESEND VALIDATION PASSPHRASE EMAIL TO USER
						EmailSender mailer = new EmailSender();
				        boolean emailResult = mailer.sendEmailToUser(firstname, lastname, username, validationPassphrase);
						if (emailResult == false){
							//RESEND EMAIL FAILURE
							sendMessage(new NetworkingMessage("resendemail", "failure"));
							server.serverGUI.addMessage("Resend email failure User: " + username + " is not a valid email address and/or the user's machine is not connected to the internet.");
							result.close();
							return;
						}
						else {
							//RESEND EMAIL SUCCESS
							sendMessage(new NetworkingMessage("resendemail", "success"));
							server.serverGUI.addMessage("Resend email success User: " + username);
							result.close();
							return;
						}
					} catch(SQLException sqle) {
						System.out.println("sqle: " + sqle.getMessage());
					} 
				}
				//PROFILE MESSAGE
				else if (message.getMessageType().equals("profile")){
					String[] parts = message.getMessage().split("\\|");
					String username = parts[0];
					String type = parts[1];
					String profilefilepath = "";
				
					//CREATE PROFILE PANEL MESSAGE
					if (type.equals("create")){
						try {						
							//GET USER PROFILE FILE PATH FROM DATABASE
							PreparedStatement ps = conn.prepareStatement("SELECT * FROM Users WHERE username =?");
							ps.setString(1,  username);
							ResultSet result = ps.executeQuery();
							
							while (result.next()){
								profilefilepath = result.getString("profilefilepath");
							}
							
							ImageIcon pictureIcon = new ImageIcon(profilefilepath);
							sendMessage(new NetworkingMessage("profile", "create", pictureIcon));
						} catch(SQLException sqle) {
							System.out.println("sqle: " + sqle.getMessage());
						} 
					}
					//UPDATE NAME INFORMATION MESSAGE
					else if (type.equals("updatename")){
						String firstname = parts[2];
						String lastname = parts[3];
						
						try {						
							//UPDATE USER'S NAME IN THE DATABASE
							PreparedStatement ps = conn.prepareStatement("UPDATE Users SET firstname = ?, lastname = ? WHERE username =?");
							ps.setString(1, firstname);
							ps.setString(2, lastname);
							ps.setString(3,  username);
							ps.executeUpdate();
							
							//CHECK THAT UPDATE WORKED
							ps = conn.prepareStatement("SELECT * FROM Users WHERE firstname = ? AND lastname = ? AND username =?");
							ps.setString(1, firstname);
							ps.setString(2, lastname);
							ps.setString(3, username);
							ResultSet result = ps.executeQuery();
							
							while (result.next()){
								sendMessage(new NetworkingMessage("profile", "updatename|success|" + firstname + "|" + lastname));
								server.serverGUI.addMessage("Update information success User: " + username);
								result.close();
								return;
							}
						} catch(SQLException sqle) {
							System.out.println("sqle: " + sqle.getMessage());
						} 
					}
					//UPDATE PROFILE PICTURE MESSAGE
					else if (type.equals("updatepicture")){
						ImageIcon pictureIcon = message.getImageIcon();
						Image pictureImage = pictureIcon.getImage();
						int width = pictureImage.getWidth(null);
					    int height = pictureImage.getHeight(null);
					    BufferedImage bufferedImage = new BufferedImage(width, height, BufferedImage.TYPE_INT_RGB);
					    Graphics g = bufferedImage.getGraphics();
					    g.drawImage(pictureImage, 0,0, null);
					    g.dispose();
					
					    File file = null;
						try {
							file = new File("users\\" + username + ".png");
						    ImageIO.write(bufferedImage, "png", file);
						} catch (IOException ioe) {
							System.out.println("ioe: " + ioe.getMessage());
						}
						
						try {						
							//UPDATE USER'S PROFILE FILE PATH IN THE DATABASE
							PreparedStatement ps = conn.prepareStatement("UPDATE Users SET profilefilepath = ? WHERE username =?");
							ps.setString(1, file.getAbsolutePath());
							ps.setString(2, username);
							ps.executeUpdate();
							
							//CHECK THAT UPDATE WORKED
							ps = conn.prepareStatement("SELECT * FROM Users WHERE profilefilepath = ? AND username =?");
							ps.setString(1,  file.getAbsolutePath());
							ps.setString(2, username);
							ResultSet result = ps.executeQuery();
							
							while (result.next()){
								sendMessage(new NetworkingMessage("profile", "updatepicture|success", pictureIcon));
								server.serverGUI.addMessage("Update profile picture success User: " + username);
								result.close();
								return;
							}
						} catch(SQLException sqle) {
							System.out.println("sqle: " + sqle.getMessage());
						} 
					}
				}
			}
		} catch (ClassNotFoundException cnfe) {
			System.out.println("cnfe in run: " + cnfe.getMessage());
		} catch (IOException ioe) {
			System.out.println("ioe in run: " + ioe.getMessage());
		} 
	}
	
	//SEND A MESSAGE TO THE CLIENT OUTPUT
	public void sendMessage(NetworkingMessage message) {
		//IF CLIENT IS NOT CONNECTED DO NOTHING
		if (!socket.isConnected()){
			close();
			return;
		}
		
		try {
			serverOutputStream.writeObject(message);
			serverOutputStream.flush();
		} catch (IOException ioe) {
			System.out.println("Error sending message to ioe: " + ioe.getMessage());
		}
	}
	
	//CLOSE EVERYTHING
	private void close(){
		//CLOSE DATABASE CONNECTION
		try {
			if (conn != null) {
				conn.close();
			} 
		} catch(SQLException sqle) {
				System.out.println("sql closing conn: " + sqle.getMessage());
		}
		//CLOSE OUTPUT STREAM
		try {
			if (serverOutputStream != null){
				serverOutputStream.close();
			}
		} catch (Exception e){
		}
		//CLOSE INPUT STREAM
		try {
			if (serverInputStream != null){
				serverInputStream.close();
			}
		} catch (Exception e){
		}
		//CLOSE CONNECTION
		try {
			if (socket != null) {
				socket.close();
			}
		} catch (Exception e){
		}
	}
}
