package annieson_CSCI201L_Scrabble;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Font;
import java.awt.FontFormatException;
import java.awt.Graphics;
import java.awt.GraphicsEnvironment;
import java.awt.Image;
import java.awt.Toolkit;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.Enumeration;

import javax.imageio.ImageIO;
import javax.swing.BoxLayout;
import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JPanel;
import javax.swing.UIManager;
import javax.swing.plaf.FontUIResource;

public class HomePage extends JFrame{

	private static final long serialVersionUID = 1L;
	
	int yellowColor;
	int blueColor;
	public Font scrabbleRegFont, scrabbleBoldFont;

	private JMenuBar jmb;
	private JMenu home, about;
	private JLabel scrabbleLogo;
	private JPanel mainPanel, panel1, panel2, panel3, panel4;
	
	private YellowHoverButton regButton, logButton, contButton;
	
	public HomePage() {
		super("Home"); //title of your GUI
		setSize(600, 500); //pixels horizontal by vertical. By default, it will only show the title bar
		setLocation(400, 100); //top left hand corner of your GUI; 0,0 is the top left
//		setLayout(new BoxLayout(getContentPane(), BoxLayout.Y_AXIS));
		setLayout(new BorderLayout());
		setFont();
		GUIFont (new FontUIResource(scrabbleRegFont));
		getColor();
		instantiateVariables();
		createGUI();
		addActions();
		
	}
	
	private void instantiateVariables() {
//		this.getContentPane().setBackground(new Color(yellowColor));
		mainPanel = new JPanel() {

			private static final long serialVersionUID = 1L;

			public void paintComponent(Graphics g) {
				g.drawImage(Toolkit.getDefaultToolkit().getImage("img/background/blueBackground.png"), 0, 0, this);
			}
		};
		mainPanel.setLayout(new BoxLayout(mainPanel, BoxLayout.Y_AXIS));
		
		jmb = new JMenuBar() {
			private static final long serialVersionUID = 1L;

			public void paintComponent(Graphics g) {
				g.setColor(new Color(yellowColor));
			}
		};
		home = new JMenu("Home");
		about = new JMenu("About");
		
		panel1 = new JPanel();
		panel2 = new JPanel();
		panel3 = new JPanel();
		panel4 = new JPanel();
		
		regButton = new YellowHoverButton("Register");
		logButton = new YellowHoverButton("Login");
		contButton = new YellowHoverButton("Continue as Guest");

		ImageIcon icon = new ImageIcon("img/scrabbleLogo.png"); 
		scrabbleLogo = new JLabel();
		scrabbleLogo.setIcon(icon);	
	}
	

	
	private void createGUI() {
		try {
			Class<?> applicationClass;
			applicationClass = Class.forName("com.apple.eawt.Application");	
			Method getApplicationMethod = applicationClass.getMethod("getApplication");
			Method setDockIconMethod = applicationClass.getMethod("setDockIconImage",  java.awt.Image.class);
			Object macOSXApplication = getApplicationMethod.invoke(null);
			Image fileImage = ImageIO.read(new File("img/icon/clientDockImage.png"));
			setDockIconMethod.invoke(macOSXApplication, fileImage);
			
		} catch (ClassNotFoundException cnfe) {
			System.out.println(cnfe.getMessage());
		} catch (NoSuchMethodException nsme) {
			System.out.println(nsme.getMessage());
		} catch (SecurityException se) {
			System.out.println(se.getMessage());
		} catch (IllegalAccessException iae) {
			System.out.println(iae.getMessage());
		} catch (IllegalArgumentException iarge) {
			System.out.println(iarge.getMessage());
		} catch (InvocationTargetException ite) {
			System.out.println(ite.getMessage());
		} catch (IOException ioe) {
			System.out.println(ioe.getMessage());
		}
		
        
		jmb.add(home);
		jmb.add(about);
		setJMenuBar(jmb);
		panel1.add(scrabbleLogo);
		panel2.add(regButton);
		panel3.add(logButton);
		panel4.add(contButton);
		
		
		mainPanel.add(panel1);
		mainPanel.add(panel2);
		mainPanel.add(panel3);
		mainPanel.add(panel4);
//		mainPanel.add(scrabbleLogo);
//		mainPanel.add(regButton);
//		mainPanel.add(logButton);
//		mainPanel.add(contButton);
//		
		add(mainPanel);
		
	}
	
	
	
	private void addActions() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	}
	
//	http://stackoverflow.com/questions/5824342/setting-the-global-font-for-a-java-application
	private static void GUIFont(FontUIResource fuir) {
	    Enumeration<Object> keys = UIManager.getDefaults().keys();
	    while (keys.hasMoreElements()) {
	        Object key = keys.nextElement();
	        Object val = UIManager.get(key);
	        if (val instanceof FontUIResource) {
	            UIManager.put(key, fuir);
	        }
	    }
	}
		
	
	private void setFont() {
		try {
            //create the font to use. Specify the size!
            scrabbleRegFont = Font.createFont(Font.TRUETYPE_FONT, new File("fonts/quicksand_regular.ttf")).deriveFont(12f);
            GraphicsEnvironment ge = GraphicsEnvironment.getLocalGraphicsEnvironment();
            ge.registerFont(scrabbleRegFont);
            
            scrabbleBoldFont = Font.createFont(Font.TRUETYPE_FONT, new File("fonts/quicksand_bold.ttf")).deriveFont(12f);
            GraphicsEnvironment ge2 = GraphicsEnvironment.getLocalGraphicsEnvironment();
            ge2.registerFont(scrabbleBoldFont);
            
        } catch (IOException ioe) {
            System.out.println(ioe.getMessage());
        }
        catch(FontFormatException ffe) {
        	System.out.println(ffe.getMessage());
        }       
	}
	
	private void getColor() {
		BufferedImage temp = null;
		try {
			temp = ImageIO.read(new File("img/scrollbar/yellowSlider.png"));
		} catch (IOException ioe) {
			System.out.println(ioe.getMessage());
		}
		yellowColor = temp.getRGB(20,20);
		
		BufferedImage temp2 = null;
		try {
			temp2 = ImageIO.read(new File("img/background/blueBackground.png"));
		} catch (IOException ioe) {
			System.out.println(ioe.getMessage());
		}
		blueColor = temp2.getRGB(0, 0);
	}
	
	public static void main(String[] args) {
		HomePage hp = new HomePage();
		hp.setVisible(true);
	}
	
}
