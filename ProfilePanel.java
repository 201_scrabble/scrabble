package CSCI201_FinalProject_Scrabble;

import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Image;
import java.awt.Insets;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;
import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;
import javax.swing.filechooser.FileNameExtensionFilter;

public class ProfilePanel extends JPanel {
	public static final long serialVersionUID = 1;
	
	//CLIENT GUI INSTANCE
	private ClientGUI clientGUI;
	//USERHOME PANEL
	//INFORMATION PANEL
	private JPanel informationcontainerPanel, informationlabelPanel, informationholderPanel;
	private JLabel informationLabel; 
	private JPanel picturePanel;
	protected ImageIcon pictureIcon;
	protected JLabel pictureLabel;
	private JButton changeprofileButton;
	private JPanel namePanel;
	private JLabel firstnameLabel, lastnameLabel;
	protected JTextField firstnameTextField, lastnameTextField;
	private GridBagConstraints gbc;
	private JButton updateinformationButton;
	//STATISTICS PANEL
	private JPanel statisticscontainerPanel;
	private JPanel statisticslabelPanel;
	private JLabel statisticsLabel;
	//ACHIEVEMENTS PANEL
	private JPanel achievementscontainerPanel;
	private JPanel achievementslabelPanel;
	private JLabel achievementsLabel;
	
	//ADD BACKGROUND 
	protected void paintComponent(Graphics g) {
		Dimension size = this.getSize();
		g.drawImage(Toolkit.getDefaultToolkit().getImage("resources/img/backgrounds/blue_panel.png"), 0, 0, size.width, size.height, this);
	}
	
	//CONSTRUCTOR
	public ProfilePanel(ClientGUI clientGUI){	
		this.clientGUI = clientGUI;
		instantiateComponents();
		createGUI();
		addActions();
	}
	
	//INSTANTIATE COMPONENTS
	private void instantiateComponents(){
		//INFORMATION CONTAINER PANEL
		informationcontainerPanel = new JPanel() {
			private static final long serialVersionUID = 1L;

			protected void paintComponent(Graphics g) {
				Dimension size = this.getSize();
				g.drawImage(Toolkit.getDefaultToolkit().getImage("resources/img/backgrounds/blue_panel.png"), 0, 0, size.width, size.height, this);
			}
		};
		informationcontainerPanel.setBorder(new EmptyBorder(20, 20, 20, 20));
		informationcontainerPanel.setSize(new Dimension(ProfilePanel.this.getWidth(), 300));
		informationcontainerPanel.setPreferredSize(new Dimension((int)informationcontainerPanel.getPreferredSize().getWidth(), 300));
		
		//INFORMATION LABEL
		informationlabelPanel = new JPanel(){
			private static final long serialVersionUID = 1L;

			protected void paintComponent(Graphics g) {
				Dimension size = this.getSize();
				g.drawImage(Toolkit.getDefaultToolkit().getImage("resources/img/backgrounds/grey_panel.png"), 0, 0, size.width, size.height, this);
			}
		};
		informationLabel = new JLabel("Your Information"); 
		
		//INFORMATION HOLDER PANEL
		informationholderPanel = new JPanel(){
			private static final long serialVersionUID = 1L;

			protected void paintComponent(Graphics g) {
				Dimension size = this.getSize();
				g.drawImage(Toolkit.getDefaultToolkit().getImage("resources/img/backgrounds/grey_panel.png"), 0, 0, size.width, size.height, this);
			}
		};
		
		//PROFILE PANEL
		picturePanel = new JPanel(){
			private static final long serialVersionUID = 1L;

			protected void paintComponent(Graphics g) {
				Dimension size = this.getSize();
				g.drawImage(Toolkit.getDefaultToolkit().getImage("resources/img/backgrounds/grey_panel.png"), 0, 0, size.width, size.height, this);
			}
		};
		picturePanel.setBorder(new EmptyBorder(20, 20, 20, 20));
		pictureIcon = new ImageIcon();
		pictureLabel = new JLabel();
		changeprofileButton = new CustomJButton(" Change Profile ");
		
		//NAME PANEL
		namePanel = new JPanel(){
			private static final long serialVersionUID = 1L;

			protected void paintComponent(Graphics g) {
				Dimension size = this.getSize();
				g.drawImage(Toolkit.getDefaultToolkit().getImage("resources/img/backgrounds/grey_panel.png"), 0, 0, size.width, size.height, this);
			}
		};
		namePanel.setLayout(new GridBagLayout());
		firstnameLabel = new JLabel("First Name: ");
		lastnameLabel = new JLabel("Last Name: ");
		firstnameTextField = new JTextField(10);
		firstnameTextField.setText(clientGUI.firstname);
		lastnameTextField = new JTextField(10);
		lastnameTextField.setText(clientGUI.lastname);
		updateinformationButton = new CustomJButton("Update Information");

		//GRID BAG CONSTRAINTS
		gbc = new GridBagConstraints();
		
		//STATISTICS PANEL
		statisticscontainerPanel = new JPanel() {
			private static final long serialVersionUID = 1L;

			protected void paintComponent(Graphics g) {
				Dimension size = this.getSize();
				g.drawImage(Toolkit.getDefaultToolkit().getImage("resources/img/backgrounds/blue_panel.png"), 0, 0, size.width, size.height, this);
			}
		};
		statisticscontainerPanel.setBorder(new EmptyBorder(20, 20, 20, 20));
		statisticscontainerPanel.setSize(new Dimension(ProfilePanel.this.getWidth(), 400));
		statisticscontainerPanel.setPreferredSize(new Dimension((int)statisticscontainerPanel.getPreferredSize().getWidth(), 400));
		
		statisticslabelPanel = new JPanel(){
			private static final long serialVersionUID = 1L;

			protected void paintComponent(Graphics g) {
				Dimension size = this.getSize();
				g.drawImage(Toolkit.getDefaultToolkit().getImage("resources/img/backgrounds/grey_panel.png"), 0, 0, size.width, size.height, this);
			}
		};
		statisticsLabel = new JLabel("Game Play Statistics");
		
		//ACHIEVEMENTS PANEL
		achievementscontainerPanel = new JPanel() {
			private static final long serialVersionUID = 1L;

			protected void paintComponent(Graphics g) {
				Dimension size = this.getSize();
				g.drawImage(Toolkit.getDefaultToolkit().getImage("resources/img/backgrounds/blue_panel.png"), 0, 0, size.width, size.height, this);
			}
		};
		achievementscontainerPanel.setBorder(new EmptyBorder(20, 20, 20, 20));
		achievementscontainerPanel.setSize(new Dimension(ProfilePanel.this.getWidth(), 300));
		achievementscontainerPanel.setPreferredSize(new Dimension((int)achievementscontainerPanel.getPreferredSize().getWidth(), 300));
		
		achievementslabelPanel = new JPanel(){
			private static final long serialVersionUID = 1L;

			protected void paintComponent(Graphics g) {
				Dimension size = this.getSize();
				g.drawImage(Toolkit.getDefaultToolkit().getImage("resources/img/backgrounds/grey_panel.png"), 0, 0, size.width, size.height, this);
			}
		};
		achievementsLabel = new JLabel("Your Achievements");
	}
	
	//CREATE GUI
	private void createGUI(){
		//SET LAYOUT
		setLayout(new BoxLayout(ProfilePanel.this, BoxLayout.Y_AXIS));
		informationcontainerPanel.setLayout(new BoxLayout(informationcontainerPanel, BoxLayout.Y_AXIS));
		informationholderPanel.setLayout(new BoxLayout(informationholderPanel, BoxLayout.X_AXIS));
		picturePanel.setLayout(new BoxLayout(picturePanel, BoxLayout.Y_AXIS));
		statisticscontainerPanel.setLayout(new BoxLayout(statisticscontainerPanel, BoxLayout.Y_AXIS));
		achievementscontainerPanel.setLayout(new BoxLayout(achievementscontainerPanel, BoxLayout.Y_AXIS));
		
		//INFORMATION PANEL: ADD COMPONENTS TO GRID BAG LAYOUT
		gbc.gridx = 0;
		gbc.gridy = 0;
		namePanel.add(firstnameLabel, gbc);
				
		gbc.gridx = 1;
		gbc.gridy = 0;
		namePanel.add(firstnameTextField, gbc);
						
		gbc.gridx = 0;
		gbc.gridy = 1;
		namePanel.add(lastnameLabel, gbc);
				
		gbc.gridx = 1;
		gbc.gridy = 1;
		namePanel.add(lastnameTextField, gbc);
		
		gbc.insets = new Insets(10,0,0,0); //top padding
		gbc.gridx = 0;
		gbc.gridy = 2;
		gbc.gridwidth = 2;
		namePanel.add(updateinformationButton, gbc);
		
		//ADD COMPONENTS TO PROFILE PANEL
		//INFORMATION PANEL
		informationlabelPanel.add(informationLabel);
		picturePanel.add(Box.createGlue());
		picturePanel.add(pictureLabel);
		picturePanel.add(changeprofileButton);
		picturePanel.add(Box.createGlue());
		informationholderPanel.add(Box.createGlue());
		informationholderPanel.add(picturePanel);
		informationholderPanel.add(Box.createGlue());
		informationholderPanel.add(namePanel);
		informationholderPanel.add(Box.createGlue());
		informationcontainerPanel.add(informationlabelPanel);
		informationcontainerPanel.add(informationholderPanel);
		//STATISTICS PANEL
		statisticslabelPanel.add(statisticsLabel);
		statisticscontainerPanel.add(statisticslabelPanel);
		//ACHIEVEMENTS PANEL
		achievementslabelPanel.add(achievementsLabel);
		achievementscontainerPanel.add(achievementslabelPanel);
		
		add(Box.createGlue());
		add(informationcontainerPanel);
		add(Box.createGlue());
		add(statisticscontainerPanel);
		add(Box.createGlue());
		add(achievementscontainerPanel);
		add(Box.createGlue());
	}
	
	//ADD ACTIONS 
	private void addActions(){
		//UPDATE INFORMATION BUTTON ACTION
		updateinformationButton.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent ae){
				String firstname = firstnameTextField.getText();
				String lastname = lastnameTextField.getText();
				
				//CHECK THAT NAME IS NOT THE SAME
				if (firstname.equals(clientGUI.firstname) && lastname.equals(clientGUI.lastname)){
					JOptionPane.showMessageDialog(clientGUI, 
							"You have not made any changes to your name." + "\n" + "Please update the fields accordingly and try again.", 
							"Update Information Failed", JOptionPane.ERROR_MESSAGE);
					return;
				}
				
				//START CLIENT AND ATTEMPT TO CONNECT TO SERVER
		        clientGUI.startClient();
				if(clientGUI.client.start()){
					clientGUI.client.sendMessageServer(new NetworkingMessage("profile", clientGUI.username + "|updatename|" + firstname + "|" + lastname));
				}
				else {
					clientGUI.loggedIn = false;
					JOptionPane.showMessageDialog(clientGUI, 
							"Server cannot be reached." + "\n" + "Please check that the server is running and try again.", 
							"Log-in Failed", JOptionPane.WARNING_MESSAGE);
					return;
				}
			}
		});
		
		//CHANGE PROFILE PICTURE BUTTON ACTION
		changeprofileButton.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent ae){
				//OPEN FILE CHOOSER AND RESTRICT SELECTION TO .PNG .JPG .JPEG .GIF FILES
				JFileChooser fileChooser = new JFileChooser();
				fileChooser.setDialogTitle("Open File...");
				FileNameExtensionFilter imageFilter = new FileNameExtensionFilter("image files (*.png, *.jpg, *.jpeg, *.gif)", new String[] {"png", "jpg", "jpeg", "gif"});
				fileChooser.setFileFilter(imageFilter);
				fileChooser.setAcceptAllFileFilterUsed(false);
				
				int returnValue = fileChooser.showOpenDialog(clientGUI);
				if (returnValue == JFileChooser.APPROVE_OPTION){
					File selectedFile = fileChooser.getSelectedFile();
	
					//IF FILE NAME DOES NOT END WITH VALID IMAGE EXTENSION, SHOW ERROR MESSAGE 
					if(!(selectedFile.getAbsolutePath().endsWith(".png") && selectedFile.getAbsolutePath().endsWith(".jpg")) && selectedFile.getAbsolutePath().endsWith(".jpeg") && selectedFile.getAbsolutePath().endsWith(".gif")){
						JOptionPane.showMessageDialog(clientGUI, 
								"Please include the file extension as part of the file name" + "\n" + "and try again.", 
								"Change Profile Picture Failed", JOptionPane.ERROR_MESSAGE);
						return;
					}
					
					//IF FILE CANNOT BE OPENED, SHOW ERROR MESSAGE AND EXIT
					if (!(selectedFile.isFile() && selectedFile.canRead())){
						JOptionPane.showMessageDialog(clientGUI, 
							"Action cannot be performed -- this file cannot be opened." + '\n' + "Please verify that the file exists in the selected directory.", 
							"Change Profile Picture Failed", JOptionPane.ERROR_MESSAGE);
						return;
					}		
					
					//LOAD IMAGE
					BufferedImage pictureImage = null;
					try {
					    pictureImage = ImageIO.read(selectedFile);
					} catch (IOException e) {
					}
					
					Image scaledpictureImage = pictureImage.getScaledInstance(175, 175,  java.awt.Image.SCALE_SMOOTH);
					ImageIcon pictureIcon = new ImageIcon(scaledpictureImage);
					
					//START CLIENT AND ATTEMPT TO CONNECT TO SERVER
			        clientGUI.startClient();
					if(clientGUI.client.start()){
						clientGUI.client.sendMessageServer(new NetworkingMessage("profile", clientGUI.username + "|updatepicture", pictureIcon));
					}
					else {
						clientGUI.loggedIn = false;
						JOptionPane.showMessageDialog(clientGUI, 
								"Server cannot be reached." + "\n" + "Please check that the server is running and try again.", 
								"Log-in Failed", JOptionPane.WARNING_MESSAGE);
						return;
					}
				}	
			}
		});
	}
}