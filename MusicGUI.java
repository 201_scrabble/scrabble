package annieson_CSCI201L_Scrabble;

import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.IOException;

import javax.sound.sampled.AudioInputStream;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.Clip;
import javax.sound.sampled.LineUnavailableException;
import javax.sound.sampled.UnsupportedAudioFileException;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;

public class MusicGUI extends JFrame{
	
	private JButton startButton, stopButton;
	private JLabel poojaNote;
	private JPanel buttonPress;
	private JButton correctButton, incorrectButton;
	private MusicPlayer mp;
	private String incorrect, correct;
	
	private static final long serialVersionUID = 1L;
	
	public MusicGUI() {
		super("Music"); //title of your GUI
		setSize(600, 500); //pixels horizontal by vertical. By default, it will only show the title bar
		setLocation(400, 100); //top left hand corner of your GUI; 0,0 is the top left
		setLayout(new BorderLayout());
		instantiateVariables();
		addActions();
		mp = new MusicPlayer();
	}
	
	private void instantiateVariables() {
		poojaNote = new JLabel("Don't forget to create a boolean that will mute all the sounds on the program!");
		
		buttonPress= new JPanel();
		correctButton = new YellowHoverButton("Correct!");
		incorrectButton = new YellowHoverButton("Incorrect!");
		
		buttonPress.add(correctButton);
		buttonPress.add(incorrectButton);
		
		startButton = new YellowHoverButton("Start Music!");
		stopButton = new YellowHoverButton("Stop Music!");
		add(startButton, BorderLayout.NORTH);
		add(poojaNote, BorderLayout.SOUTH);
		add(buttonPress, BorderLayout.CENTER);
		
		correct = "CorrectAnswer.wav"; 
		incorrect = "WrongAnswer.wav";
		
	}
	
	private void addActions() {
		
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		startButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent ae) {
				mp.play();
				addStopButton();
			}
		});
		
		stopButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent ae) {
				mp.stopMusic();
				addStartButton();
			}
		});
		
		correctButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent ae) {
				
				try {
					AudioInputStream audioInputStream = AudioSystem.getAudioInputStream(new File(correct).getAbsoluteFile());
					Clip clip = AudioSystem.getClip();
					clip.open(audioInputStream);
					clip.start();
				} catch (UnsupportedAudioFileException uafe) {
					uafe.getMessage();
				} catch (IOException ioe) {
					ioe.getMessage();
				} catch (LineUnavailableException lue) {
					lue.getMessage();
				}
			}
		});
		
		incorrectButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent ae) {
				try {
					AudioInputStream audioInputStream1 = AudioSystem.getAudioInputStream(new File(incorrect).getAbsoluteFile());
			
					Clip clip = AudioSystem.getClip();
					clip.open(audioInputStream1);
					clip.start();
				} catch (UnsupportedAudioFileException uafe) {
					uafe.getMessage();
				} catch (IOException ioe) {
					ioe.getMessage();
				} catch (LineUnavailableException lue) {
					lue.getMessage();
				}
			}
		});
	}
	
	private void addStopButton() {
		this.remove(startButton);
		this.add(stopButton, BorderLayout.NORTH);
		repaint();
		revalidate();
	}
	
	private void addStartButton() {
		this.remove(stopButton);
		this.add(startButton, BorderLayout.NORTH);
		repaint();
		revalidate();
	}
	
	public static void main (String[] args) {
		MusicGUI mgui = new MusicGUI();
		mgui.setVisible(true);
	}
	
}
