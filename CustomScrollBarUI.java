package CSCI201_FinalProject_Scrabble;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.Rectangle;
import java.io.File;
import java.io.IOException;
import javax.imageio.ImageIO;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.plaf.basic.BasicScrollBarUI;

//CUSTOM SCROLLBAR CLASS THAT EXTENDS BASICSCROLLBARUI
public class CustomScrollBarUI extends BasicScrollBarUI {
	//CUSTOM SCROLLBAR IMAGES
	private Image imageThumb;
	private Image imageTrack;
	private Image imageUpArrow;
	private Image imageDownArrow;
	
	//CONSTRUCTOR
	CustomScrollBarUI() {
		try {
			imageThumb = ImageIO.read(new File("resources/img/scrollbar/thumb.png"));
			imageTrack = ImageIO.read(new File("resources/img/scrollbar/track.png"));
			imageUpArrow = ImageIO.read(new File("resources/img/scrollbar/uparrow.png"));
			imageDownArrow = ImageIO.read(new File("resources/img/scrollbar/downarrow.png"));
		} catch (IOException e){
			e.printStackTrace();
		}
	}
  
	//UP AND DOWN ARROWS
	protected JButton createDecreaseButton(int orientation) {
		JButton button = new JButton();
		button.setIcon(new ImageIcon(imageUpArrow));
		return button;
	}

	protected JButton createIncreaseButton(int orientation) {
		JButton button = new JButton();
		button.setIcon(new ImageIcon(imageDownArrow));
		return button;
	}
	
	//THUMB AND TRACK
	protected void paintTrack(Graphics g, JComponent c, Rectangle trackBounds) {
		((Graphics2D) g).drawImage(imageTrack, trackBounds.x, trackBounds.y, trackBounds.width, trackBounds.height, null); 
	}

	protected void paintThumb(Graphics g, JComponent c, Rectangle thumbBounds) {
		g.setColor(Color.YELLOW);
	    ((Graphics2D) g).drawImage(imageThumb, thumbBounds.x, thumbBounds.y, thumbBounds.width, thumbBounds.height, null);
	}
}

