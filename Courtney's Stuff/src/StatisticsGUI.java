import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Graphics;
import java.awt.GridLayout;
import java.awt.Image;
import java.awt.Toolkit;

import javax.swing.BorderFactory;
import javax.swing.BoxLayout;
import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.border.Border;

import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartFrame;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.plot.PiePlot;
import org.jfree.data.category.DefaultCategoryDataset;
import org.jfree.data.general.DefaultPieDataset;

public class StatisticsGUI extends JFrame {
	private static final long serialVersionUID = 1L;
	private JLabel gamesPlayed, winsLabel, lastGame, longestWord, highestScoredPlay, totalPoints, twoLetters, threeLetters;
	private int numbOfGames;
	private Border boxBorder;
	private int numGames, numWins, numDraws, numLoss, numHighestScoredPlay, numPoints;
	private double perTwoLetters, perThreeLetters;
	private String lastGameTime, highestScoringWord;
	private String lastOp;
	private String longestWordS;
	//private static Image game1, game5, game10, game25, game50, game100, win1, win5, win10, win25, win50, win100, point50, point250, point750, point500, point100, point1000;
	private ImageIcon game1, game5, game10, game25, game50, game100, win1, win5, win10, win25, win50, win100, point50, point100, point250, point500, point750, point1000;
	private Toolkit myTK;
	
	public void importPictures(){
		game1 = new ImageIcon("img/1sun.png");
		game5 = new ImageIcon("img/5sun.png");
		game10 = new ImageIcon("img/10sun.png");
		game25 = new ImageIcon("img/25sun.png");
		game50 = new ImageIcon("img/50sun.png");
		game100 = new ImageIcon("img/100sun.png");
		win1 = new ImageIcon("img/1win.png");
		win5 = new ImageIcon("img/5win.png");
		win10 = new ImageIcon("img/10win.png");
		win25 = new ImageIcon("img/25win.png");
		win50 = new ImageIcon("img/50win.png");
		win100 = new ImageIcon("img/100win.png");
		point50 = new ImageIcon("img/50points.png");
		point100 = new ImageIcon("img/100points.png");
		point250 = new ImageIcon("img/250points.png");
		point500 = new ImageIcon("img/500points.png");
		point750 = new ImageIcon("img/750points.png");
		point1000 = new ImageIcon("img/1000points.png");

	}
	private void instantiateComponents(){
		importPictures();
		numGames = 1;
		perTwoLetters = 22;
		perThreeLetters = 36; 
		numWins = 30;
		numDraws = 2;
		numLoss = 6;
		numPoints = 780;
		
		numHighestScoredPlay = 15;
		lastGameTime = "1/8/16 1:45PM";
		longestWordS = "Poops";
		highestScoringWord = "Poo";
		
		gamesPlayed = new JLabel("Number of Games Played: ");
		winsLabel = new JLabel("Wins | Draws | Losses");
		lastGame = new JLabel("Last Game Played: ");
		longestWord = new JLabel("Longest Word Played: ");
		highestScoredPlay = new JLabel("Highest Scoring Play:");
		totalPoints = new JLabel("Total points:");
		twoLetters = new JLabel("");//("2 Letter Word Plays");
		threeLetters = new JLabel("");//("3 Letter Word Plays");
		boxBorder = BorderFactory.createLineBorder(Color.black);
		
	}
	
	private void createGUI(){
		setSize(400, 700);
		setLocation(500, 50);
		setLayout(new GridLayout(4, 2));
		
		//number of games panel
		StatsPanel gamesPlayedP = new StatsPanel(gamesPlayed);
		if(numGames != 0){
			JLabel imageHolder = new JLabel();
			if(numGames < 5){
				imageHolder.setIcon(game1);
			} else if(numGames >= 5 && numGames < 10){
				imageHolder.setIcon(game5);
			} else if(numGames >= 10 && numGames < 25){
				imageHolder.setIcon(game10);
			} else if(numGames >= 25 && numGames < 50){
				imageHolder.setIcon(game25);
			} else if(numGames >= 50 && numGames < 100){
				imageHolder.setIcon(game50);
			} else if (numGames >= 100) {
				imageHolder.setIcon(game100);
			}
			gamesPlayedP.addLabel(imageHolder);
		} 
		JLabel gameNum = new JLabel("Games: " + Integer.toString(numGames));
		gamesPlayedP.addLabel(gameNum);
		add(gamesPlayedP);
		
		//number of wins/draws/losses panel
		StatsPanel wDLP = new StatsPanel(winsLabel);
		if(numWins != 0){
			JLabel imageHolder = new JLabel(); 
			if(numWins < 5){
				imageHolder.setIcon(win1);
			} else if(numWins >= 5 && numWins < 10){
				imageHolder.setIcon(win5);
			} else if(numWins >= 10 && numWins < 25){
				imageHolder.setIcon(win10);
			} else if(numWins >= 25 && numWins < 50){
				imageHolder.setIcon(win25);
			} else if(numWins >= 50 && numWins < 100){
				imageHolder.setIcon(win50);
			} else if (numWins >= 100) {
				imageHolder.setIcon(win100);
			}
			wDLP.addLabel(imageHolder);
		} 
		JLabel winsL = new JLabel("Won: " + Integer.toString(numWins) + " Draws: " + Integer.toString(numDraws) + " Lost: " + Integer.toString(numLoss));
		wDLP.addLabel(winsL);
		add(wDLP);
		
		//last game played panel
		StatsPanel lastGameP = new StatsPanel(lastGame);
		lastGameP.addLabel(new JLabel(lastGameTime));
		add(lastGameP);
		
		//longest word panel
		StatsPanel longestWordP = new StatsPanel(longestWord);	
		JPanel longTileHolder = new JPanel();
		JLabel[] lImages = new JLabel[longestWordS.length()];
		for(int i = 0; i < longestWordS.length(); i++){
			//System.out.println(longestWordS);
			longestWordS = longestWordS.toUpperCase();
			char currCh = longestWordS.charAt(i);
			lImages[i] = new JLabel();
			lImages[i].setIcon(new ImageIcon("img/" + currCh + ".jpg"));
			longTileHolder.add(lImages[i]);
		}
		longestWordP.add(longTileHolder);
		add(longestWordP);
		
		//highest scoring play panel
		StatsPanel highestScoreP = new StatsPanel(highestScoredPlay);
		JPanel highScoreHolder = new JPanel();
		JLabel[] highImages = new JLabel[highestScoringWord.length()];
		for(int i = 0; i < highestScoringWord.length(); i++){
			highestScoringWord = highestScoringWord.toUpperCase();
			char myCurr = highestScoringWord.charAt(i);
			highImages[i] = new JLabel();
			highImages[i].setIcon(new ImageIcon("img/" + myCurr + ".jpg"));
			highScoreHolder.add(highImages[i]);
		}
		highestScoreP.add(highScoreHolder);
		add(highestScoreP);
		
		//total number of points panel
		StatsPanel totalScoreP = new StatsPanel(totalPoints);
		if(numPoints >= 50){
			JLabel imageHolder = new JLabel();
			if(numPoints >= 50 && numPoints < 100){
				imageHolder.setIcon(point50);
			} else if(numPoints >= 100 && numPoints < 250){
				imageHolder.setIcon(point100);	
			} else if(numPoints >= 250 && numPoints < 500){
				imageHolder.setIcon(point250);
			} else if(numPoints >= 500 && numPoints < 750){
				imageHolder.setIcon(point500);
			} else if(numPoints >= 750 && numPoints < 1000){
				imageHolder.setIcon(point750);
			} else if (numPoints >= 1000) {
				imageHolder.setIcon(point1000);
			}
			totalScoreP.addLabel(imageHolder);	
		} 
		JLabel gamePLabel = new JLabel("Points: " + Integer.toString(numPoints));
		totalScoreP.addLabel(gamePLabel);
		add(totalScoreP);
		
		//percentage of two letters panel
		
		StatsPanel twoLettersP = new StatsPanel(twoLetters);
		add(twoLettersP);
		
		//percentage of three letters panel
		StatsPanel threeLettersP = new StatsPanel(threeLetters);
		add(threeLettersP);
		DefaultPieDataset twoLetters = new DefaultPieDataset();
		twoLetters.setValue("Two Letters", new Double(perTwoLetters));
		twoLetters.setValue("Other", new Double(100-perTwoLetters));
		JFreeChart twoChart = ChartFactory.createPieChart("Two Letter Plays", twoLetters, true, true, true); //true true false?
		ChartPanel twoChartP = new ChartPanel(twoChart);
		twoLettersP.add(twoChartP);
		
		DefaultPieDataset threeLetters = new DefaultPieDataset();
		threeLetters.setValue("Three Letters", new Double(perThreeLetters));
		threeLetters.setValue("Other", new Double(100-perThreeLetters));
		JFreeChart threeChart = ChartFactory.createPieChart("Three Letter Plays", threeLetters, true, true, true); //true true false?
		ChartPanel threeChartP = new ChartPanel(threeChart);
		threeLettersP.add(threeChartP);
		
	}
	
	private void addActions(){
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
	}

	public StatisticsGUI(){
		super("Player Statistics"); 
		instantiateComponents();
		createGUI();
		addActions();
	}
	
	public static void main(String[] args){
		StatisticsGUI myGUI = new StatisticsGUI();
		myGUI.setVisible(true);
	}
	
	class StatsPanel extends JPanel{
		private static final long serialVersionUID = 1L;
		StatsPanel(JLabel myL){
			this.setLayout(new BoxLayout(this,BoxLayout.Y_AXIS));
			this.setBorder(boxBorder);
			JPanel flow = new JPanel();
			flow.add(myL);
			add(flow);
			
		}
		public void addLabel(JLabel label){
			JPanel cP = new JPanel();
			cP.add(label);
			this.add(cP);
		}
		
		
	}


	
		
}
