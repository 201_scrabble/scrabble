import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.FlowLayout;

import javax.swing.BorderFactory;
import javax.swing.BoxLayout;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.ListCellRenderer;
import javax.swing.ListSelectionModel;
import javax.swing.UIManager;
import javax.swing.border.Border;

public class FriendsGUI extends JFrame{
	private static final long serialVersionUID = 1L;
	private JLabel searchForFriendsL, notificationsL, yourFriendsL, leadershipL;
	private JTextField findFriendField;
	private JButton addFriendB, inviteToGameB, viewProfileB, removeFriendB;
	private JScrollPane notificationsScroll, friendScroll;
	private JTextArea notificationText;
	private JList myFriends;
	private void instantiateComponents(){
		searchForFriendsL = new JLabel("SEARCH FOR FRIENDS");
		notificationsL = new JLabel("YOUR NOTIFICATIONS");
		yourFriendsL = new JLabel("YOUR FRIENDS");
		leadershipL = new JLabel("FRIENDS LEADERBOARD");
		findFriendField = new JTextField(8);
		addFriendB = new JButton("ADD");
		notificationText = new JTextArea(3,1);
		notificationText.setEditable(false);
		notificationText.setLineWrap(true);
		notificationsScroll = new JScrollPane(notificationText);
		inviteToGameB = new JButton("Invite To Game");
		viewProfileB = new JButton("View Profile");
		removeFriendB = new JButton("Remove Friend");
	}
	
	private void createGUI(){
		setSize(450, 500);
		setLocation(500, 50);
		JPanel overall = new JPanel();
		overall.setLayout(new BoxLayout(overall, BoxLayout.Y_AXIS));
		//top panel
		JPanel topPanel = new JPanel();
		
		//Top left search panel
		JPanel searchPanel = new JPanel();
		searchPanel.setLayout(new BoxLayout(searchPanel, BoxLayout.Y_AXIS));
		searchPanel.add(searchForFriendsL);
		searchPanel.add(findFriendField);
		searchPanel.add(addFriendB);
		topPanel.add(searchPanel);
		
		//Top right notifications panel
		JPanel notificationsP = new JPanel();
		notificationsP.setLayout(new BoxLayout(notificationsP, BoxLayout.Y_AXIS));
		notificationsP.add(notificationsL);
		notificationsScroll.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_ALWAYS);
		notificationsP.add(notificationsScroll);
		topPanel.add(notificationsP);
		overall.add(topPanel);
		
		//middle panel
		JPanel middlePanel = new JPanel();
		JPanel labelPanel = new JPanel();
		labelPanel.setLayout(new FlowLayout(FlowLayout.LEFT));
		middlePanel.setLayout(new BoxLayout(middlePanel, BoxLayout.Y_AXIS));
		labelPanel.add(yourFriendsL);
		middlePanel.add(labelPanel);
		myFriends = new JList();
		myFriends.setCellRenderer(new FriendsListCellRenderer());
		//images
		ImageIcon annieI = new ImageIcon("img/Annie.jpg");
		ImageIcon jennyI = new ImageIcon("img/Jenny.jpg");
		ImageIcon courtneyI = new ImageIcon("img/Courtney.jpg");
		ImageIcon poojaI = new ImageIcon("img/Pooja.jpg");
		
		JLabel annieL = new JLabel("Annie", annieI, JLabel.LEFT);
		JLabel jennyL = new JLabel("Jenny", jennyI, JLabel.LEFT);
		JLabel courtneyL = new JLabel("Courtney", courtneyI, JLabel.LEFT);
		JLabel poojaL = new JLabel("Pooja", poojaI, JLabel.LEFT);
		
		JPanel annieP = new JPanel(new FlowLayout(FlowLayout.LEFT));
		JPanel jennyP = new JPanel(new FlowLayout(FlowLayout.LEFT));
		JPanel courtneyP = new JPanel(new FlowLayout(FlowLayout.LEFT));
		JPanel poojaP = new JPanel(new FlowLayout(FlowLayout.LEFT));
		
		annieP.add(annieL);
		jennyP.add(jennyL);
		courtneyP.add(courtneyL);
		poojaP.add(poojaL);
		
		Object[] friendsPanels = {annieP, jennyP, courtneyP, poojaP};
		myFriends.setListData(friendsPanels);
		myFriends.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		myFriends.setLayoutOrientation(JList.VERTICAL);
		
		JScrollPane friendsScroll = new JScrollPane(myFriends);
		friendsScroll.setSize(new Dimension(10, 10));
		middlePanel.add(friendsScroll);
		JPanel friendButtons = new JPanel();
		friendButtons.add(inviteToGameB);
		friendButtons.add(viewProfileB);
		friendButtons.add(removeFriendB);
		middlePanel.add(friendButtons);
		overall.add(middlePanel);	
		
		add(overall);
	}
	
	private void addActions(){
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	}
	
	public FriendsGUI(){
		super("FRIENDS"); 
		instantiateComponents();
		createGUI();
		addActions();
	}
	
	public static void main(String[] args){
		FriendsGUI friendsGUI = new FriendsGUI();
		friendsGUI.setVisible(true);
	}
	class FriendsListCellRenderer implements ListCellRenderer{

		@Override
		public Component getListCellRendererComponent(JList list, Object value, int index, boolean isSelected, boolean cellHasFocus) {
			if (value instanceof JPanel){
				Component component = (Component) value;
				component.setForeground(Color.white);
				component.setBackground(isSelected ? UIManager.getColor("Table.focusCellForeground") : Color.white);
				return component;
			} else {
				return new JLabel();
			}
		}
		
	}
	//http://alvinalexander.com/java/jlist-image-jlabel-renderer
	

	
}
