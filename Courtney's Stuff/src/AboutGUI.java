import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

import javax.swing.BoxLayout;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;

public class AboutGUI extends JFrame{
	private static final long serialVersionUID = 1L;
	private JScrollPane gamePlayScroll, creatorScroll, sourcesScroll;
	private JTextArea gamePlayText, creatorText, sourcesText;
	private JLabel gamePlayL, creatorL, sourcesL;
	private FileReader myFr;
	private BufferedReader myBr;
	
	private void instantiateComponents(){
		gamePlayText = new JTextArea(1,1);
		gamePlayText.setEditable(false);
		gamePlayText.setLineWrap(true);
		gamePlayScroll = new JScrollPane(gamePlayText);
		creatorText = new JTextArea(1,1);
		creatorText.setEditable(false);
		creatorText.setLineWrap(true);
		creatorScroll = new JScrollPane(creatorText);
		sourcesText = new JTextArea(1,1);
		sourcesText.setEditable(false);
		sourcesText.setLineWrap(true);
		sourcesScroll = new JScrollPane(sourcesText);
		gamePlayL = new JLabel("Game Play Rules");
		creatorL = new JLabel("About The Developers");
		sourcesL = new JLabel("Sources Used");
	}
	
	private void createGUI(){
		setSize(420, 500);
		setLocation(500, 50);
		JPanel overall = new JPanel();
		overall.setLayout(new BoxLayout(overall, BoxLayout.Y_AXIS));
		//game play
		overall.add(gamePlayL);
		gamePlayScroll.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_ALWAYS);
		overall.add(gamePlayScroll);
		
		//creator
		overall.add(creatorL);
		creatorScroll.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_ALWAYS);
		overall.add(creatorScroll);
		
		//sources
		overall.add(sourcesL);
		sourcesScroll.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_ALWAYS);
		overall.add(sourcesScroll);
		add(overall);
	}
	
	private void addActions(){
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	}
	
	public AboutGUI(){
		super("About"); 
		instantiateComponents();
		readRules();
		createGUI();
		addActions();
	}
	
	public static void main(String[] args){
		AboutGUI myAGUI = new AboutGUI();
		myAGUI.setVisible(true);
	}

	public void readRules(){
		String line = "";
		try{
			myFr = new FileReader("ScrabbleRules.txt");
			myBr = new BufferedReader(myFr);
			while((line = myBr.readLine()) != null){
				gamePlayText.append(line);
				gamePlayText.append("\n");
			}
			myFr = new FileReader("Developers.txt");
			myBr = new BufferedReader(myFr);
			while((line = myBr.readLine()) != null){
				creatorText.append(line);
				creatorText.append("\n");
			}
			myFr = new FileReader("Sources.txt");
			myBr = new BufferedReader(myFr);
			while((line = myBr.readLine()) != null){
				sourcesText.append(line);
				sourcesText.append("\n");
			}
		} catch (FileNotFoundException fnfe){
			System.out.println("FileNotFoundException: " + fnfe.getMessage());
		} catch (IOException ioe){
			System.out.println("IOException: " + ioe.getMessage());
		} finally {
			try {
				if (myBr != null){
					myBr.close();
				}
				if (myFr != null){
					myFr.close();
				}
			} catch (IOException ioe) {
				System.out.println("IOException closing file: " + ioe.getMessage());
			}
		}
	}
	
}
