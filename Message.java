package CSCI201_FinalProject_Scrabble;

import java.io.Serializable;

public class Message implements Serializable {
	public static final long serialVersionUID = 1;
	
	private String messagetype;
	private String message;
	
	public Message(String messagetype, String message) {
		this.messagetype = messagetype;
		this.message = message;
	}
	
	public String getMessageType() {
		return messagetype;
	}
	
	public String getMessage() {
		return message;
	}
}
