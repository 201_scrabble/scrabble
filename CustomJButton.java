package CSCI201_FinalProject_Scrabble;

import java.awt.Dimension;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.Graphics;
import java.awt.Toolkit;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import javax.swing.JButton;

//CUSTOM BUTTON CLASS THAT EXTENDS JBUTTON
public class CustomJButton extends JButton implements MouseListener{
	private static final long serialVersionUID = 1L;
	
	String buttonText;
	boolean isButtonHover = false;
	
	public CustomJButton(String buttonText){
		super(buttonText);
		this.buttonText = buttonText;
		addMouseListener(this);
	}
	
	protected void paintComponent(Graphics g) {
		validate();
		repaint();
		setOpaque(false);
		setContentAreaFilled(false);
		setBorderPainted(false);
		
		Dimension size = this.getSize();
		if (isButtonHover == false){
			g.drawImage(Toolkit.getDefaultToolkit().getImage("resources/img/buttons/button.png"), 0, 0, size.width, size.height, null);	
		}
		else {
			g.drawImage(Toolkit.getDefaultToolkit().getImage("resources/img/buttons/buttonhover.png"), 0, 0, size.width, size.height, null);		
		}
		
		Font font = this.getFont();
		FontMetrics metrics = g.getFontMetrics(font);
		g.setFont(font);
		g.drawString(buttonText, (size.width - metrics.stringWidth(buttonText))/2, size.height/2);
	}
	
	public void mouseEntered(MouseEvent e) {				
		isButtonHover = true;
		revalidate();
		repaint();
	}

	public void mouseExited(MouseEvent e) {				
		isButtonHover = false;
		repaint();
		revalidate();
	}

	public void mouseClicked(MouseEvent arg0) {		
	}

	public void mousePressed(MouseEvent arg0) {		
	}

	public void mouseReleased(MouseEvent arg0) {		
	}
}
