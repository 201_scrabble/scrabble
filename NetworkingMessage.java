package CSCI201_FinalProject_Scrabble;

import java.io.Serializable;

import javax.swing.ImageIcon;

public class NetworkingMessage implements Serializable {
	public static final long serialVersionUID = 1;
	
	private String messagetype;
	private String message;
	private ImageIcon imageIcon;
	
	public NetworkingMessage(String messagetype, String message) {
		this.messagetype = messagetype;
		this.message = message;
	}
	
	public NetworkingMessage(String messagetype, String message, ImageIcon imageIcon) {
		this.messagetype = messagetype;
		this.message = message;
		this.imageIcon = imageIcon;
	}
	
	public String getMessageType() {
		return messagetype;
	}
	
	public String getMessage() {
		return message;
	}
	
	public ImageIcon getImageIcon() {
		return imageIcon;
	}
}
