package annieson_CSCI201L_Scrabble;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Image;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

import javax.imageio.ImageIO;
import javax.swing.BoxLayout;
import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;

public class Badges extends JFrame {

	public static final long serialVersionUID = 1; 
			
		int yellowColor;
		int blueColor;
		
		private JLabel pt50, pt100, pt250, pt500, pt750, pt1000;
		private JLabel tg1, tg5, tg10, tg25, tg50, tg100;
		private JLabel tw1, tw5, tw10, tw25, tw50, tw100;
		private JPanel totalWins, totalPoints, totalGames, mainPanel;

	public Badges () {
		super("Badge Test!!!");
		setSize(600,600);
		setLocation(400,100);
		setLayout(new BorderLayout());
		getColor();
		instantiateVariables();
		createGUI();
		addActions();
	}
	
	private void instantiateVariables() {
		mainPanel = new JPanel();
		mainPanel.setLayout(new BoxLayout(mainPanel, BoxLayout.Y_AXIS));
		totalWins = new JPanel();
		totalPoints = new JPanel();
		totalGames = new JPanel();
		
		totalWins.setBackground(new Color(yellowColor));
		totalPoints.setBackground(new Color(blueColor));
		totalGames.setBackground(new Color(yellowColor));

		ImageIcon icon = new ImageIcon("img/totalwins/1win.png"); 
		tw1 = new JLabel();
		tw1.setIcon(icon);
		
		ImageIcon icon1 = new ImageIcon("img/totalwins/5win.png"); 
		tw5 = new JLabel();
		tw5.setIcon(icon1);
		
		ImageIcon icon2 = new ImageIcon("img/totalwins/10win.png"); 
		tw10 = new JLabel();
		tw10.setIcon(icon2);
		
		ImageIcon icon3 = new ImageIcon("img/totalwins/25win.png"); 
		tw25 = new JLabel();
		tw25.setIcon(icon3);
		
		ImageIcon icon4 = new ImageIcon("img/totalwins/50wins.png"); 
		tw50 = new JLabel();
		tw50.setIcon(icon4);	
		
		ImageIcon icon5 = new ImageIcon("img/totalwins/100win.png"); 
		tw100 = new JLabel();
		tw100.setIcon(icon5);
		
		//totalpoints
		ImageIcon icona = new ImageIcon("img/totalpoints/50points.png"); 
		pt50 = new JLabel();
		pt50.setIcon(icona);
		
		ImageIcon iconb = new ImageIcon("img/totalpoints/100points.png"); 
		pt100 = new JLabel();
		pt100.setIcon(iconb);
		
		ImageIcon iconc = new ImageIcon("img/totalpoints/250points.png"); 
		pt250 = new JLabel();
		pt250.setIcon(iconc);
		
		ImageIcon icond = new ImageIcon("img/totalpoints/500points.png"); 
		pt500 = new JLabel();
		pt500.setIcon(icond);
		
		ImageIcon icone = new ImageIcon("img/totalpoints/750points.png"); 
		pt750 = new JLabel();
		pt750.setIcon(icone);
		
		ImageIcon iconf = new ImageIcon("img/totalpoints/1000points.png"); 
		pt1000 = new JLabel();
		pt1000.setIcon(iconf);
		
		//totalgames
		ImageIcon icong = new ImageIcon("img/totalgames/1sun.png"); 
		tg1 = new JLabel();
		tg1.setIcon(icong);
		
		ImageIcon iconh = new ImageIcon("img/totalgames/5sun.png"); 
		tg5 = new JLabel();
		tg5.setIcon(iconh);
		
		ImageIcon iconi = new ImageIcon("img/totalgames/10sun.png"); 
		tg10 = new JLabel();
		tg10.setIcon(iconi);
		
		ImageIcon iconj = new ImageIcon("img/totalgames/25sun.png"); 
		tg25 = new JLabel();
		tg25.setIcon(iconj);
		
		ImageIcon iconk = new ImageIcon("img/totalgames/50sun.png"); 
		tg50= new JLabel();
		tg50.setIcon(iconk);
		
		ImageIcon iconl = new ImageIcon("img/totalgames/100sun.png"); 
		tg100 = new JLabel();
		tg100.setIcon(iconl);
		
	}
		
	private void createGUI() {
		try {
			Class<?> applicationClass;
			applicationClass = Class.forName("com.apple.eawt.Application");	
			Method getApplicationMethod = applicationClass.getMethod("getApplication");
			Method setDockIconMethod = applicationClass.getMethod("setDockIconImage",  java.awt.Image.class);
			Object macOSXApplication = getApplicationMethod.invoke(null);
			Image fileImage = ImageIO.read(new File("img/icon/ServerDockImage.png"));
			setDockIconMethod.invoke(macOSXApplication, fileImage);
				
		} catch (ClassNotFoundException cnfe) {
			System.out.println(cnfe.getMessage());
		} catch (NoSuchMethodException nsme) {
			System.out.println(nsme.getMessage());
		} catch (SecurityException se) {
			System.out.println(se.getMessage());
		} catch (IllegalAccessException iae) {
			System.out.println(iae.getMessage());
		} catch (IllegalArgumentException iarge) {
			System.out.println(iarge.getMessage());
		} catch (InvocationTargetException ite) {
			System.out.println(ite.getMessage());
		} catch (IOException ioe) {
			System.out.println(ioe.getMessage());
		}
			
		totalWins.add(tw1);
		totalWins.add(tw5);
		totalWins.add(tw10);
		totalWins.add(tw25);
		totalWins.add(tw50);
		totalWins.add(tw100);
		
		totalPoints.add(pt50);
		totalPoints.add(pt100);
		totalPoints.add(pt250);
		totalPoints.add(pt500);
		totalPoints.add(pt750);
		totalPoints.add(pt1000);
		
		totalGames.add(tg1);
		totalGames.add(tg5);
		totalGames.add(tg10);
		totalGames.add(tg25);
		totalGames.add(tg50);
		totalGames.add(tg100);
	       
		mainPanel.add(totalWins);
		mainPanel.add(totalPoints);
		mainPanel.add(totalGames);
		add(mainPanel);

	}
		
		
		
	private void addActions() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	}
			
		

	private void getColor() {
		BufferedImage temp = null;
		try {
			temp = ImageIO.read(new File("img/scrollbar/yellowSlider.png"));
		} catch (IOException ioe) {
			System.out.println(ioe.getMessage());
		}
		yellowColor = temp.getRGB(20,20);
			
		BufferedImage temp2 = null;
		try {
			temp2 = ImageIO.read(new File("img/background/blueBackground.png"));
		} catch (IOException ioe) {
			System.out.println(ioe.getMessage());
		}
		blueColor = temp2.getRGB(0, 0);
	}
		
	public static void main(String[] args) {
		Badges badgeTest = new Badges();
		badgeTest.setVisible(true);
			
	}
	
}
