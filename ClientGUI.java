package CSCI201_FinalProject_Scrabble;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JToolBar;
import javax.swing.SwingConstants;

public class ClientGUI extends JFrame{
	
	public static final long serialVersionUID = 1;
	
	//NETWORKING COMPONENTS
	private String hostname;
	private int port;
	protected Client client;
	protected String username;
	protected String firstname;
	protected String lastname;
	protected boolean loggedIn;
	//HOME TOOL BAR
	protected JToolBar hometoolBar;
	private JButton homeButton, aboutButton;
	//USERHOME TOOL BAR
	protected JToolBar userhometoolBar;
	private JButton userhomeButton, logoutButton;
	//CLIENT GUI COMPONENTS
	protected HomePanel homePanel;
	protected AboutPanel aboutPanel;
	protected LoginPanel loginPanel;
	protected RegisterPanel registerPanel;
	protected ValidationPanel validationPanel;
	protected UserHomePanel userhomePanel;
	protected ProfilePanel profilePanel;
	
	//CONSTRUCTOR
	public ClientGUI(){
		super("Scrabble");
		instantiateComponents();
		createGUI();
		addActions();
		setVisible(true);
	}
	
	//INITIALIZE COMPONENTS
	private void instantiateComponents(){
		//TOOL BAR
		hometoolBar = new JToolBar() { 
			private static final long serialVersionUID = 1L;

			public void paintComponent(Graphics g){
				Dimension size = this.getSize();
				g.drawImage(Toolkit.getDefaultToolkit().getImage("resources/img/backgrounds/grey_panel.png"), 0, 0, size.width, size.height, this);
			}
        };   
        hometoolBar.setFloatable(false);
        
        ImageIcon homeIcon = new ImageIcon("resources/img/icon/homeicon.png");
        homeButton = new JButton("Home", homeIcon);
        homeButton.setHorizontalTextPosition(SwingConstants.RIGHT);
        
        ImageIcon aboutIcon = new ImageIcon("resources/img/icon/abouticon.png");
		aboutButton = new JButton("About", aboutIcon);
		aboutButton.setHorizontalTextPosition(SwingConstants.RIGHT);
		
		//USERHOME TOOL BAR
		userhometoolBar = new JToolBar() { 
			private static final long serialVersionUID = 1L;

			public void paintComponent(Graphics g){
				Dimension size = this.getSize();
				g.drawImage(Toolkit.getDefaultToolkit().getImage("resources/img/backgrounds/grey_panel.png"), 0, 0, size.width, size.height, this);
			}
        };   
        userhometoolBar.setFloatable(false);
        
        ImageIcon userhomeIcon = new ImageIcon("resources/img/icon/homeicon.png");
        userhomeButton = new JButton("User Home", userhomeIcon);
        userhomeButton.setHorizontalTextPosition(SwingConstants.RIGHT);
        
        ImageIcon logoutIcon = new ImageIcon("resources/img/icon/logouticon.png");
		logoutButton = new JButton("Logout", logoutIcon);
		logoutButton.setHorizontalTextPosition(SwingConstants.RIGHT);		
		
		//ADD USERHOME TOOL BAR ITEMS
		userhometoolBar.add(userhomeButton);
		userhometoolBar.add(logoutButton);
		
		//HOME PANEL
		homePanel = new HomePanel(ClientGUI.this);
		
		//ABOUT PANEL
		aboutPanel = new AboutPanel(ClientGUI.this);
		
		//LOGIN PANEL
		loginPanel = new LoginPanel(ClientGUI.this);
		
		//REGISTER PANEL
		registerPanel = new RegisterPanel(ClientGUI.this);
		
		//VALIDATION PANEL
		validationPanel = new ValidationPanel(ClientGUI.this);
	}
		
	//CREATE GUI
	private void createGUI(){
		//GET SCREEN SIZE
		Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
		int screenWidth = (int) screenSize.getWidth();
		int screenHeight = (int) screenSize.getHeight();
				
		//SET APPLICATION ICON
		ImageIcon imgIcon = new ImageIcon("resources/img/icon/clienticon.png");
		setIconImage(imgIcon.getImage());
				
		//SET SIZE AND LOCATION
		setSize(1000,800);
		setMinimumSize(new Dimension(1000, 1000));
		setLocation((screenWidth/2-1000)/2,(screenHeight-1000)/2);
					
		//SET LAYOUT
		setLayout(new BorderLayout());
		
		//ADD TOOL BAR ITEMS AND SET TOOL BAR
		hometoolBar.add(homeButton);
		hometoolBar.add(aboutButton);
		add(hometoolBar, BorderLayout.NORTH);
		
		//ADD HOME PANEL
		add(homePanel, BorderLayout.CENTER);
	}
	
	//ADD ACTIONS
	private void addActions() {
		//SET PROGRAM TO TERMINATE WHEN 'X' IS CLICKED
		addWindowListener(new WindowAdapter() {
			public void windowClosing(WindowEvent we) {
				dispose();
				System.exit(0);
			}
		});
		
		//NETWORKING: PARSE CONFIG FILE TO DETERMINE SERVER HOSTNAME AND PORT
		BufferedReader buffered_reader = null;
				
		try{
			String line;
			buffered_reader = new BufferedReader(new FileReader("resources/client_server_configuration/Client.config"));

			while ((line = buffered_reader.readLine()) != null) {
				if (line.startsWith("ServerIP")){
					String[] parts = line.split("\\:");
					hostname = parts[1].replaceAll("\\s","");
				}
				if (line.startsWith("Port")){
					String[] parts = line.split("\\:");
					port = Integer.parseInt(parts[1].replaceAll("\\s",""));
				}
			}
		} catch(IOException exception){
			exception.printStackTrace();
		} finally{
			try{
				if (buffered_reader != null){
					buffered_reader.close();
				}
			} catch(IOException exception){
				exception.printStackTrace();
			}
		}	
		
		//HOME BUTTON
		homeButton.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent ae){
				loggedIn = false;
				//IF ABOUT PANEL IS VISIBLE
				if (aboutPanel.isDisplayable()){
					remove(aboutPanel);
					add(homePanel, BorderLayout.CENTER);
					repaint();
					revalidate();
				}
				//ELSE IF LOGIN PANEL IS VISIBLE
				else if (loginPanel.isDisplayable()){
					remove(loginPanel);
					add(homePanel, BorderLayout.CENTER);
					repaint();
					revalidate();
				}
				//ELSE IF REGISTER PANEL IS VISIBLE
				else if (registerPanel.isDisplayable()){
					remove(registerPanel);
					add(homePanel, BorderLayout.CENTER);
					repaint();
					revalidate();
				}
				//ELSE IF VALIDATION PANEL IS VISIBLE
				else if (validationPanel.isDisplayable()){
					remove(validationPanel);
					add(homePanel, BorderLayout.CENTER);
					repaint();
					revalidate();
				}
			}
		});
		
		//ABOUT BUTTON
		aboutButton.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent ae){
				loggedIn = false;
				//IF HOME PANEL IS VISIBLE
				if (homePanel.isDisplayable()){
					remove(homePanel);
					add(aboutPanel, BorderLayout.CENTER);
					repaint();
					revalidate();
				}
				//ELSE IF LOGIN PANEL IS VISIBLE
				else if (loginPanel.isDisplayable()){
					remove(loginPanel);
					add(aboutPanel, BorderLayout.CENTER);
					repaint();
					revalidate();
				}
				//ELSE IF REGISTER PANEL IS VISIBLE
				else if (registerPanel.isDisplayable()){
					remove(registerPanel);
					add(aboutPanel, BorderLayout.CENTER);
					repaint();
					revalidate();
				}
				//ELSE IF VALIDATION PANEL IS VISIBLE
				else if (validationPanel.isDisplayable()){
					remove(validationPanel);
					add(aboutPanel, BorderLayout.CENTER);
					repaint();
					revalidate();
				}
			}
		});
		
		//USERHOME BUTTON
		userhomeButton.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent ae){
				//IF PROFILE PANEL IS VISIBLE
				if (profilePanel.isDisplayable()){
					remove(profilePanel);
					add(userhomePanel, BorderLayout.CENTER);
					repaint();
					revalidate();
				}
			}
		});
		
		//LOGOUT BUTTON
		logoutButton.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent ae){
				loggedIn = false;
				remove(userhometoolBar);
				add(hometoolBar, BorderLayout.NORTH);
				
				//IF USERHOME PANEL IS VISIBLE
				if (userhomePanel.isDisplayable()){
					remove(userhomePanel);
					add(homePanel, BorderLayout.CENTER);
					repaint();
					revalidate();
				}
				//ELSE IF PROFILE PANEL IS VISIBLE
				if (profilePanel.isDisplayable()){
					remove(profilePanel);
					add(homePanel, BorderLayout.CENTER);
					repaint();
					revalidate();
				}
			}
		});
	}	
	
	//START CLIENT AND ATTEMPT TO CONNECT TO SERVER
	protected void startClient() {
		client = new Client(hostname, port, ClientGUI.this);
	}
}
