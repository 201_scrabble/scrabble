package CSCI201_FinalProject_Scrabble;

import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Random;

import javax.imageio.ImageIO;
import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JTextField;

public class RegisterPanel extends JPanel {
	public static final long serialVersionUID = 1;
	
	//CLIENT GUI INSTANCE
	private ClientGUI clientGUI;
	//REGISTER PANEL
	private JPanel logoPanel;
	private JLabel logoLabel;
	private JPanel containerPanel;
	private JPanel registerholderPanel;
	private GridBagConstraints gbc;
	private JLabel firstnameLabel;
	private JLabel lastnameLabel;
	private JLabel usernameLabel;
	private JLabel passwordLabel;
	private JLabel repeatpasswordLabel;
	protected JTextField firstnameTextField;
	protected JTextField lastnameTextField;
	protected JTextField usernameTextField;
	protected JPasswordField passwordPasswordField;
	protected JPasswordField repeatpasswordPasswordField;
	private JButton registerButton;
	//VALIDATION PASSPHRASE
	private String validationPassphrase;
	private static final String CHAR_LIST = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890";
	private static final int RANDOM_STRING_LENGTH = 10;
	
	//ADD BACKGROUND 
	protected void paintComponent(Graphics g) {
		Dimension size = this.getSize();
		g.drawImage(Toolkit.getDefaultToolkit().getImage("resources/img/backgrounds/blue_panel.png"), 0, 0, size.width, size.height, this);
	}
	
	//CONSTRUCTOR
	public RegisterPanel(ClientGUI clientGUI){	
		this.clientGUI = clientGUI;
		instantiateComponents();
		createGUI();
		addActions();
	}
	
	//INSTANTIATE COMPONENTS
	private void instantiateComponents(){
		//LOGO PANEL
		logoPanel = new JPanel() {
			private static final long serialVersionUID = 1L;
			
			protected void paintComponent(Graphics g) {				
				Dimension size = this.getSize();
				g.drawImage(Toolkit.getDefaultToolkit().getImage("resources/img/backgrounds/blue_panel.png"), 0, 0, size.width, size.height, this);
			}
		};
		BufferedImage logoImage = null;
		try {
			logoImage = ImageIO.read(new File("resources/img/logo/logo.png"));
		} catch (IOException ioe) {
			System.out.println("ioe: " + ioe.getMessage());
		}
		logoLabel = new JLabel(new ImageIcon(logoImage));
		
		//CONTAINER PANEL
		containerPanel = new JPanel() {
			private static final long serialVersionUID = 1L;

			protected void paintComponent(Graphics g) {
				Dimension size = this.getSize();
				g.drawImage(Toolkit.getDefaultToolkit().getImage("resources/img/backgrounds/blue_panel.png"), 0, 0, size.width, size.height, this);
			}
		};
		
		//REGISTER HOLDER PANEL
		registerholderPanel = new JPanel() {
			private static final long serialVersionUID = 1L;

			protected void paintComponent(Graphics g) {
				Dimension size = this.getSize();
				g.drawImage(Toolkit.getDefaultToolkit().getImage("resources/img/backgrounds/grey_panel.png"), 0, 0, size.width, size.height, this);
			} 
		};
		registerholderPanel.setLayout(new GridBagLayout());
		
		//REGISTER LABELS, TEXT FIELDS, AND BUTTON
		firstnameLabel = new JLabel("First Name:");
		lastnameLabel = new JLabel("Last Name:");
		usernameLabel = new JLabel("Username (Email):");
		passwordLabel = new JLabel("Password:");
		repeatpasswordLabel = new JLabel("Re-enter Password:");
		firstnameTextField = new JTextField(10);
		lastnameTextField = new JTextField(10);
		usernameTextField = new JTextField(10);
		passwordPasswordField = new JPasswordField(10);
		passwordPasswordField.setEchoChar('*');
		repeatpasswordPasswordField = new JPasswordField(10);
		repeatpasswordPasswordField.setEchoChar('*');
		registerButton = new CustomJButton("Register");
		
		//GRID BAG CONSTRAINTS
		gbc = new GridBagConstraints();
	}
	
	//CREATE GUI
	private void createGUI(){
		//SET LAYOUT
		setLayout(new BoxLayout(RegisterPanel.this, BoxLayout.Y_AXIS));
		
		//ADD COMPONENTS TO GRID BAG LAYOUT
		gbc.gridx = 0;
		gbc.gridy = 0;
		registerholderPanel.add(firstnameLabel, gbc);
		
		gbc.gridx = 1;
		gbc.gridy = 0;
		registerholderPanel.add(firstnameTextField, gbc);
				
		gbc.gridx = 0;
		gbc.gridy = 1;
		registerholderPanel.add(lastnameLabel, gbc);
		
		gbc.gridx = 1;
		gbc.gridy = 1;
		registerholderPanel.add(lastnameTextField, gbc);
		
		gbc.gridx = 0;
		gbc.gridy = 2;
		registerholderPanel.add(usernameLabel, gbc);
		
		gbc.gridx = 1;
		gbc.gridy = 2;
		registerholderPanel.add(usernameTextField, gbc);
		
		gbc.gridx = 0;
		gbc.gridy = 3;
		registerholderPanel.add(passwordLabel, gbc);
		
		gbc.gridx = 1;
		gbc.gridy = 3;
		registerholderPanel.add(passwordPasswordField, gbc);
		
		gbc.gridx = 0;
		gbc.gridy = 4;
		registerholderPanel.add(repeatpasswordLabel, gbc);
		
		gbc.gridx = 1;
		gbc.gridy = 4;
		registerholderPanel.add(repeatpasswordPasswordField, gbc);
		
		gbc.insets = new Insets(10,0,0,0); //top padding
		gbc.gridx = 0;
		gbc.gridy = 5;
		gbc.gridwidth = 2;
		registerholderPanel.add(registerButton, gbc);
		
		//ADD COMPONENTS TO REGISTER PANEL
		containerPanel.add(registerholderPanel);
		logoPanel.add(logoLabel);
		add(Box.createGlue());
		add(logoPanel);
		add(containerPanel);
		add(Box.createGlue());
	}
	
	//ADD ACTIONS 
	private void addActions(){
		//REGISTER BUTTON ACTION
		registerButton.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent ae){
				//CHECK IF INPUT FIELDS ARE BLANK
				String firstname = firstnameTextField.getText();
				String lastname = lastnameTextField.getText();
				String username = usernameTextField.getText();
				String password = new String(passwordPasswordField.getPassword());
				String repeatpassword = new String(repeatpasswordPasswordField.getPassword());
				
				if (firstname.equals("") || lastname.equals("") || username.equals("") || password.equals("") || repeatpassword.equals("")){
					JOptionPane.showMessageDialog(clientGUI, 
							"One or more input fields is blank." + "\n" + "Please try again.", 
							"Register Failed", JOptionPane.ERROR_MESSAGE);
					return;
				}
				
				//CHECK IF PASSWORDS MATCH
				if (!password.equals(repeatpassword)){
					JOptionPane.showMessageDialog(clientGUI, 
							"Passwords do not match." + "\n" + "Please try again.", 
							"Register Failed", JOptionPane.ERROR_MESSAGE);
					return;
				}
				
				//CHECK IF PASSWORD CONTAINS AT LEAST 1 UPEPRCASE LETTER AND 1 NUMBER
				int count_uppercase = 0;
				int count_number = 0;
				for (int i = 0; i < password.length(); i++){
					if (password.charAt(i) >= '0' && password.charAt(i) <= '9'){
						count_number++;
					}
					if (password.charAt(i) >= 'A' && password.charAt(i) <= 'Z'){
						count_uppercase++;
					}
				}
				if (!(count_uppercase >= 1 && count_number >= 1)){
					JOptionPane.showMessageDialog(clientGUI, 
							"Password must contain at least: \n 1-number 1-uppercase letter", 
							"Register Failed", JOptionPane.WARNING_MESSAGE);
					return;
				}
				
				//HASH PASSWORD USING MD5
				String hashedPassword = "";
		        try {
		            MessageDigest md = MessageDigest.getInstance("MD5");
		            md.update(password.getBytes()); 
		            byte[] bytes = md.digest();
		            StringBuilder sb = new StringBuilder();
		            for(int i=0; i< bytes.length ;i++) {
		                sb.append(Integer.toString((bytes[i] & 0xff) + 0x100, 16).substring(1));
		            }
		            hashedPassword = sb.toString();
		        } catch (NoSuchAlgorithmException e) {
		            e.printStackTrace();
		        }
		        
		        //START CLIENT AND ATTEMPT TO CONNECT TO SERVER
		        clientGUI.startClient();
				if(clientGUI.client.start()){
					clientGUI.loggedIn = true;
					clientGUI.validationPanel.passphraseTextField.setText("");
					validationPassphrase = generateRandomString();
					clientGUI.client.sendMessageServer(new NetworkingMessage("register", firstname + "|" + lastname + "|" + username + "|" + hashedPassword + "|" + validationPassphrase));
				}
				else {
					clientGUI.loggedIn = false;
					JOptionPane.showMessageDialog(clientGUI, 
							"Server cannot be reached." + "\n" + "Please check that the server is running and try again.", 
							"Register Failed", JOptionPane.WARNING_MESSAGE);
					return;
				}
			}
		});
	}
	
	//GENERATE VALIDATION PASSPHRASE
	public String generateRandomString(){     
		StringBuffer randStr = new StringBuffer();
	    for(int i = 0; i <RANDOM_STRING_LENGTH; i++){
	    	int number = getRandomNumber();
	        char ch = CHAR_LIST.charAt(number);
	        randStr.append(ch);
	    }
	    return randStr.toString();
	}
	 
	private int getRandomNumber() {
		int randomInt = 0;
	    Random randomGenerator = new Random();
	    randomInt = randomGenerator.nextInt(CHAR_LIST.length());
	    if (randomInt - 1 == -1) {
	    	return randomInt;
	    } else {
	    	return randomInt - 1;
	    }
	}
}
