package CSCI201_FinalProject_Scrabble;

import java.awt.Font;
import java.awt.FontFormatException;
import java.awt.GraphicsEnvironment;
import java.io.File;
import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.Enumeration;
import java.util.Vector;

import javax.swing.UIManager;
import javax.swing.plaf.FontUIResource;

public class Server {
	//SERVER COMPONENTS
	//SERVER GUI INSTANCE
	protected ServerGUI serverGUI;
	//SERVER PORT
	private int port;
	//VECTOR OF ALL CLIENT CONNECTIONS
	private Vector<ServerThread> serverthreadVector;
	//BOOLEAN THAT MAINTAINS WHETHER THE SERVER IS ON OR OFF
	private boolean serverStarted;
	
	//CONSTRUCTOR
	public Server(int port, ServerGUI serverGUI){
		//INITIALIZE SERVER COMPONENTS
		this.serverGUI = serverGUI;
		this.port = port;
		serverthreadVector = new Vector<ServerThread>();
	}
	
	public void start(){
		//SET SERVER STARTED BOOLEAN TO TRUE
		serverStarted = true;
		
		//CREATE SERVER SOCKET, WAIT FOR CONNECTION REQUESTS FROM CLIENTS
		ServerSocket serverSocket = null;
		try {
			serverSocket = new ServerSocket(port);
			serverGUI.addMessage("Server started on Port: " + port);
			
			//INFINITE LOOP TO WAIT FOR CLIENT CONNECTIONS
			while (serverStarted) {
				//ACCEPT CLIENT CONNECTION
				Socket socket = serverSocket.accept();
				
				if (!serverStarted){
					break;
				}
				
				//ADD CLIENT TO CLIENT CONNECTION VECTOR
				ServerThread st = new ServerThread(socket, this);
				serverthreadVector.add(st);
			}
			//SERVER STOPPED
			try {
				serverSocket.close();
				for (int i = 0; i < serverthreadVector.size(); i++){
					ServerThread st = serverthreadVector.get(i);
					try {
						st.serverInputStream.close();
						st.serverOutputStream.close();
						st.socket.close();
					} catch(IOException ioe){
					}
				}
			} catch (Exception e) {
				System.out.println("Error closing the server and clients: " + e.getMessage());
			}
		} catch (IOException ioe) {
			System.out.println("ioe: " + ioe.getMessage());
		}
	}
	
	protected void stop(){
		serverStarted = false;
		
		//CONNECT SERVER TO ITSELF AS CLIENT TO EXIT INFINITE LOOP
		Socket socket = null;
		try {
			socket = new Socket("localhost", port);
		} catch(Exception e) {
		} finally {
			try {
				socket.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}
	
	public void removeServerThread(ServerThread st){
		serverthreadVector.remove(st);
	}
	
	//FUNCTION TO SET CUSTOM FONT FOR ALL COMPONENTS
	public static void setUIFont(FontUIResource f) {
		Enumeration<Object> keys = UIManager.getDefaults().keys();
		while (keys.hasMoreElements()) {
			Object key = keys.nextElement();
			Object value = UIManager.get(key);
			if (value instanceof FontUIResource) {
				FontUIResource orig = (FontUIResource) value;
				Font font = new Font(f.getFontName(), orig.getStyle(), f.getSize());
				UIManager.put(key, new FontUIResource(font));
			}
		}
	}
	
	//MAIN METHOD TO CREATE SERVER GUI INSTANCE THAT WILL THEN CREATE SERVER TO ACCEPT CLIENT CONNECTIONS
	public static void main(String [] args) {
		//SET CROSS PLATFORM LOOK AND FEEL
		try {
			UIManager.setLookAndFeel(UIManager.getCrossPlatformLookAndFeelClassName());
		} catch (Exception e) {
			System.out.println("Warning! Cross-platform L&F not used!");
		}
			
		//SET CUSTOM FONT
		try {
			Font font = Font.createFont(Font.TRUETYPE_FONT, new File("resources/fonts/Quicksand-Regular.ttf"));
			font = font.deriveFont(Font.PLAIN, 16);
			GraphicsEnvironment ge = GraphicsEnvironment.getLocalGraphicsEnvironment();
			ge.registerFont(font);
			setUIFont(new FontUIResource(font));
		} catch (IOException|FontFormatException e) {
			e.printStackTrace();
		}
			
		//INSTANTIATE SERVER GUI INSTANCE
		new ServerGUI();
	}
}
