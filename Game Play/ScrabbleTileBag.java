import java.io.Serializable;
import java.util.ArrayList;
import java.util.Random;

// KEEPS TRACK OF ALL 100 TILES
//2 blank tiles (scoring 0 points)
//1 point: E ×12, A ×9, I ×9, O ×8, N ×6, R ×6, T ×6, L ×4, S ×4, U ×4
//2 points: D ×4, G ×3
//3 points: B ×2, C ×2, M ×2, P ×2
//4 points: F ×2, H ×2, V ×2, W ×2, Y ×2
//5 points: K ×1
//8 points: J ×1, X ×1
//10 points: Q ×1, Z ×1
//The total number of points is 187.
public class ScrabbleTileBag implements Serializable {

	private static final long serialVersionUID = 1L;
	private ArrayList<ScrabbleTile> tiles_list;
	
	public ScrabbleTileBag() {
		tiles_list = new ArrayList<ScrabbleTile>();
		for (int i = 0; i < 9; i++) tiles_list.add(new ScrabbleTile('A', 1, "images/A.jpg"));
		for (int i = 0; i < 2; i++) tiles_list.add(new ScrabbleTile('B', 3, "images/B.jpg"));
		for (int i = 0; i < 2; i++) tiles_list.add(new ScrabbleTile('C', 3, "images/C.jpg"));
		for (int i = 0; i < 4; i++) tiles_list.add(new ScrabbleTile('D', 2, "images/D.jpg"));
		for (int i = 0; i < 12; i++) tiles_list.add(new ScrabbleTile('E', 1, "images/E.jpg"));
		
		for (int i = 0; i < 2; i++) tiles_list.add(new ScrabbleTile('F', 4, "images/F.jpg"));
		for (int i = 0; i < 3; i++) tiles_list.add(new ScrabbleTile('G', 2, "images/G.jpg"));
		for (int i = 0; i < 2; i++) tiles_list.add(new ScrabbleTile('H', 4, "images/H.jpg"));
		for (int i = 0; i < 9; i++) tiles_list.add(new ScrabbleTile('I', 1, "images/I.jpg"));
		for (int i = 0; i < 1; i++) tiles_list.add(new ScrabbleTile('J', 8, "images/J.jpg"));
		
		for (int i = 0; i < 1; i++) tiles_list.add(new ScrabbleTile('K', 5, "images/K.jpg"));		
		for (int i = 0; i < 4; i++) tiles_list.add(new ScrabbleTile('L', 1, "images/L.jpg"));
		for (int i = 0; i < 2; i++) tiles_list.add(new ScrabbleTile('M', 3, "images/M.jpg"));
		for (int i = 0; i < 6; i++) tiles_list.add(new ScrabbleTile('N', 1, "images/N.jpg"));
		for (int i = 0; i < 8; i++) tiles_list.add(new ScrabbleTile('O', 1, "images/O.jpg"));
		
		for (int i = 0; i < 2; i++) tiles_list.add(new ScrabbleTile('P', 3, "images/P.jpg"));
		for (int i = 0; i < 1; i++) tiles_list.add(new ScrabbleTile('Q', 10, "images/Q.jpg"));
		for (int i = 0; i < 6; i++) tiles_list.add(new ScrabbleTile('R', 1, "images/R.jpg"));
		for (int i = 0; i < 4; i++) tiles_list.add(new ScrabbleTile('S', 1, "images/S.jpg"));
		for (int i = 0; i < 6; i++) tiles_list.add(new ScrabbleTile('T', 1, "images/T.jpg"));
		
		for (int i = 0; i < 4; i++) tiles_list.add(new ScrabbleTile('U', 1, "images/U.jpg"));
		for (int i = 0; i < 2; i++) tiles_list.add(new ScrabbleTile('V', 4, "images/V.jpg"));
		for (int i = 0; i < 2; i++) tiles_list.add(new ScrabbleTile('W', 4, "images/W.jpg"));
		for (int i = 0; i < 1; i++) tiles_list.add(new ScrabbleTile('X', 8, "images/X.jpg"));
		for (int i = 0; i < 2; i++) tiles_list.add(new ScrabbleTile('Y', 4, "images/Y.jpg"));
		
		for (int i = 0; i < 1; i++) tiles_list.add(new ScrabbleTile('Z', 10, "images/Z.jpg"));
		for (int i = 0; i < 2; i++) tiles_list.add(new ScrabbleTile('-', 0, "images/blankTile.png"));
	}
	
	public boolean isBagEmpty() {
		return tiles_list.isEmpty();
	}
	
	public ScrabbleTile requestATile() {
		// CHECKING IF BAG IS EMPTY FIRST
		if (this.isBagEmpty()) return null;
		// GENERATING RANDOM INDEX AND RETURNING THAT SCRABBLE TILE
		Random rand = new Random();
		int index = rand.nextInt(tiles_list.size());
		ScrabbleTile st = tiles_list.get(index);
		tiles_list.remove(index);
		return st;
	}
	
	public ArrayList<ScrabbleTile> dealTiles() {
		ArrayList<ScrabbleTile> deal = new ArrayList<ScrabbleTile>();
		Random rand = new Random();
		// WHILE THE SIZE OF DEAL IS NOT 7
		while (deal.size() != 7) {
			// GENERATE RANDOM TILE IN TILE BAG, ADD TO DEAL, AND REMOVE FROM TILE BAG
			int index = rand.nextInt(tiles_list.size());
			deal.add(tiles_list.get(index));
			tiles_list.remove(index);
		}
		return deal;		
	}
	
	public ArrayList<ScrabbleTile> replaceAllTiles(ArrayList<ScrabbleTile> original) {
		ArrayList<ScrabbleTile> replaced = new ArrayList<ScrabbleTile>();	
		// ADD ORIGINAL TILE RACK BACK INTO TILE RACK
		for (ScrabbleTile st : original)
			tiles_list.add(st);
		Random rand = new Random();
		// WHILE THE SIZE OF REPLACED IS NOT 7 AND THE BAG IS NOT EMPTY
		while (replaced.size() != 7 && !this.isBagEmpty()) {
			// GENERATE RANDOM TILE IN TILE BAG, ADD TO REPLACED, AND REMOVE FROM TILE BAG
			int index = rand.nextInt(tiles_list.size());
			replaced.add(tiles_list.get(index));
			tiles_list.remove(index);
		}
		return replaced;
	}
	
	// http://stackoverflow.com/questions/1519736/random-shuffling-of-an-array
	public ArrayList<ScrabbleTile> shuffleTiles(ArrayList<ScrabbleTile> array) {
	    int index;
	    ScrabbleTile temp = null;
	    Random random = new Random();
	    for (int i = array.size() - 1; i > 0; i--)
	    {
	        index = random.nextInt(i + 1);
	        //System.out.println("BEFORE i: " + i + " " + array.get(i).getLetter() + " index: " + index + " " + array.get(index).getLetter());
	        temp = array.get(index);
	        array.set(index, array.get(i));
	        array.set(i, temp);
	        //System.out.println("AFTER i: " + i + " " + array.get(i).getLetter() + " index: " + index + " " + array.get(index).getLetter());
	    }
	
	    return array;
	}
	
	// FOR TESTING PURPOSES
	public void printBag() {
		for (int i = 0; i < tiles_list.size(); i++) {
			System.out.println("Tile " + (i+1) + ": " + tiles_list.get(i).getLetter());
		}
	}
}

// INDIVIDUAL CLASS FOR EACH TILE
// KEEPS TRACK OF THE TILE'S CHARACTER, SCORE, AND IMG FILE NAME
class ScrabbleTile implements Serializable {

	private static final long serialVersionUID = 1L;
	private char letter;
	private int score;
	private String img;
	
	public ScrabbleTile(char letter, int score, String img) {
		this.letter = letter;
		this.score = score;
		this.img = img;
	}
	
	public char getLetter() {
		return letter;
	}
	
	public int getScore() {
		return score;
	}
	
	public String getImage() {
		return img;
	}
	
	public void setImage(String img) {
		this.img= img;
	}
	
	public void setChar(char c) {
		this.letter = c;
	}
}