
public class Location {
	int a;
	int b;
	
	public Location(int a, int b) {
		this.a = a;
		this.b = b;
	}
	
	public int getRow() {
		return a;
	}
	
	public int getCol() {
		return b;
	}
	
	public void setRow(int row) {
		a = row;
	}
	
	public void setCol(int col) {
		b = col;
	}
}
