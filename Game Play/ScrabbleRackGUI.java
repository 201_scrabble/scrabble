import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.Insets;
import java.awt.Toolkit;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.ArrayList;

import javax.swing.Box;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;

public class ScrabbleRackGUI extends JFrame {

	private static final long serialVersionUID = 1L;

	private JPanel jp;
	private ScrabbleTileBag bag; 
	private TileRackGUI rackOne;
	//private TileRackGUI rackTwo;
	private JButton dealButton;
	private JButton shuffleButton;
	private JButton replaceButton; 
	
	public ScrabbleRackGUI(ScrabbleTileBag bag) {
		super("Scrabble Rack GUI");
		this.bag = bag;
		
		instantiateComponents();
		createGUI();
		addActions();
	}
	
	public void instantiateComponents() {
		rackOne = new TileRackGUI(new ScrabbleBoardGUI());
		//rackTwo = new TileRackGUI();
		jp = new JPanel();
		dealButton = new JButton("Deal Tiles");
		shuffleButton = new JButton("Shuffle Tiles");
		replaceButton = new JButton("Replace Tiles");
	
		
	}
	
	public void createGUI() {
		setSize(500,500);
		setVisible(true);
	
		//add(rackTwo);
		
		jp.add(dealButton);
		jp.add(shuffleButton);
		jp.add(replaceButton);
		jp.add(rackOne);

		add(jp);
		repaint();
		revalidate();

	}
	
	public void addActions() {
		
		dealButton.addActionListener((event) -> {
			rackOne.setRack(bag.dealTiles());
			dealButton.setEnabled(false);
		});
		
		shuffleButton.addActionListener((event) -> {
			ArrayList<ScrabbleTile> shuffled = new ArrayList<ScrabbleTile>();
			ArrayList<ScrabbleTile> rack = rackOne.getTiles();
			for (ScrabbleTile st: rack) {
				shuffled.add(st);
			}
			shuffled = bag.shuffleTiles(shuffled);
			rackOne.resetRack(shuffled);
		});
		
		replaceButton.addActionListener((event) -> {
			//System.out.println("BEFORE");
			//bag.printBag();	
			rackOne.resetRack(bag.replaceAllTiles(rackOne.getTiles()));
			//System.out.println("AFTER");
			//bag.printBag();			
		});
		
	}
	
	public static void main(String[] args) {
		ScrabbleTileBag stb = new ScrabbleTileBag();
		ScrabbleRackGUI srgui = new ScrabbleRackGUI(stb);
	}
 }

class TileButton extends JButton {
	private static final long serialVersionUID = 1;
	//private Image tileImg;
	private ScrabbleTile tile;
	private boolean onBoard; 
	int x,y;
	
	public TileButton(ScrabbleTile tile) {
		super("");
		this.tile = tile;
        setBorder(null);
        setBorderPainted(false);
        setMargin( new Insets(0,0,0,0));
		ImageIcon icon = new ImageIcon(tile.getImage());
		setIcon(icon);
		onBoard = false;
		//tileImg = Toolkit.getDefaultToolkit().getImage(tile.getImage());
	}
	
	// DRAWING IMAGE ONTO THE BUTTON
//	protected void paintComponent(Graphics g) {
//		super.paintComponent(g);
//		setContentAreaFilled(false);
//		Dimension size = this.getSize();
//		g.drawImage(tileImg, 0, 0, size.width, size.height, null);
//		repaint();
//		revalidate();
//	}	

	public ScrabbleTile getTile() {
		return tile;
	}
	
	public boolean isOnBoard() {
		return onBoard;
	}
	
	public void setOnBoard(boolean onBoard) {
		this.onBoard = onBoard;
	}
	
	public void setXValue(int x) {
		this.x = x;
	}
	
	public void setYValue(int y) {
		this.y = y;
	}
	
	public int getXValue() {
		return x;
	}
	
	public int getYValue() {
		return y;
	}
}

class TileRackGUI extends JPanel {
	private ArrayList<TileButton> rack;
	private ArrayList<ScrabbleTile> tiles;
	private TileButton mostRecent; // should this be reset after shuffling/replacing rack???
	private ScrabbleBoardGUI sbg;
	
	public TileRackGUI(ScrabbleBoardGUI sbg) {
		super();
		rack = new ArrayList<TileButton>();
		tiles = new ArrayList<ScrabbleTile>();
		mostRecent = null;
		this.sbg = sbg;
	}
	
	public void setRack(ArrayList<ScrabbleTile> st) {
		for (ScrabbleTile tile : st) {
			TileButton tb = new TileButton(tile);
			rack.add(tb);
			tiles.add(tile);
			add(tb);
		}
		
		repaint();
		revalidate();
		for (TileButton button: rack)
			addActions(button);
	}
	
	public void resetRack(ArrayList<ScrabbleTile> st) {
		rack.clear();
		tiles.clear();
		this.removeAll();
		
		for (ScrabbleTile tile : st) {
			TileButton tb = new TileButton(tile);
			rack.add(tb);
			tiles.add(tile);
			add(tb);
		}
		repaint();
		revalidate();
		for (TileButton button: rack)
			addActions(button);
	}
	
	public void addActions(TileButton button) {
			button.addActionListener((event) -> {
				if (!button.isOnBoard()) {
					System.out.println("Button isn't on board!");
					mostRecent = button;
					sbg.undoSelection.setEnabled(true);
				} else if (sbg.savedBoard[button.getYValue()][button.getXValue()] == null){
					if (mostRecent == null) {
						System.out.println("Most recent is null");
						Object[] options = {"Move back", "Do not move back"};
						int val = JOptionPane.showOptionDialog(null, "Move this tile back to the tile rack?", "Clicked on scrabble tile on the board", JOptionPane.YES_NO_OPTION, 
								JOptionPane.QUESTION_MESSAGE, null, options, options[0]);
						if (val == JOptionPane.YES_OPTION) {
							System.out.println("Clicked on tile already on the board. Wants to move it back to rack");
							// get row and col of button
							int x = button.getXValue();
							int y = button.getYValue();
							
							SpaceType type = sbg.checkSpaceType(x, y);
							SquareButton sb = new SquareButton();
							ImageIcon icon = null;
							
							if (type == SpaceType.BLANK) {
								icon = new ImageIcon("images/blank.png");
							} else if (type == SpaceType.CENTER) {
								icon = new ImageIcon("images/centerTile.png");
							} else if (type == SpaceType.D_LETTER) {
								icon = new ImageIcon("images/doubleLetter.png");
							} else if (type == SpaceType.D_WORD) {
								icon = new ImageIcon("images/doubleWord.png");
							} else if (type == SpaceType.T_LETTER) {
								icon = new ImageIcon("images/tripleLetter.png");
							} else { // T_WORD
								icon = new ImageIcon("images/tripleWord.png");
							}
							sb.setIcon(icon);
							sb.setXValue(x);
							sb.setYValue(y);
							sbg.scrabbleBoardSquares[x][y] = sb;
							// set currentPlay[row][col] to null
							sbg.currentPlay[x][y] = null;
							
							if(button.getTile().getScore() == 0) {
								sbg.blankTileBoard[x][y] = null;
								button.getTile().setChar('-');
								ImageIcon icon2 = new ImageIcon("images/blankTile.png");
								button.setIcon(icon2);
								button.getTile().setImage("images/blankTile.png");
							}
							
							rack.add(button);
							tiles.add(button.getTile());
							add(button);
							button.setOnBoard(false);
							button.setXValue(0);
							button.setYValue(0);
							// RESET X AND Y OF BUTTON??????
							sbg.getscrabbleBoard().remove(button);
							sbg.gbc.gridx = x;
							sbg.gbc.gridy = y;
							sbg.getscrabbleBoard().add(sb, sbg.gbc);
							sbg.addActions(sb, x, y);
							sbg.getGui().repaint();
							sbg.getGui().validate();
							repaint();
							revalidate();
						}
					} else {
						System.out.println("most recent is not null?");
						Object[] options = {"Replace", "Do not replace"};
						int val = JOptionPane.showOptionDialog(null, "Replace this tile with the last selected tile?", "Clicked on scrabble tile on the board", JOptionPane.YES_NO_OPTION, 
								JOptionPane.QUESTION_MESSAGE, null, options, options[0]);
						if (val == JOptionPane.YES_OPTION) {
							System.out.println("Clicked on tile already on the board. Wants to replace it with another tile");
							// get the location of button 
							int x = button.getXValue();
							int y = button.getYValue();
							// remove button from scrabble board
							sbg.getscrabbleBoard().remove(button);
							
							if (mostRecent.getTile().getLetter() == '-') {
	    						Object[] options2 = {'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N',
	    								'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z'};
	    						char s = (char) JOptionPane.showInputDialog(null, "Choose a letter for your blank tile: ",
	    								"Select Letter", JOptionPane.PLAIN_MESSAGE, null, options2, 'A');
	    						if (s != ' ') {
	    							String image = "images/" + s + ".jpg";
	    							mostRecent.getTile().setImage(image);
	    							mostRecent.getTile().setChar(s);
	    							ImageIcon icon = new ImageIcon(image);
	    							mostRecent.setIcon(icon);
	    							repaint();
	    							revalidate();
	    							sbg.blankTileBoard[mostRecent.getXValue()][mostRecent.getYValue()] = "" + s;
	    						}
	    					}
								
							sbg.currentPlay[x][y] = mostRecent;
							// add mostrecent to scrabble board in that location
							sbg.gbc.gridx = x;
							sbg.gbc.gridy = y;
							sbg.getscrabbleBoard().add(mostRecent, sbg.gbc);
							// set mostrecent onBoard to true
							mostRecent.setOnBoard(true);
							
							// repaint/revalidate
							sbg.getGui().repaint();
							sbg.getGui().revalidate();
							
							
							if(button.getTile().getScore() == 0) {
								if (mostRecent.getTile().getScore() != 0) {
									sbg.blankTileBoard[x][y] = null;
								}
								button.getTile().setChar('-');
								ImageIcon icon2 = new ImageIcon("images/blankTile.png");
								button.setIcon(icon2);
								button.getTile().setImage("images/blankTile.png");
							}
							
							// ADD BUTTON BACK TO RACK
							rack.add(button);
							tiles.add(button.getTile());
							add(button);						
							button.setOnBoard(false);
							button.setXValue(0);
							button.setYValue(0);
							
							// REMOVE MOST RECENT FROM RACK
							rack.remove(mostRecent);
							tiles.remove(mostRecent.getTile());
							remove(mostRecent);
							mostRecent.setXValue(x);
							mostRecent.setYValue(y);
							
							resetMostRecentlyClicked();
							
							repaint();
							revalidate();
						}						
					}
					
				}
			});
		
	}
	
	public ArrayList<ScrabbleTile> getTiles() {
		return tiles;
	}
	
	public ArrayList<TileButton> getRack() {
		return rack;
	}
	
	public TileButton getMostRecentlyClicked() {
		return mostRecent;
	}
	
	public void resetMostRecentlyClicked() {
		mostRecent = null;
		sbg.undoSelection.setEnabled(false);
	}
}
