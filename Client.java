package CSCI201_FinalProject_Scrabble;

import java.awt.BorderLayout;
import java.awt.Font;
import java.awt.FontFormatException;
import java.awt.GraphicsEnvironment;
import java.io.File;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;
import java.util.Enumeration;

import javax.swing.ImageIcon;
import javax.swing.JOptionPane;
import javax.swing.UIManager;
import javax.swing.plaf.FontUIResource;

public class Client {

	//CLIENT COMPONENTS
	private ObjectInputStream clientInputStream;
	private ObjectOutputStream clientOutputStream;
	private Socket socket;
	private ClientGUI clientGUI;
	private String hostname;
	private int port;
	
	//CLIENT CONSTRUCTOR
	public Client(String hostname, int port, ClientGUI clientGUI) {	
		this.hostname = hostname;
		this.port = port;
		this.clientGUI = clientGUI;
	}
	
	//START CLIENT
	public boolean start(){
		//TRY TO CONNECT TO THE SERVER
		socket = null;
		try {
			//OPEN A SOCKET
			//System.out.println("Connecting to " + hostname + " on port " + port);
			socket = new Socket(hostname, port);
		} catch (Exception e) {
			//System.out.println("Error connecting to server");
			return false;
		}
		//System.out.println("Connection accepted " + socket.getInetAddress() + ":" + socket.getPort());
		
		//CREATE DATA STREAMS
		try {	
			clientOutputStream = new ObjectOutputStream(socket.getOutputStream());
			clientInputStream = new ObjectInputStream(socket.getInputStream());
		} catch (Exception e) {
			System.out.println("Error creating input/output streams: " + e);
			return false;
		}
		
		//CREATE THE THREAD TO LISTEN FROM THE SERVER 
		new ClientThread().start();
		return true;
	}
		
	//CHANGE CLIENT GUI ACCORDINGLY BASED ON RECEIVED MESSAGE TYPE
	public void sendMessageClientGUI(NetworkingMessage message){
		//LOGIN MESSAGE
		if (message.getMessageType().equals("login")){
			String[] parts = message.getMessage().split("\\|");
			String username = parts[0];
			String result = parts[1];
			
			//LOGIN CREDENTIALS ARE INVALID
			if (result.equals("failure")){
				JOptionPane.showMessageDialog(clientGUI, 
						"Username or password is invalid", 
						"Log-in Failed", JOptionPane.ERROR_MESSAGE);
				return;
			}
					
			//LOGIN CREDENTIALS ARE VALID
			//REMOVE LOGIN PANEL
			clientGUI.username = username;
			clientGUI.firstname = parts[3];
			clientGUI.lastname = parts[4];
			clientGUI.remove(clientGUI.loginPanel);
			
			//CHECK IF ACCOUNT HAS BEEN VALIDATED
			String accountvalidated = parts[2];
			if (accountvalidated.equals("false")){
				//ADD VALIDATION PANEL
				clientGUI.validationPanel.infoTextArea.setText("An email was sent to " + username + "\n" + "with the validation passphrase necessary"
						+ "\n" + "to complete your account creation.");
				clientGUI.add(clientGUI.validationPanel, BorderLayout.CENTER);
				clientGUI.revalidate();
				clientGUI.repaint();
			}
			else {
				//ADD USERHOME PANEL
				clientGUI.remove(clientGUI.hometoolBar);
				clientGUI.add(clientGUI.userhometoolBar, BorderLayout.NORTH);
				clientGUI.userhomePanel = new UserHomePanel(clientGUI);
				clientGUI.add(clientGUI.userhomePanel, BorderLayout.CENTER);
				clientGUI.revalidate();
				clientGUI.repaint();
			}
		}
		//REGISTER MESSAGE
		else if (message.getMessageType().equals("register")){
			String[] parts = message.getMessage().split("\\|");
			String username = parts[0];
			String result = parts[1];
			
			//REGISTER WAS INVALID
			if (result.equals("failure")){
				String reason = parts[2];
				if (reason.equals("username exists")){
					JOptionPane.showMessageDialog(clientGUI, 
							"Username is already taken", 
							"Register Failed", JOptionPane.ERROR_MESSAGE);
					return;
				}
				else if (reason.equals("email failure")){
					JOptionPane.showMessageDialog(clientGUI, 
							"Username is not a valid email address and/or you are not connected to the internet." + "\n" + "Please try again.", 
							"Register Failed", JOptionPane.ERROR_MESSAGE);
					return;
				}
			}
					
			//REGISTER WAS VALID
			//REMOVE REGISTER PANEL
			//ADD VALIDATION PANEL
			clientGUI.username = username;
			clientGUI.firstname = parts[2];
			clientGUI.lastname = parts[3];
			clientGUI.remove(clientGUI.registerPanel);
			clientGUI.validationPanel.infoTextArea.setText("An email was sent to " + username + "\n" + "with the validation passphrase necessary"
								+ "\n" + "to complete your account creation.");
			clientGUI.add(clientGUI.validationPanel, BorderLayout.CENTER);
			clientGUI.revalidate();
			clientGUI.repaint();
		}
		//VALIDATE MESSAGE
		else if (message.getMessageType().equals("validate")){
			String result = message.getMessage();
			
			//VALIDATION WAS INVALID
			if (result.equals("failure")){
				JOptionPane.showMessageDialog(clientGUI, 
							"Passphrase is invalid", 
							"Validation Failed", JOptionPane.ERROR_MESSAGE);
				return;
			}
					
			//VALIDATION WAS VALID
			//REMOVE VALIDATION PANEL
			//ADD USERHOME PANEL
			clientGUI.remove(clientGUI.hometoolBar);
			clientGUI.add(clientGUI.userhometoolBar, BorderLayout.NORTH);
			clientGUI.remove(clientGUI.validationPanel);
			clientGUI.userhomePanel = new UserHomePanel(clientGUI);
			clientGUI.add(clientGUI.userhomePanel, BorderLayout.CENTER);
			clientGUI.revalidate();
			clientGUI.repaint();
		}
		//RESEND EMAIL MESSAGE
		else if (message.getMessageType().equals("resendemail")){
			String result = message.getMessage();
					
			//RESEND EMAIL FAILED
			if (result.equals("failure")){
				JOptionPane.showMessageDialog(clientGUI, 
						"Username is not a valid email address and/or you are not connected to the internet." + "\n" + "Please try again.", 
						"Resend Email Failed", JOptionPane.ERROR_MESSAGE);
				return;
			}
			//RESEND EMAIL SUCCESS
			else {
				JOptionPane.showMessageDialog(clientGUI, 
						"An email was resent to " + clientGUI.username + "\n" + "with the validation passphrase necessary"
								+ "\n" + "to complete your account creation.", 
						"Resend Email Success", JOptionPane.INFORMATION_MESSAGE);
				return;
			}
		}
		//PROFILE MESSAGE
		else if (message.getMessageType().equals("profile")){
			String[] parts = message.getMessage().split("\\|");
			String type = parts[0];
			
			//CREATE PROFILE PANEL
			if (type.equals("create")){
				ImageIcon pictureIcon = message.getImageIcon();
				clientGUI.profilePanel.pictureIcon = pictureIcon;
				clientGUI.profilePanel.pictureLabel.setIcon(pictureIcon);
				//REMOVE USERHOME PANEL
				//ADD PROFILE PANEL
				clientGUI.remove(clientGUI.userhomePanel);
				clientGUI.add(clientGUI.profilePanel, BorderLayout.CENTER);
				clientGUI.revalidate();
				clientGUI.repaint();
			}
			//UPDATE NAME
			else if (type.equals("updatename")){
				String result = parts[1];
				String firstname = parts[2];
				String lastname = parts[3];
				
				if (result.equals("success")){
					clientGUI.firstname = firstname;
					clientGUI.lastname = lastname;
					clientGUI.userhomePanel.usernameLabel.setText("Welcome " + clientGUI.firstname + " " + clientGUI.lastname + "!");
					clientGUI.profilePanel.firstnameTextField.setText(firstname);
					clientGUI.profilePanel.lastnameTextField.setText(lastname);
					clientGUI.revalidate();
					clientGUI.repaint();
					JOptionPane.showMessageDialog(clientGUI, 
								"Information update was successful", 
							"Update Information Success", JOptionPane.INFORMATION_MESSAGE);
					return;
				}
			}
			//UPDATE PROFILE PICTURE
			else if (type.equals("updatepicture")){
				String result = parts[1];
				ImageIcon pictureIcon = message.getImageIcon();
				
				if (result.equals("success")){
					clientGUI.profilePanel.pictureIcon = pictureIcon;
					clientGUI.profilePanel.pictureLabel.setIcon(pictureIcon);
					clientGUI.revalidate();
					clientGUI.repaint();
					JOptionPane.showMessageDialog(clientGUI, "Profile picture update was successful", 
								"Update Profile Picture Success", JOptionPane.INFORMATION_MESSAGE);
					return;
				}
			}
		}
	}
	
	//SEND MESSAGE TO SERVER 
	public void sendMessageServer(NetworkingMessage message){
		try {
			clientOutputStream.writeObject(message);
			clientOutputStream.flush();
		} catch(IOException ioe) {
			System.out.println("Error sending message to server: " + ioe);
		}
	}
	
	//CLIENT THREAD CLASS THAT WAITS FOR A MESSAGE FROM THE SERVER
	class ClientThread extends Thread {
		public void run(){
			while (true){			
				try {
					NetworkingMessage message = (NetworkingMessage) clientInputStream.readObject();
					sendMessageClientGUI(message);
					//System.out.println(message.getMessage());//TESTING PURPOSES
				} catch (ClassNotFoundException cnfe) {
					System.out.println("cnfe: " + cnfe.getMessage());
				} catch (IOException ioe) {
					//System.out.println("server closed ioe: " + ioe.getMessage());
					clientGUI.loggedIn = false;
					break;
				}	
			}
		}
	}
	
	//FUNCTION TO SET CUSTOM FONT FOR ALL COMPONENTS
	public static void setUIFont(FontUIResource f) {
		Enumeration<Object> keys = UIManager.getDefaults().keys();
		while (keys.hasMoreElements()) {
			Object key = keys.nextElement();
			Object value = UIManager.get(key);
			if (value instanceof FontUIResource) {
			    FontUIResource orig = (FontUIResource) value;
			    Font font = new Font(f.getFontName(), orig.getStyle(), f.getSize());
			    UIManager.put(key, new FontUIResource(font));
			}
		}
	}
	
	//MAIN METHOD TO CREATE CLIENT GUI INSTANCE THAT WILL THEN CREATE CLIENT TO CONNECT TO SERVER
	public static void main(String [] args) {
		//SET CROSS PLATFORM LOOK AND FEEL
		try {
			UIManager.setLookAndFeel(UIManager.getCrossPlatformLookAndFeelClassName());
		} catch (Exception e) {
			System.out.println("Warning! Cross-platform L&F not used!");
		}
		
		//SET CUSTOM FONT
		try {
			Font font = Font.createFont(Font.TRUETYPE_FONT, new File("resources/fonts/Quicksand-Regular.ttf"));
			font = font.deriveFont(Font.PLAIN, 16);
			GraphicsEnvironment ge = GraphicsEnvironment.getLocalGraphicsEnvironment();
			ge.registerFont(font);
			setUIFont(new FontUIResource(font));
		} catch (IOException|FontFormatException e) {
			e.printStackTrace();
		}
		
		//INSTANTIATE CLIENT GUI INSTANCE
		new ClientGUI();
	}
}

