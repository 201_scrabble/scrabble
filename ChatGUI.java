package annieson_CSCI201L_Scrabble;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.FontFormatException;
import java.awt.GraphicsEnvironment;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.Enumeration;

import javax.imageio.ImageIO;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollBar;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.UIManager;
import javax.swing.plaf.FontUIResource;

public class ChatGUI extends JFrame {

	private static final long serialVersionUID = 1L;
	
	private JTextField sendJTF;
	private JTextArea messages;
	private JPanel topPanel, bottomPanel, centerPanel;
	private JLabel title, image;
	private YellowHoverButton sendButton;
	private JScrollPane scrollPane;
	
	private int yellowColor;
	private int blueColor;
	private Font scrabbleRegFont;
	private Font scrabbleBoldFont;
	
	
	public ChatGUI() {
		super("ChatGUI");
		setSize(600,500);
		setLocation(400,100);
		setLayout(new BorderLayout());
		
		getColor();
		setFont();
		GUIFont (new FontUIResource(scrabbleRegFont));
		instantiateVariables();
		createGUI();
		addActions();
	}
	
//	http://stackoverflow.com/questions/5824342/setting-the-global-font-for-a-java-application
	private static void GUIFont(FontUIResource fuir) {
	    Enumeration<Object> keys = UIManager.getDefaults().keys();
	    while (keys.hasMoreElements()) {
	        Object key = keys.nextElement();
	        Object val = UIManager.get(key);
	        if (val instanceof FontUIResource) {
	            UIManager.put(key, fuir);
	        }
	    }
	}
	
	private void instantiateVariables() {
		sendJTF = new JTextField(30);
		messages = new JTextArea();
		topPanel = new JPanel();
		bottomPanel = new JPanel();
		centerPanel = new JPanel();
		title = new JLabel("Chat with Courtney Vu");
		image = new JLabel();
		sendButton = new YellowHoverButton("Send");
	}
	
	private void createGUI() {
		
		scrollPane = new JScrollPane(messages);
//		messages.setFont(scrabbleRegFont);
		messages.setForeground(Color.WHITE);
		messages.setLineWrap(true);
		messages.setBackground(new Color(blueColor));
		JScrollBar vBar = scrollPane.getVerticalScrollBar();
		YellowScrollBarUI msbu = new YellowScrollBarUI();
		vBar.setUI(msbu);
		
		ImageIcon icon1 = new ImageIcon("img/totalgames/5sun.png"); 
		image.setIcon(icon1);
		
		topPanel.setBackground(new Color(yellowColor));
		bottomPanel.setBackground(new Color(yellowColor));
		title.setBackground(new Color(yellowColor));
		title.setFont(scrabbleBoldFont);
		title.setForeground(Color.WHITE);
		
		topPanel.add(title);
		topPanel.add(image);
		add(topPanel, BorderLayout.NORTH);
		
		
		add(scrollPane, BorderLayout.CENTER);

//		messages.setPreferredSize(new Dimension(500,500));
//		centerPanel.add(scrollPane);
//		add(centerPanel, BorderLayout.CENTER);
//		
		bottomPanel.add(sendJTF);
		bottomPanel.add(sendButton);
		add(bottomPanel,BorderLayout.SOUTH);
	}
	
	private void addActions() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	}
	
	private void setFont() {
		try {
            //create the font to use. Specify the size!
            scrabbleRegFont = Font.createFont(Font.TRUETYPE_FONT, new File("fonts/quicksand_regular.ttf")).deriveFont(16f);
            GraphicsEnvironment ge = GraphicsEnvironment.getLocalGraphicsEnvironment();
            ge.registerFont(scrabbleRegFont);
            
            scrabbleBoldFont = Font.createFont(Font.TRUETYPE_FONT, new File("fonts/quicksand_bold.ttf")).deriveFont(16f);
            GraphicsEnvironment ge2 = GraphicsEnvironment.getLocalGraphicsEnvironment();
            ge2.registerFont(scrabbleBoldFont);
       
    
        } catch (IOException ioe) {
            System.out.println(ioe.getMessage());
        }
        catch(FontFormatException ffe) {
        	System.out.println(ffe.getMessage());
        }       
	}
	
	private void getColor() {
		BufferedImage temp = null;
		try {
			temp = ImageIO.read(new File("img/scrollbar/yellowSlider.png"));
		} catch (IOException ioe) {
			System.out.println(ioe.getMessage());
		}
		yellowColor = temp.getRGB(20,20);
		
		BufferedImage temp2 = null;
		try {
			temp2 = ImageIO.read(new File("img/background/blueBackground.png"));
		} catch (IOException ioe) {
			System.out.println(ioe.getMessage());
		}
		blueColor = temp2.getRGB(0, 0);
	}
	
	
	public static void main(String[] args) {
		ChatGUI cgui = new ChatGUI();
		cgui.setVisible(true);
	}
}


	

