import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.awt.Insets;
import java.awt.Toolkit;
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;

import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.SwingUtilities;
import javax.swing.border.EmptyBorder;
import javax.swing.border.LineBorder;
import javax.swing.plaf.basic.BasicInternalFrameTitlePane.SystemMenuBar;

class CorrectTurn {
	private static final long serialVersionUID = 1L;
	
	private String currWord;
	private int currScore;
	
	public CorrectTurn(String currWord, int currScore) {
		this.currWord = currWord;
		this.currScore = currScore;
	}
	
	public void setCurrWord(String currWord) {
		this.currWord = currWord;
	}
	
	public void setCurrScore(int currScore) {
		this.currScore = currScore;
	}
	
	public String getCurrWord() {
		return currWord;
	}
	
	public int getCurrScore() {
		return currScore;
	}
}

class SquareButton extends JButton {
	
	private static final long serialVersionUID = 1L;
	private int x;
	private int y;
	
	public SquareButton() {
		super("");
        setBorder(null);
        setBorderPainted(false);
        setMargin(new Insets(0,0,0,0));
        setSize(new Dimension(300,300));
	}
	
	public void setXValue(int x) {
		this.x = x;
	}
	
	public void setYValue(int y) {
		this.y = y;
	}
	
	public int getXValue() {
		return x;
	}
	
	public int getYValue() {
		return y;
	}
	
}
// http://stackoverflow.com/questions/21077322/create-a-chess-board-with-jpanel
public class ScrabbleBoardGUI {

    private final JPanel gui = new JPanel() {
    	private static final long serialVersionUID = 1L;

		public void paintComponent(Graphics g) {
			g.drawImage(Toolkit.getDefaultToolkit().getImage("img/background/blueBackground.png"), 0, 0, this);
			
		}
    };
    
    
    private final JPanel gamePanel = new JPanel(new GridLayout(1,2));
    private final JPanel gameOptions = new JPanel();
    protected SquareButton[][] scrabbleBoardSquares = new SquareButton[15][15];
    protected TileButton[][] currentPlay = new TileButton[15][15]; // WHEN SHOULD THIS BE CLEARED? AFTER PLAY HAS BEEN SUBMITTED + ACCEPTED?
    protected String[][] savedBoard = new String[15][15];
    protected String[][] blankTileBoard = new String[15][15];
    private JPanel scrabbleBoard;
    private TileRackGUI tileRack;
    private ScrabbleTileBag bag = new ScrabbleTileBag();
    private YellowHoverButton score, askTeacher, saveBoard, dealTiles, shuffleTiles, replaceTiles, passTurn, playWord, endGame;
    protected YellowHoverButton undoSelection;
    protected GridBagConstraints gbc = new GridBagConstraints();
    private boolean isFirstTurn = true;
    private int totalScore = 0;
    private int numTurns = 0;
    private boolean bagIsEmpty = false;
    private int numPassedTurns = 0;
    private Constants constants = new Constants();
    
    public static Trie dictionary = new Trie();

    public ScrabbleBoardGUI() {
        initializeGUI();
    }

    public final void initializeGUI() {
        // set up the main GUI
    	gui.setLayout(new BorderLayout());
        gui.setBorder(new EmptyBorder(5, 5, 5, 5));

        scrabbleBoard = new JPanel(new GridBagLayout());
  
        scrabbleBoard.setBorder(new LineBorder(Color.BLACK));
        scrabbleBoard.setMaximumSize(new Dimension(100, 100));
        gamePanel.add(scrabbleBoard);

        // create the chess board squares
        Insets buttonMargin = new Insets(0,0,0,0);
        for (int ii = 0; ii < scrabbleBoardSquares.length; ii++) {
            for (int jj = 0; jj < scrabbleBoardSquares[ii].length; jj++) {
                SquareButton b = new SquareButton();
//                b.setBorder(null);
//                b.setBorderPainted(false);
//                b.setMargin(buttonMargin);
                // our chess pieces are 64x64 px in size, so we'll
                // 'fill this in' using a transparent icon..
                ImageIcon icon = null;
                if (ii == 7 && jj == 7)
                	icon = new ImageIcon("images/centerTile.png");
                else if ((ii == jj) && ((ii == 0) || (ii == 14)))
                	icon = new ImageIcon("images/tripleWord.png");
                else if ((ii == jj) && ((ii == 5) || (ii == 9)))
                	icon = new ImageIcon("images/tripleLetter.png");
                else if ((ii == jj) && ((ii == 6) || (ii == 8)))
                	icon = new ImageIcon("images/doubleLetter.png");
                else if (((14-ii) == jj) && (((14-ii) == 0) || ((14-ii) == 14)))
                	icon = new ImageIcon("images/tripleWord.png");
                else if (((14-ii) == jj) && (((14-ii) == 5) || ((14-ii) == 9)))
                	icon = new ImageIcon("images/tripleLetter.png");
                else if (((14-ii) == jj) && (((14-ii) == 6) || ((14-ii) == 8)))
                	icon = new ImageIcon("images/doubleLetter.png");
                else if (((ii == 0 || ii == 14) && (jj == 7)) || ((jj == 0 || jj == 14) && (ii == 7)))
                	icon = new ImageIcon("images/tripleWord.png");
                else if (((ii == 0 || ii == 14) && (jj == 3 || jj == 11)) || 
                		((jj == 0 || jj == 14) && (ii == 3 || ii == 11)))
                	icon = new ImageIcon("images/doubleLetter.png");
                else if (((ii == 1 || ii == 13) && (jj == 5 || (14-jj) == 5)) ||
                		((jj == 1 || jj == 13) && (ii == 5 || (14-ii) == 5)))
                	icon = new ImageIcon("images/tripleLetter.png");
                else if (((ii == 2 || ii == 12) && (jj == 6 || (14-jj) == 6)) ||
                		((jj == 2 || jj == 2) && (ii == 6 || (14-ii) == 6)))
                	icon = new ImageIcon("images/doubleLetter.png");
                else if (((ii == 7) && (jj == 3 || (14-jj) == 3)) ||
                		((jj == 7) && (ii == 3 || (14-ii) == 3)))
                	icon = new ImageIcon("images/doubleLetter.png");
                else if (ii == jj)
                	icon = new ImageIcon("images/doubleWord.png");
                else if ((14-ii) == jj)
                	icon = new ImageIcon("images/doubleWord.png");
                else
                	icon = new ImageIcon("images/blank.png");
                
                b.setIcon(icon);
                scrabbleBoardSquares[jj][ii] = b;
            }
        }

        // fill the black non-pawn piece row
//        gbc.fill = gbc.BOTH;
        gbc.anchor = GridBagConstraints.FIRST_LINE_START;
        gbc.weightx = 1.0;
        gbc.weighty = 1.0;
        gbc.fill = GridBagConstraints.BOTH;
        for (int ii = 0; ii < 15; ii++) {
        	gbc.gridx = ii;
            for (int jj = 0; jj < 15; jj++) {
            	gbc.gridy = jj;
            	
//            	System.out.println(ii + " " + jj);
            	scrabbleBoardSquares[jj][ii].setXValue(ii);
            	scrabbleBoardSquares[jj][ii].setYValue(jj);
            	scrabbleBoard.add(scrabbleBoardSquares[jj][ii], gbc);
            	
//            	System.out.println(scrabbleBoardSquares[jj][ii].getX() + " " + scrabbleBoardSquares[jj][ii].getY() );
//            	System.out.println(scrabbleBoardSquares[jj][ii].getWidth() + scrabbleBoardSquares[jj][ii].getHeight());
            }
        }
        
        for (int ii = 0; ii < 15; ii++) {
            for (int jj = 0; jj < 15; jj++) {
            	addActions(scrabbleBoardSquares[jj][ii], jj, ii);
            }
        }
        
        tileRack = new TileRackGUI(this);
    
        
        score = new YellowHoverButton("Score");
        askTeacher = new YellowHoverButton("Ask the Teacher");
        saveBoard = new YellowHoverButton("Save Board");
        dealTiles = new YellowHoverButton("Deal Tiles");
        shuffleTiles = new YellowHoverButton("Shuffle Tiles");
        replaceTiles = new YellowHoverButton("Replace Tiles");
        passTurn = new YellowHoverButton("Pass Turn");
        playWord = new YellowHoverButton("Play Word");
        endGame = new YellowHoverButton("End Game");
        undoSelection = new YellowHoverButton("Undo Selection of Tile");
        undoSelection.setEnabled(false);
        
        BoxLayout bl = new BoxLayout(gameOptions, BoxLayout.Y_AXIS);
        gameOptions.setLayout(bl);
        gameOptions.add(score);
        gameOptions.add(Box.createVerticalGlue());
        gameOptions.add(askTeacher);
        gameOptions.add(Box.createVerticalGlue());
        gameOptions.add(saveBoard);
        gameOptions.add(Box.createVerticalGlue());
        gameOptions.add(dealTiles);
        gameOptions.add(Box.createVerticalGlue());
        gameOptions.add(shuffleTiles);
        gameOptions.add(Box.createVerticalGlue());
        gameOptions.add(replaceTiles);
        gameOptions.add(Box.createVerticalGlue());
        gameOptions.add(passTurn);
        gameOptions.add(Box.createVerticalGlue());
        gameOptions.add(undoSelection);
        gameOptions.add(Box.createVerticalGlue());
        gameOptions.add(playWord);
        gameOptions.add(Box.createVerticalGlue());
        gameOptions.add(endGame);

        gamePanel.add(gameOptions);
        gui.add(gamePanel, BorderLayout.CENTER);
        gui.add(tileRack, BorderLayout.SOUTH);
        
        addActions();

    }
    
    public void addActions() {
		dealTiles.addActionListener((event) -> {
			tileRack.setRack(bag.dealTiles());
			dealTiles.setEnabled(false);
		});
		
		shuffleTiles.addActionListener((event) -> {
			ArrayList<ScrabbleTile> shuffled = new ArrayList<ScrabbleTile>();
			ArrayList<ScrabbleTile> rack = tileRack.getTiles();
			for (ScrabbleTile st: rack) {
				shuffled.add(st);
			}
			shuffled = bag.shuffleTiles(shuffled);
			tileRack.resetRack(shuffled);
		});
		
		replaceTiles.addActionListener((event) -> {
			//System.out.println("BEFORE");
			//bag.printBag();	
			tileRack.resetRack(bag.replaceAllTiles(tileRack.getTiles()));
			numTurns++;
			//System.out.println("AFTER");
			//bag.printBag();			
		});   	
		
		undoSelection.addActionListener((event) -> {
			tileRack.resetMostRecentlyClicked();
		});
		
		passTurn.addActionListener((event) -> {
			numPassedTurns++;
			if (numPassedTurns == 6) {
				JOptionPane.showConfirmDialog(null, "Game ended! Total Score: " + totalScore + " | Number of Turns: " + numTurns, "Game Over!", JOptionPane.DEFAULT_OPTION);
				gui.setVisible(false);
			}
		});
		
		playWord.addActionListener((event) -> {
			CorrectTurn ct = null;
			
			if (isFirstTurn) {
				ct = checkFirstTurnConfig();
			} else {
				printSavedBoard();
				ct = checkIfValidConfiguration();
			}
			
			
			//is word
			//fill in savedBoard
			if (ct != null) {
				numTurns++;
				numPassedTurns = 0;
				System.out.println("First turn: " + ct.getCurrWord() + " " + ct.getCurrScore());
				totalScore += ct.getCurrScore();
				JOptionPane.showConfirmDialog(null, "Played Word: " + ct.getCurrWord() + " | Word Score: " + ct.getCurrScore() + 
						" | Total Score: " + totalScore, "Successful Turn: " + numTurns, JOptionPane.DEFAULT_OPTION);
				currentPlay = null;
				currentPlay = new TileButton[15][15];
				if (isFirstTurn) {
					isFirstTurn = false;
				} else {
					
				}
				
				System.out.println("Tile rack size " + tileRack.getRack().size());
				if (!bagIsEmpty) {
					int add = 7-tileRack.getRack().size();
					for(int i = 0; i < add; i++) {
						System.out.println("adding tile");
						ScrabbleTile st = bag.requestATile();
						if (st != null) {
							TileButton tb = new TileButton(st);
							tileRack.getTiles().add(st);
							tileRack.getRack().add(tb);
							tileRack.add(tb);
							tileRack.addActions(tb);
							tileRack.repaint();
							tileRack.revalidate();
						} else {
							bagIsEmpty = true;
						}
					}
				} else if (bagIsEmpty && tileRack.getRack().size() == 0) {
					JOptionPane.showConfirmDialog(null, "Game ended! Total Score: " + totalScore + " | Number of Turns: " + numTurns, "Game Over!", JOptionPane.DEFAULT_OPTION);
					gui.setVisible(false);
				}
				//checking to see if it is a word
				
			} else { 
				System.out.println("incorrect configuration");
			}
		});
    }
   
    public void addActions(SquareButton button, int row, int col) {
    	
    	button.addActionListener((event) -> {
    		System.out.println("Adding action listeners: " + row + " " + col);
    		System.out.println(scrabbleBoard.getComponentAt(row, col) instanceof JButton);
//    		if (scrabbleBoard.getComponentAt(row, col) instanceof SquareButton) {
    		if (scrabbleBoard.getComponentAt(row,  col) instanceof JButton) {
   
//    			System.out.println(currentPlay[row][col]== null);
    			if (currentPlay[row][col] == null) {
    				System.out.println("action listener for " + row + " " + col);
    				TileButton mostRecent = tileRack.getMostRecentlyClicked();
    				if (mostRecent != null) {
    					if (mostRecent.getTile().getScore() == 0) {
    						Object[] options = {'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N',
    								'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z'};
    						char s = (char) JOptionPane.showInputDialog(null, "Choose a letter for your blank tile: ",
    								"Select Letter", JOptionPane.PLAIN_MESSAGE, null, options, 'A');
    						if (s != ' ') {
    							String image = "images/" + s + ".jpg";
    							mostRecent.getTile().setImage(image);
    							mostRecent.getTile().setChar(s);
    							ImageIcon icon = new ImageIcon(image);
    							mostRecent.setIcon(icon);
    							tileRack.repaint();
    							tileRack.revalidate();
    						
    							blankTileBoard[button.getYValue()][button.getXValue()] = "" + s;
    							System.out.println(button.getXValue() + " " + button.getYValue() + " " + s);
    						}
    					}
    					
	    				currentPlay[row][col] = mostRecent;
	    				mostRecent.setOnBoard(true);
	    				//Point p  = scrabbleBoard.getLocation();
	    				System.out.println("Adding most recently clicked to board: " + mostRecent.getTile().getLetter());
	    				
	    				gbc.gridx = button.getXValue();
	    				gbc.gridy = button.getYValue();
	    				System.out.println(button.getXValue() + " " + button.getYValue());
	    				System.out.println(gbc.gridx + " "  + gbc.gridy);
	    				
	    				mostRecent.setXValue(button.getXValue());
	    				mostRecent.setYValue(button.getYValue());
	    				
	    				
	    				scrabbleBoard.add(mostRecent, gbc);
	    				scrabbleBoard.remove(button);
	    				
	    				scrabbleBoardSquares[button.getXValue()][button.getYValue()] = null;
	   	    				
	    				//scrabbleBoard.setComponentZOrder(mostRecent, 200);
	    				gui.repaint();
	    				gui.revalidate();
	    				tileRack.resetMostRecentlyClicked();
	    				tileRack.remove(mostRecent);
	    				tileRack.getTiles().remove(mostRecent.getTile());
	    				tileRack.getRack().remove(mostRecent);
    				}
    			} else {
    				
    			}
    		} else {
    			
    		}
    	});
    }
    
    public final JComponent getscrabbleBoard() {
        return scrabbleBoard;
    }

    public final JComponent getGui() {
        return gui;
    }
    
    // TAKES IN ROW,COL
    // RETURNS SPACETYPE FROM OG SCRABBLE BOARD
    public SpaceType checkSpaceType(int ii, int jj) {
    	if (ii == 7 && jj == 7)
    		return SpaceType.CENTER;
    	else if ((ii == jj) && ((ii == 0) || (ii == 14)))
        	return SpaceType.T_WORD;
        else if ((ii == jj) && ((ii == 5) || (ii == 9)))
        	return SpaceType.T_LETTER;
        else if ((ii == jj) && ((ii == 6) || (ii == 8)))
        	return SpaceType.D_LETTER;
        else if (((14-ii) == jj) && (((14-ii) == 0) || ((14-ii) == 14)))
        	return SpaceType.T_WORD;
        else if (((14-ii) == jj) && (((14-ii) == 5) || ((14-ii) == 9)))
        	return SpaceType.T_LETTER;
        else if (((14-ii) == jj) && (((14-ii) == 6) || ((14-ii) == 8)))
        	return SpaceType.D_LETTER;
        else if (((ii == 0 || ii == 14) && (jj == 7)) || ((jj == 0 || jj == 14) && (ii == 7)))
        	return SpaceType.T_WORD;
        else if (((ii == 0 || ii == 14) && (jj == 3 || jj == 11)) || 
        		((jj == 0 || jj == 14) && (ii == 3 || ii == 11)))
        	return SpaceType.D_LETTER;
        else if (((ii == 1 || ii == 13) && (jj == 5 || (14-jj) == 5)) ||
        		((jj == 1 || jj == 13) && (ii == 5 || (14-ii) == 5)))
        	return SpaceType.T_LETTER;
        else if (((ii == 2 || ii == 12) && (jj == 6 || (14-jj) == 6)) ||
        		((jj == 2 || jj == 2) && (ii == 6 || (14-ii) == 6)))
        	return SpaceType.D_LETTER;
        else if (((ii == 7) && (jj == 3 || (14-jj) == 3)) ||
        		((jj == 7) && (ii == 3 || (14-ii) == 3)))
        	return SpaceType.D_LETTER;
        else if (ii == jj)
        	return SpaceType.D_WORD;
        else if ((14-ii) == jj)
        	return SpaceType.D_WORD;
        else
        	return SpaceType.BLANK;
    }
    
    public CorrectTurn checkFirstTurnConfig() {
    	ArrayList<Location> locs = new ArrayList<Location>();
    	boolean centerTileFilled = false;
    	for (int i = 0; i<15; i++) {
    		for (int j = 0; j<15; j++) {
    		
    			if (currentPlay[i][j] != null) {
    				if (i == 7 && j ==7) {
        				centerTileFilled = true;
        			}
    				locs.add(new Location(i,j));
    			}
    		}
    	}
    	if (!centerTileFilled) {
    		return null;
    	}
    	
    	if (locs.isEmpty()) return null;
    	
    	boolean sameRow = true;
    	boolean sameCol = true;
    	int curRow = locs.get(0).getRow();
    	int curCol = locs.get(0).getCol();
    	for (int i = 1; i < locs.size(); i++) {
    		if (curRow != locs.get(i).getRow())
    			sameRow = false;
    		if (curCol != locs.get(i).getCol())
    			sameCol = false;
    	}
    	
    	if (!sameRow && !sameCol) return null;
    	else {
    		if (sameRow && !sameCol) {
    			// SORTING IN COL ORDER
    			System.out.println("SAME ROW FOR FIRST TURN");
    			Location temp = null;
    			for (int i = 0; i < locs.size(); i++) {
    				for (int j = 1; j < locs.size()-i; j++) {
    					if (locs.get(j-1).getCol() > locs.get(j).getCol()) {
    						temp = locs.get(j-1);
    						locs.set(j-1, locs.get(i));
    						locs.set(j, temp);
    					}
    				}
    			}
    			
    			// CHECKING FOR GAPS
    			for (int i = 0; i < locs.size()-1; i++) {
    				if((locs.get(i).getCol()+1) == locs.get(i+1).getCol()) {
    					continue;
    				} else {
    					return null;
    				}
    			}
    			int score = 0;
    			String word = "";
    			boolean tripleW = false;
    			boolean doubleW = true;
    			for (int i = 0; i < locs.size(); i++) {
    				int r = locs.get(i).getRow();
    				int c = locs.get(i).getCol();
    				TileButton t = currentPlay[r][c];
    				word += t.getTile().getLetter(); 
    				if (t.getTile().getScore() == 0) {
    					score += 0;
    				}
    				// CHECK FOR BLANK TILE
    				else {
	    				SpaceType type = checkSpaceType(r,c);
						if (type == SpaceType.BLANK) {
							score += t.getTile().getScore();
						} else if (type == SpaceType.CENTER) {
							score += t.getTile().getScore();
							doubleW = true;
						} else if (type == SpaceType.D_LETTER) {
							score += 2*t.getTile().getScore();
						} else if (type == SpaceType.D_WORD) {
							score += t.getTile().getScore();
							doubleW = true;
						} else if (type == SpaceType.T_LETTER) {
							score += 3*t.getTile().getScore();
						} else { // T_WORD
							score += t.getTile().getScore();
							tripleW = true;
						}
    				}
    			}
    			if (doubleW) score = score*2;
    			if (tripleW) score = score*3;
    			
    			System.out.println("Checking: " + word.toLowerCase());
    			if (dictionary.contains(word.toLowerCase())) {
    				System.out.println("word is found in dictionary");
    				for (int i = 0; i < word.length(); i++) {
    					savedBoard[locs.get(i).getRow()][locs.get(i).getCol()] = ""+word.charAt(i);
    				}
    				return new CorrectTurn(word, score);
    			} else {
    				System.out.println("Word is NOT found in dictionary");
    				return null;
    			}
    			
    			
    		} else if (!sameRow && sameCol) {
    			System.out.println("SAME COL FOR FIRST TURN");
    			// SORTING IN ROW ORDER
    			Location temp = null;
    			for (int i = 0; i < locs.size(); i++) {
    				for (int j = 1; j < locs.size()-i; j++) {
    					if (locs.get(j-1).getRow() > locs.get(j).getRow()) {
    						temp = locs.get(j-1);
    						locs.set(j-1, locs.get(i));
    						locs.set(j, temp);
    					}
    				}
    			}
    			for (int i = 0; i < locs.size()-1; i++) {
    				if((locs.get(i).getRow()+1) == locs.get(i+1).getRow()) {
    					continue;
    				} else {
    					return null;
    				}
    			}
    			int score = 0;
    			String word = "";
    			boolean tripleW = false;
    			boolean doubleW = true;
    			for (int i = 0; i < locs.size(); i++) {
    				int r = locs.get(i).getRow();
    				int c = locs.get(i).getCol();
    				TileButton t = currentPlay[r][c];
    				word += t.getTile().getLetter();
    				if (t.getTile().getScore() == 0) {
    					
    				} else {
    				
	    				SpaceType type = checkSpaceType(r,c);
						if (type == SpaceType.BLANK) {
							score += t.getTile().getScore();
						} else if (type == SpaceType.CENTER) {
							score += t.getTile().getScore();
							doubleW = true;
						} else if (type == SpaceType.D_LETTER) {
							score += 2*t.getTile().getScore();
						} else if (type == SpaceType.D_WORD) {
							score += t.getTile().getScore();
							doubleW = true;
						} else if (type == SpaceType.T_LETTER) {
							score += 3*t.getTile().getScore();
						} else { // T_WORD
							score += t.getTile().getScore();
							tripleW = true;
						}
    				}
    			}
    			if (doubleW) score = score*2;
    			if (tripleW) score = score*3;
    			
    			System.out.println("Checking: " + word.toLowerCase());
    			if (dictionary.contains(word.toLowerCase())) {
    				System.out.println("word is found in dictionary");
    				for (int i = 0; i < word.length(); i++) {
    					savedBoard[locs.get(i).getRow()][locs.get(i).getCol()] = ""+word.charAt(i);
    				}
     				return new CorrectTurn(word, score);
    			} else {
    				System.out.println("word is NOT found in dictionary");
    				return null;
    			}
    			
    		} else { // WHAT IF THERE'S ONLY ONE TILE? THEN SAMEROW AND SAMECOL ARE BOTH TRUE
    			return null;
    		}
    	}
    }
     
    public CorrectTurn checkIfValidConfiguration() {
    	ArrayList<Location> locs = new ArrayList<Location>();
    	
    	for (int i = 0; i<15; i++) {
    		for (int j = 0; j<15; j++) {
    			if (currentPlay[i][j] != null) {
    				System.out.println("Adding to locs: " + currentPlay[i][j].getTile().getLetter());
    				locs.add(new Location(i,j));
    			}
    		}
    	}
    	
    	if (locs.isEmpty()) return null;
    	
    	
    	
    	boolean sameRow = true;
    	boolean sameCol = true;
    	int curRow = locs.get(0).getRow();
    	int curCol = locs.get(0).getCol();
    	for (int i = 1; i < locs.size(); i++) {
    		if (curRow != locs.get(i).getRow())
    			sameRow = false;
    		if (curCol != locs.get(i).getCol())
    			sameCol = false;
    	}
    	
    	if (!sameRow && !sameCol) { System.out.println("Not the same row or col, returning false"); return null; }
    	else {
    		if (sameRow && !sameCol) { System.out.println("HORIZONTAL WORD");
    			// SORING IN COL ORDER
    			Location temp = null;
    			for (int i = 0; i < locs.size(); i++) {
    				for (int j = 1; j < locs.size()-i; j++) {
    					if (locs.get(j-1).getCol() > locs.get(j).getCol()) {
    						temp = locs.get(j-1);
    						locs.set(j-1, locs.get(i));
    						locs.set(j, temp);
    					}
    				}
    			}
    			
    			// CHECKING FOR GAPS
    			ArrayList<Location> gaps = new ArrayList<Location>();
    			for (int i = 0; i < locs.size()-1; i++) {
    				if((locs.get(i).getCol()+1) == locs.get(i+1).getCol()) {
    					continue;
    				} else {
    					int diff = locs.get(i+1).getCol() - locs.get(i).getCol();
    					for (int j = 1; j < diff; j++) {
    						if (scrabbleBoardSquares[locs.get(i).getCol()+j][locs.get(i).getRow()] != null || 
    								savedBoard[locs.get(i).getRow()][locs.get(i).getCol()+j] == null) {
    							System.out.println(scrabbleBoardSquares[locs.get(i).getRow()][locs.get(i).getCol()+j] != null);
    							System.out.println(savedBoard[locs.get(i).getRow()][locs.get(i).getCol()+j] == null);
    							System.out.println("Invalid gap, returning false: " + locs.get(i).getRow() + " " + (locs.get(i).getCol()+j));
    							return null;
    						}
    						gaps.add(new Location(locs.get(i).getRow(),locs.get(i).getCol()+j));
    					}
    					return getHorizontalWord(locs, true, gaps, 0, null);
    					//return true; // NOTE: ONLY ACCOUNTS FOR ONE SET OF GAPS
    				}
    			}
    			
    			// IF STRAIGHT ROW W NO GAPS
    			for (int i = 0; i < locs.size(); i++) {
    				// CHECK N,S,W
    				if (i == 0) {
    					if(locs.get(i).getCol() != 0) { // W
    						//if (scrabbleBoardSquares[locs.get(i).getRow()][locs.get(i).getCol()-1] == null) {
    						if (savedBoard[locs.get(i).getRow()][locs.get(i).getCol()-1] != null) {
    							System.out.println("west");
    							return getHorizontalWord(locs, false, null, 4, new Location(locs.get(i).getRow(), locs.get(i).getCol()-1));
    						}
    					}
    				} if (i == locs.size()-1) { 
    					if(locs.get(i).getCol() != 14) { // E
    						//if (scrabbleBoardSquares[locs.get(i).getRow()][locs.get(i).getCol()+1] == null) {
    						if (savedBoard[locs.get(i).getRow()][locs.get(i).getCol()+1] != null) {
    							System.out.println("east");
    							return getHorizontalWord(locs, false, null, 2, new Location(locs.get(i).getRow(), locs.get(i).getCol()+1));
    					
    						}
    					}
    				} 
    				
					if (locs.get(i).getRow() != 0) { // N
						if (savedBoard[locs.get(i).getRow()-1][locs.get(i).getCol()] != null) {
							System.out.println("north");
							return getHorizontalWord(locs, false, null, 1, new Location(locs.get(i).getRow()-1, locs.get(i).getCol()));
						}
					}
					if (locs.get(i).getRow() != 14) { // S
						if (savedBoard[locs.get(i).getRow()+1][locs.get(i).getCol()] != null) {
							System.out.println("south");
							return getHorizontalWord(locs, false, null, 3, new Location(locs.get(i).getRow()+1, locs.get(i).getCol()));
						}
					}    				
    			}
    			
    			System.out.println("returning null at end of horizontal word");
    			return null;
    			
    		} else if (!sameRow && sameCol) { // VERTICAL WORD
    			// SORING IN ROW ORDER
    			Location temp = null;
    			for (int i = 0; i < locs.size(); i++) {
    				for (int j = 1; j < locs.size()-i; j++) {
    					if (locs.get(j-1).getRow() > locs.get(j).getRow()) {
    						temp = locs.get(j-1);
    						locs.set(j-1, locs.get(i));
    						locs.set(j, temp);
    					}
    				}
    			}
    			
    			ArrayList<Location> gaps = new ArrayList<Location>();
    			for (int i = 0; i < locs.size()-1; i++) {
    				if((locs.get(i).getRow()+1) == locs.get(i+1).getRow()) {
    					continue;
    				} else {
    					int diff = locs.get(i+1).getRow() - locs.get(i).getRow();
    					for (int j = 1; j < diff; j++) {
    						if (scrabbleBoardSquares[locs.get(i).getCol()][locs.get(i).getRow()+j] != null ||
    								savedBoard[locs.get(i).getRow()+j][locs.get(i).getCol()] == null) {
    							return null;
    						}
    						gaps.add(new Location(locs.get(i).getRow()+j,locs.get(i).getCol()));
    					}
    					return getVerticalWord(locs, true, gaps, 0, null);
    				}
    			}
    			
    			// IF STRAIGHT COL W NO GAPS
    			for (int i = 0; i < locs.size(); i++) {
    				// CHECK N,S,W
    				if (i == 0) {
    					if(locs.get(i).getRow() != 0) { // N
    						//if (scrabbleBoardSquares[locs.get(i).getRow()-1][locs.get(i).getCol()] == null) {
    						if (savedBoard[locs.get(i).getRow()-1][locs.get(i).getCol()] != null) {
    							System.out.println("north");
    							return getVerticalWord(locs, false, null, 1, new Location(locs.get(i).getRow()-1, locs.get(i).getCol()));
    						}
    					}
    				} if (i == locs.size()-1) { 
    					if(locs.get(i).getRow() != 14) { // S
    						//if (scrabbleBoardSquares[locs.get(i).getRow()+1][locs.get(i).getCol()] == null) {
    						if (savedBoard[locs.get(i).getRow()+1][locs.get(i).getCol()] != null) {
    							System.out.println("south");
    							return getVerticalWord(locs, false, null, 3, new Location(locs.get(i).getRow()+1, locs.get(i).getCol()));
    						}
    					}
    				} 
    				
					if (locs.get(i).getCol() != 0) { // W
						if (savedBoard[locs.get(i).getRow()][locs.get(i).getCol()-1] != null) {
							System.out.println("west");
							return getVerticalWord(locs, false, null, 4, new Location(locs.get(i).getRow(), locs.get(i).getCol()-1));
						}
					}
					if (locs.get(i).getCol() != 14) { // E
						if (savedBoard[locs.get(i).getRow()][locs.get(i).getCol()+1] != null) {
							System.out.println("east");
							return getVerticalWord(locs, false, null, 2, new Location(locs.get(i).getRow(), locs.get(i).getCol()+1));
						}
					}    				
    			}
    			
    			return null;
    			
    		} else { // WHAT IF THERE'S ONLY ONE TILE? THEN SAMEROW AND SAMECOL ARE BOTH TRUE
    			return new CorrectTurn("",0);
    		}
    	}
    }
    
    //public CorrectTurn getHorizontalWord(ArrayList<Location> locs, boolean hasGaps, ArrayList<Location> gaps, int direction, Location connected) {
    public CorrectTurn getHorizontalWord(ArrayList<Location> locs, boolean hasGaps, ArrayList<Location> gaps, int direction, Location connected) {
    	int length = locs.get(0).getCol()-locs.get(locs.size()-1).getCol();
    	String word = "";

    	if (hasGaps) {
    		int gaps_index = 0;
    		ArrayList<Location> letter_locs = new ArrayList<Location>();
			for (int i = 0; i < locs.size()-1; i++) {
				if((locs.get(i).getCol()+1) == locs.get(i+1).getCol()) {
					letter_locs.add(locs.get(i));
					word += currentPlay[locs.get(i).getRow()][locs.get(i).getCol()].getTile().getLetter();
				} else {
					letter_locs.add(locs.get(i));
					word += currentPlay[locs.get(i).getRow()][locs.get(i).getCol()].getTile().getLetter();
					int diff = locs.get(i+1).getCol() - locs.get(i).getCol()-1;
					System.out.println("difference: " + diff);
					
					for (int j = gaps_index; j < diff; j++) {
						System.out.println("Accessing savedBoard: " + gaps.get(j).getRow() + " " +  gaps.get(j).getCol());
						letter_locs.add(gaps.get(j));
						word += savedBoard[gaps.get(j).getRow()][gaps.get(j).getCol()];
					}
					gaps_index += diff;
				}
			}
			// GET THE LAST LETTER
			word += currentPlay[locs.get(locs.size()-1).getRow()][locs.get(locs.size()-1).getCol()].getTile().getLetter();
			letter_locs.add(locs.get(locs.size()-1));
			System.out.println("Getting horizontal word with gap(s): " + word);
			
			if (dictionary.contains(word.toLowerCase())) {
				int score = calculateScore(word, letter_locs);
				for (int i = 0; i < word.length(); i++) {
					savedBoard[letter_locs.get(i).getRow()][letter_locs.get(i).getCol()] = ""+word.charAt(i);
				}
				return new CorrectTurn(word, score);
			} else {
				return null;
			}
    	} else {
    		if (direction == 1) { // NORTH 
    			ArrayList<Location> letter_locs = new ArrayList<Location>();
    			int totalS = 0;
    			for (int i = 0; i < locs.size(); i++) {
    				letter_locs.add(locs.get(i));
    				word += currentPlay[locs.get(i).getRow()][locs.get(i).getCol()].getTile().getLetter();
    			}
    			if (dictionary.contains(word.toLowerCase())) {
    				totalS += calculateScore(word, letter_locs);

    			} else {
    				return null;
    			} 
    			
    			ArrayList<Location> letter_locs2 = new ArrayList<Location>();
    			String word2 = "";
    			//word2 += currentPlay[connected.getRow()-1][connected.getCol()].getTile().getLetter();
    			letter_locs2.add(new Location(connected.getRow()+1,connected.getCol()));
    			int r = connected.getRow();
    			int c = connected.getCol();
    			while(savedBoard[r][c] != null && r >= 0) {
    				//word2 += savedBoard[r][c];
    				letter_locs2.add(0, new Location(r,c));			
    				r--;
    			}
    			
    			for (int i = 0; i < letter_locs2.size()-1; i++) {
    				word2 += savedBoard[letter_locs2.get(i).getRow()][letter_locs2.get(i).getCol()];
    			}
    			word2 += currentPlay[letter_locs2.get(letter_locs2.size()-1).getRow()][letter_locs2.get(letter_locs2.size()-1).getCol()].getTile().getLetter();
    			
    			if (dictionary.contains(word2.toLowerCase())) {
    				totalS += calculateScore(word2, letter_locs2);
    				for (int i = 0; i < word.length(); i++) {
    					savedBoard[letter_locs.get(i).getRow()][letter_locs.get(i).getCol()] = ""+word.charAt(i);
    				}
    				return new CorrectTurn(word, totalS);
    			} else {
    				return null;
    			} 
    			
    		} else if (direction == 2) { // EAST
    			ArrayList<Location> letter_locs = new ArrayList<Location>();
    			for (int i = 0; i < locs.size(); i++) {
    				letter_locs.add(locs.get(i));
    				word += currentPlay[locs.get(i).getRow()][locs.get(i).getCol()].getTile().getLetter();
    			}
    			int r = locs.get(0).getRow();
    			int c = locs.get(locs.size()-1).getCol()+1;
    			while(savedBoard[r][c] != null && c < 15) {
    				letter_locs.add(new Location(r,c));
    				word += savedBoard[r][c];
    				c++;
    			}
    			if (dictionary.contains(word.toLowerCase())) {
    				int score = calculateScore(word, letter_locs);
    				for (int i = 0; i < word.length(); i++) {
    					savedBoard[letter_locs.get(i).getRow()][letter_locs.get(i).getCol()] = ""+word.charAt(i);
    				}
    				return new CorrectTurn(word, score);
    			} else {
    				return null;
    			}
    		} else if (direction == 3) { // SOUTH
    			ArrayList<Location> letter_locs = new ArrayList<Location>();
    			int totalS = 0;
    			for (int i = 0; i < locs.size(); i++) {
    				letter_locs.add(locs.get(i));
    				word += currentPlay[locs.get(i).getRow()][locs.get(i).getCol()].getTile().getLetter();
    			}
    			if (dictionary.contains(word.toLowerCase())) {
    				totalS += calculateScore(word, letter_locs);

    			} else {
    				return null;
    			} 
    			
    			ArrayList<Location> letter_locs2 = new ArrayList<Location>();
    			String word2 = "";
    			word2 += currentPlay[connected.getRow()-1][connected.getCol()].getTile().getLetter();
    			letter_locs2.add(new Location(connected.getRow()-1,connected.getCol()));
    			int r = connected.getRow();
    			int c = connected.getCol();
    			while(savedBoard[r][c] != null && r < 15) {
    				word2 += savedBoard[r][c];
    				letter_locs2.add(new Location(r,c));			
    				r++;
    			}
    			
    			if (dictionary.contains(word2.toLowerCase())) {
    				totalS += calculateScore(word2, letter_locs2);
    				for (int i = 0; i < word.length(); i++) {
    					savedBoard[letter_locs.get(i).getRow()][letter_locs.get(i).getCol()] = ""+word.charAt(i);
    				}
    				return new CorrectTurn(word, totalS);
    			} else {
    				return null;
    			} 
    		} else { // WEST
    			ArrayList<Location> letter_locs = new ArrayList<Location>();
    			int r = locs.get(0).getRow();
    			int c = locs.get(0).getCol()-1;
    			while(savedBoard[r][c] != null && !(c < 0)) {
    				System.out.println("adding west tiles");
    				letter_locs.add(0, new Location(r,c));			
    				c--;
    			}
    				System.out.println("size of letter_locs: " + letter_locs.size());
    			for (int i = 0; i < letter_locs.size(); i++) {
    				word += savedBoard[letter_locs.get(i).getRow()][letter_locs.get(i).getCol()];
    			} System.out.println("Word is " + word);
    			
    			for (int i = 0; i < locs.size(); i++) {
    				letter_locs.add(locs.get(i));
    				word += currentPlay[locs.get(i).getRow()][locs.get(i).getCol()].getTile().getLetter();
    			}

    			if (dictionary.contains(word.toLowerCase())) {
    				int score = calculateScore(word, letter_locs);
    				for (int i = 0; i < word.length(); i++) {
    					savedBoard[letter_locs.get(i).getRow()][letter_locs.get(i).getCol()] = ""+word.charAt(i);
    				}
    				return new CorrectTurn(word, score);
    			} else {
    				return null;
    			}   			
    		}
    		
    	}

    }
    
    //public CorrectTurn getVerticalWord(ArrayList<Location> locs, boolean hasGaps, ArrayList<Location> gaps, int direction, Location connected) {
	public CorrectTurn getVerticalWord(ArrayList<Location> locs, boolean hasGaps, ArrayList<Location> gaps, int direction, Location connected) {
    	int length = locs.get(0).getRow() - locs.get(locs.size()-1).getRow();
    	String word = "";
    	
    	if (hasGaps) {
    		int gaps_index = 0;
    		ArrayList<Location> letter_locs = new ArrayList<Location>();
			for (int i = 0; i < locs.size()-1; i++) {
				if((locs.get(i).getRow()+1) == locs.get(i+1).getRow()) {
					letter_locs.add(locs.get(i));
					word += currentPlay[locs.get(i).getRow()][locs.get(i).getCol()].getTile().getLetter();
				} else {
					letter_locs.add(locs.get(i));
					word += currentPlay[locs.get(i).getRow()][locs.get(i).getCol()].getTile().getLetter();
					int diff = locs.get(i+1).getRow() - locs.get(i).getRow()-1;
					System.out.println("difference: " + diff);
					
					for (int j = gaps_index; j < diff; j++) {
						System.out.println("Accessing savedBoard: " + gaps.get(j).getRow() + " " +  gaps.get(j).getCol());
						letter_locs.add(gaps.get(j));
						word += savedBoard[gaps.get(j).getRow()][gaps.get(j).getCol()];
					}
					gaps_index += diff;
				}
			}
			// GET THE LAST LETTER
			word += currentPlay[locs.get(locs.size()-1).getRow()][locs.get(locs.size()-1).getCol()].getTile().getLetter();
			letter_locs.add(locs.get(locs.size()-1));
			System.out.println("Getting horizontal word with gap(s): " + word);
			
			if (dictionary.contains(word.toLowerCase())) {
				int score = calculateScore(word, letter_locs);
				for (int i = 0; i < word.length(); i++) {
					savedBoard[letter_locs.get(i).getRow()][letter_locs.get(i).getCol()] = ""+word.charAt(i);
				}
				return new CorrectTurn(word, score);
			} else {
				return null;
			}
    	} else {
    		if (direction == 1) { // NORTH 
    			ArrayList<Location> letter_locs = new ArrayList<Location>();
    			int r = locs.get(0).getRow()-1;
    			int c = locs.get(0).getCol();
    			while(savedBoard[r][c] != null && r >= 0) {
    				//System.out.println("adding north tiles");
    				letter_locs.add(0, new Location(r,c));			
    				r--;
    			}
    				//System.out.println("size of letter_locs: " + letter_locs.size());
    			for (int i = 0; i < letter_locs.size(); i++) {
    				word += savedBoard[letter_locs.get(i).getRow()][letter_locs.get(i).getCol()];
    			} System.out.println("Word is " + word);
    			
    			for (int i = 0; i < locs.size(); i++) {
    				letter_locs.add(locs.get(i));
    				word += currentPlay[locs.get(i).getRow()][locs.get(i).getCol()].getTile().getLetter();
    			}

    			if (dictionary.contains(word.toLowerCase())) {
    				int score = calculateScore(word, letter_locs);
    				for (int i = 0; i < word.length(); i++) {
    					savedBoard[letter_locs.get(i).getRow()][letter_locs.get(i).getCol()] = ""+word.charAt(i);
    				}
    				return new CorrectTurn(word, score);
    			} else {
    				return null;
    			}   
    		} else if (direction == 2) { // EAST
    			ArrayList<Location> letter_locs = new ArrayList<Location>();
    			int totalS = 0;
    			for (int i = 0; i < locs.size(); i++) {
    				letter_locs.add(locs.get(i));
    				word += currentPlay[locs.get(i).getRow()][locs.get(i).getCol()].getTile().getLetter();
    			}
    			if (dictionary.contains(word.toLowerCase())) {
    				totalS += calculateScore(word, letter_locs);

    			} else {
    				return null;
    			} 
    			
    			ArrayList<Location> letter_locs2 = new ArrayList<Location>();
    			String word2 = "";
    			word2 += currentPlay[connected.getRow()][connected.getCol()-1].getTile().getLetter();
    			letter_locs2.add(new Location(connected.getRow(),connected.getCol()-1));
    			int r = connected.getRow();
    			int c = connected.getCol();
    			while(savedBoard[r][c] != null && c < 15) {
    				word2 += savedBoard[r][c];
    				letter_locs2.add(new Location(r,c));			
    				c++;
    			}
    			
    			if (dictionary.contains(word2.toLowerCase())) {
    				totalS += calculateScore(word2, letter_locs2);
    				for (int i = 0; i < word.length(); i++) {
    					savedBoard[letter_locs.get(i).getRow()][letter_locs.get(i).getCol()] = ""+word.charAt(i);
    				}
    				return new CorrectTurn(word, totalS);
    			} else {
    				return null;
    			}  

    		} else if (direction == 3) { // SOUTH
    			ArrayList<Location> letter_locs = new ArrayList<Location>();
    			for (int i = 0; i < locs.size(); i++) {
    				letter_locs.add(locs.get(i));
    				word += currentPlay[locs.get(i).getRow()][locs.get(i).getCol()].getTile().getLetter();
    			}
    			int r = locs.get(locs.size()-1).getRow()+1;
    			int c = locs.get(0).getCol();
    			while(savedBoard[r][c] != null && r < 15) {
    				letter_locs.add(new Location(r,c));
    				word += savedBoard[r][c];
    				r++;
    			}
    			if (dictionary.contains(word.toLowerCase())) {
    				int score = calculateScore(word, letter_locs);
    				for (int i = 0; i < word.length(); i++) {
    					savedBoard[letter_locs.get(i).getRow()][letter_locs.get(i).getCol()] = ""+word.charAt(i);
    				}
    				return new CorrectTurn(word, score);
    			} else {
    				return null;
    			}
    		} else { // WEST
    			ArrayList<Location> letter_locs = new ArrayList<Location>();
    			int totalS = 0;
    			for (int i = 0; i < locs.size(); i++) {
    				letter_locs.add(locs.get(i));
    				word += currentPlay[locs.get(i).getRow()][locs.get(i).getCol()].getTile().getLetter();
    			}
    			if (dictionary.contains(word.toLowerCase())) {
    				totalS += calculateScore(word, letter_locs);

    			} else {
    				return null;
    			} 
    			
    			ArrayList<Location> letter_locs2 = new ArrayList<Location>();
    			String word2 = "";
    			//word2 += currentPlay[connected.getRow()-1][connected.getCol()].getTile().getLetter();
    			letter_locs2.add(new Location(connected.getRow(),connected.getCol()+1));
    			int r = connected.getRow();
    			int c = connected.getCol();
    			while(savedBoard[r][c] != null && c >=0) {
    				//word2 += savedBoard[r][c];
    				letter_locs2.add(0, new Location(r,c));			
    				c--;
    			}
    			
    			for (int i = 0; i < letter_locs2.size()-1; i++) {
    				word2 += savedBoard[letter_locs2.get(i).getRow()][letter_locs2.get(i).getCol()];
    			}
    			word2 += currentPlay[letter_locs2.get(letter_locs2.size()-1).getRow()][letter_locs2.get(letter_locs2.size()-1).getCol()].getTile().getLetter();
    			
    			if (dictionary.contains(word2.toLowerCase())) {
    				totalS += calculateScore(word2, letter_locs2);
    				for (int i = 0; i < word.length(); i++) {
    					savedBoard[letter_locs.get(i).getRow()][letter_locs.get(i).getCol()] = ""+word.charAt(i);
    				}
    				return new CorrectTurn(word, totalS);
    			} else {
    				return null;
    			} 			
    		}
    	}
    }
	
	public int calculateScore(String word, ArrayList<Location> letter_locs) {
		int score = 0;
		boolean tripleW = false;
		boolean doubleW = false;
		System.out.println("PRINTING OUT LETTER LOCS");
		for(int i = 0; i < letter_locs.size(); i++) {
			System.out.println(letter_locs.get(i).getRow() + " " + letter_locs.get(i).getCol());
		}
		System.out.println("word is: " + word);
		for (int i = 0; i < word.length(); i++) {
			int r = letter_locs.get(i).getRow();
			int c = letter_locs.get(i).getCol();
			// CHECK FOR BLANK TILE
			
			SpaceType type = checkSpaceType(r,c);
			if (blankTileBoard[r][c] == null) {
				System.out.println("Letter is: " + blankTileBoard[r][c]);
				
				if (type == SpaceType.BLANK) {
					score += constants.letterScores.get(""+word.charAt(i));
				} else if (type == SpaceType.D_LETTER && savedBoard[r][c] == null) {
					score += 2*constants.letterScores.get(""+word.charAt(i));
				} else if (type == SpaceType.D_WORD && savedBoard[r][c] == null) {
					score += constants.letterScores.get(""+word.charAt(i));
					doubleW = true;
				} else if (type == SpaceType.T_LETTER && savedBoard[r][c] == null) {
					score += 3*constants.letterScores.get(""+word.charAt(i));
				} else if (type == SpaceType.T_WORD && savedBoard[r][c] == null){ // T_WORD
					score += constants.letterScores.get(""+word.charAt(i));
					tripleW = true;
				} else {
					score += constants.letterScores.get(""+word.charAt(i));
				}
			} else {
				if (type == SpaceType.D_WORD && savedBoard[r][c] == null) {
					doubleW = true;
				} else if (type == SpaceType.T_WORD && savedBoard[r][c] == null) {
					tripleW = true;
				}
				
			}
		}
		if (doubleW) score = score*2;
		if (tripleW) score = score*3;
		
		return score;
	}
    
    // FOR TESTING PURPOSES
    public void printSavedBoard() {
    	System.out.println("PRINTING OUT SAVED BOARD");
    	for (int i = 0; i < 15; i++) {
    		for (int j = 0; j < 15; j++) {
    			if (savedBoard[i][j] != null)
    				System.out.println(i + " " + j + " " + savedBoard[i][j]);
    		}
    	}
    }

    public static void main(String[] args) {

    	FileReader wl_reader = null;
		try {
			wl_reader = new FileReader("dictionary.txt");
		} catch (FileNotFoundException f) {
			System.out.println("Unable to open word list file");
		}
		
		BufferedReader wl_buff = new BufferedReader(wl_reader);	
		String word = "";
		// reading in the first word
		try {
			word = wl_buff.readLine().toLowerCase();
		} catch (IOException io) {
			System.out.println("Unable to read buffer of word list");
		}
		// reading in the rest of the words and adding to trie
		while (word != null) {
			word = word.toLowerCase();
			//System.out.println(word);
			dictionary.insert(word);
			try {
				word = wl_buff.readLine();
			} catch (IOException io) {
				System.out.println("Unable to read buffer of word list");
			}	
		}
    	
        Runnable r = new Runnable() {

            @Override
            public void run() {
                ScrabbleBoardGUI cb =
                        new ScrabbleBoardGUI();

                JFrame f = new JFrame("Scrabble Board GUI");
                f.add(cb.getGui());
                f.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
                f.setLocationByPlatform(true);
                f.setSize(500,500);
                // ensures the frame is the minimum size it needs to be
                // in order display the components within it
                //f.pack();
                // ensures the minimum size is enforced.
                //f.setMinimumSize(f.getSize());
                f.setVisible(true);
            }
        };
        SwingUtilities.invokeLater(r);
    } 
}