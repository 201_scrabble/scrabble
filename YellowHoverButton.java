package annieson_CSCI201L_Scrabble;

import java.awt.Dimension;
import java.awt.Font;
import java.awt.FontFormatException;
import java.awt.FontMetrics;
import java.awt.Graphics;
import java.awt.GraphicsEnvironment;
import java.awt.Toolkit;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.File;
import java.io.IOException;

import javax.swing.JButton;

public class YellowHoverButton extends JButton {

	private static final long serialVersionUID = 1L;
	private boolean isHovering;
	private String buttonName;
	Font scrabbleBoldFont;
	
	public YellowHoverButton(String name) {
		super(name);
		buttonName = name;
		isHovering = false;
		this.setOpaque(false);
		this.setContentAreaFilled(false);
		try {
           
            
			scrabbleBoldFont = Font.createFont(Font.TRUETYPE_FONT, new File("fonts/quicksand_bold.ttf")).deriveFont(16f);
            GraphicsEnvironment ge2 = GraphicsEnvironment.getLocalGraphicsEnvironment();
            ge2.registerFont(scrabbleBoldFont);
            System.out.println("registered!");
            
        } catch (IOException ioe) {
            System.out.println(ioe.getMessage());
        }
        catch(FontFormatException ffe) {
        	System.out.println(ffe.getMessage());
        }       
		
		revalidate();
		repaint();
			
		this.addMouseListener(new MouseAdapter() {	
			public void mouseEntered(MouseEvent me) {
				isHovering = true;
			}
			public void mouseExited(MouseEvent me) {
				isHovering = false;
			}
		});
	}
	
	protected void paintComponent(Graphics g) {	
		super.paintComponent(g);
		revalidate();
		repaint();
		Dimension size = getSize();
		g.setFont(scrabbleBoldFont);
		FontMetrics fm = g.getFontMetrics(scrabbleBoldFont);
		int x = (this.getWidth() - fm.stringWidth(buttonName)) / 2;
		 
			int y = ((this.getHeight() - fm.getHeight()) / 2) + fm.getAscent();

		if (isHovering) {
			g.drawImage(Toolkit.getDefaultToolkit().getImage("img/buttons/buttonunclicked.png"), 0, 0, size.width, size.height, null);
			g.drawString(buttonName, x, y);
			revalidate();
			repaint();
		} 
		if (!isHovering) {
			g.drawImage(Toolkit.getDefaultToolkit().getImage("img/buttons/buttonhoveredwoutarrow.png"), 0, 0, size.width, size.height, null);
			g.drawString(buttonName, x, y);
			revalidate();
			repaint();
		}			
	}		
}
