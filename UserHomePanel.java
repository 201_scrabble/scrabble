package CSCI201_FinalProject_Scrabble;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

public class UserHomePanel extends JPanel {
	public static final long serialVersionUID = 1;
	
	//CLIENT GUI INSTANCE
	private ClientGUI clientGUI;
	//USERHOME PANEL
	private JPanel usernamePanel;
	protected JLabel usernameLabel;
	private JPanel containerPanel;
	private JPanel userhomeholderPanel;
	private GridBagConstraints gbc;  
	private JButton profileButton;
	private JButton friendsButton;
	private JButton settingsButton;
	private JButton newgameButton;
	
	//ADD BACKGROUND 
	protected void paintComponent(Graphics g) {
		Dimension size = this.getSize();
		g.drawImage(Toolkit.getDefaultToolkit().getImage("resources/img/backgrounds/blue_panel.png"), 0, 0, size.width, size.height, this);
	}
	
	//CONSTRUCTOR
	public UserHomePanel(ClientGUI clientGUI){	
		this.clientGUI = clientGUI;
		instantiateComponents();
		createGUI();
		addActions();
	}
	
	//INSTANTIATE COMPONENTS
	private void instantiateComponents(){
		//USERNAME PANEL
		usernamePanel = new JPanel() {
			private static final long serialVersionUID = 1L;
					
			protected void paintComponent(Graphics g) {				
				Dimension size = this.getSize();
				g.drawImage(Toolkit.getDefaultToolkit().getImage("resources/img/backgrounds/blue_panel.png"), 0, 0, size.width, size.height, this);
			}
		};
		usernameLabel = new JLabel("Welcome " + clientGUI.firstname + " " + clientGUI.lastname + "!");
		Font font = this.getFont().deriveFont(Font.BOLD, 48);
		usernameLabel.setFont(font);
		usernameLabel.setForeground(Color.white);
		
		//CONTAINER PANEL
		containerPanel = new JPanel() {
			private static final long serialVersionUID = 1L;

			protected void paintComponent(Graphics g) {
				Dimension size = this.getSize();
				g.drawImage(Toolkit.getDefaultToolkit().getImage("resources/img/backgrounds/blue_panel.png"), 0, 0, size.width, size.height, this);
			}
		};
		
		//USERHOME HOLDER PANEL
		userhomeholderPanel = new JPanel() {
			private static final long serialVersionUID = 1L;

			protected void paintComponent(Graphics g) {
				Dimension size = this.getSize();
				g.drawImage(Toolkit.getDefaultToolkit().getImage("resources/img/backgrounds/grey_panel.png"), 0, 0, size.width, size.height, this);
			} 
		};
		userhomeholderPanel.setLayout(new GridBagLayout());
		userhomeholderPanel.setBorder(new EmptyBorder(20, 20, 0, 20));
		
		//USERHOME BUTTONS
		profileButton = new CustomJButton("Profile");
		friendsButton = new CustomJButton("Friends");
		settingsButton = new CustomJButton("Settings");
		newgameButton = new CustomJButton("New Game");
		
		//GRID BAG CONSTRAINTS
		gbc = new GridBagConstraints();
		
		//INITIALIZE ALL PANELS ACCESSIBLE FROM USERHOME PANEL
		clientGUI.profilePanel = new ProfilePanel(clientGUI); 
	}
	
	//CREATE GUI
	private void createGUI(){
		//SET LAYOUT
		setLayout(new BoxLayout(UserHomePanel.this, BoxLayout.Y_AXIS));

		//ADD COMPONENTS TO GRID BAG LAYOUT
		gbc.insets = new Insets(0,0,20,0); //bottom padding
		gbc.gridx = 0;
		gbc.gridy = 0;
		userhomeholderPanel.add(profileButton, gbc);
		
		gbc.gridx = 0;
		gbc.gridy = 1;
		userhomeholderPanel.add(friendsButton, gbc);
		
		gbc.gridx = 0;
		gbc.gridy = 2;
		userhomeholderPanel.add(settingsButton, gbc);

		gbc.gridx = 0;
		gbc.gridy = 3;
		userhomeholderPanel.add(newgameButton, gbc);
		
		//ADD COMPONENTS TO USERHOME PANEL
		containerPanel.add(userhomeholderPanel);
		usernamePanel.add(usernameLabel);
		add(Box.createGlue());
		add(usernamePanel);
		add(containerPanel);
		add(Box.createGlue());
	}
	
	//ADD ACTIONS 
	private void addActions(){
		//PROFILE BUTTON ACTION
		profileButton.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent ae){
				//START CLIENT AND ATTEMPT TO CONNECT TO SERVER
		        clientGUI.startClient();
				if(clientGUI.client.start()){
					clientGUI.client.sendMessageServer(new NetworkingMessage("profile", clientGUI.username + "|create"));
				}
				else {
					clientGUI.loggedIn = false;
					JOptionPane.showMessageDialog(clientGUI, 
							"Server cannot be reached." + "\n" + "Please check that the server is running and try again.", 
							"Log-in Failed", JOptionPane.WARNING_MESSAGE);
					return;
				}
				
				//REMOVE USERHOME PANEL
				//ADD PROFILE PANEL
				clientGUI.profilePanel.firstnameTextField.setText(clientGUI.firstname);
				clientGUI.profilePanel.lastnameTextField.setText(clientGUI.lastname);
				clientGUI.remove(clientGUI.userhomePanel);
				clientGUI.add(clientGUI.profilePanel, BorderLayout.CENTER);
				clientGUI.revalidate();
				clientGUI.repaint();
			}
		});
		
		//FRIENDS BUTTON ACTION
		friendsButton.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent ae){
			}
		});
		
		//SETTINGS BUTTON ACTION
		settingsButton.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent ae){
			}
		});
		
		//NEW GAME BUTTON ACTION
		newgameButton.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent ae){
			}
		});
	}
}

