package CSCI201_FinalProject_Scrabble;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.image.BufferedImage;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import javax.imageio.ImageIO;
import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingUtilities;

import Scrabble_Game_Play.SinglePlayerGUI;
import Scrabble_Game_Play.Trie;

public class HomePanel extends JPanel {
	public static final long serialVersionUID = 1;
	
	//CLIENT GUI INSTANCE
	private ClientGUI clientGUI;
	//HOME PANEL
	private JPanel logoPanel;
	private JLabel logoLabel;
	private JPanel containerPanel;
	private JPanel buttonholderPanel;
	private GridBagConstraints gbc;
	private JButton loginButton;
	private JButton registerButton;
	private JButton guestButton;
	
	//ADD BACKGROUND 
	protected void paintComponent(Graphics g) {
		Dimension size = this.getSize();
		g.drawImage(Toolkit.getDefaultToolkit().getImage("resources/img/backgrounds/blue_panel.png"), 0, 0, size.width, size.height, this);
	}
	
	//CONSTRUCTOR
	public HomePanel(ClientGUI clientGUI){	
		this.clientGUI = clientGUI;
		instantiateComponents();
		createGUI();
		addActions();
	}
	
	//INSTANTIATE COMPONENTS
	private void instantiateComponents(){
		//LOGO PANEL
		logoPanel = new JPanel() {
			private static final long serialVersionUID = 1L;
			
			protected void paintComponent(Graphics g) {				
				Dimension size = this.getSize();
				g.drawImage(Toolkit.getDefaultToolkit().getImage("resources/img/backgrounds/blue_panel.png"), 0, 0, size.width, size.height, this);
			}
		};
		BufferedImage logoImage = null;
		try {
			logoImage = ImageIO.read(new File("resources/img/logo/logo.png"));
		} catch (IOException ioe) {
			System.out.println("ioe: " + ioe.getMessage());
		}
		logoLabel = new JLabel(new ImageIcon(logoImage));
		
		//CONTAINER PANEL
		containerPanel = new JPanel() {
			private static final long serialVersionUID = 1L;

			protected void paintComponent(Graphics g) {
				Dimension size = this.getSize();
				g.drawImage(Toolkit.getDefaultToolkit().getImage("resources/img/backgrounds/blue_panel.png"), 0, 0, size.width, size.height, this);
			}
		};
		
		//BUTTON HOLDER PANEL
		buttonholderPanel = new JPanel() {
			private static final long serialVersionUID = 1L;

			protected void paintComponent(Graphics g) {
				Dimension size = this.getSize();
				g.drawImage(Toolkit.getDefaultToolkit().getImage("resources/img/backgrounds/blue_panel.png"), 0, 0, size.width, size.height, this);
			} 
		};
		buttonholderPanel.setLayout(new GridBagLayout());
		
		//LOGIN, SIGNUP, OFFLINE BUTTONS
		loginButton = new CustomJButton("Login");
		registerButton = new CustomJButton("Register");
		guestButton = new CustomJButton("Continue as Guest");
		
		//GRID BAG CONSTRAINTS
		gbc = new GridBagConstraints();
	}
	
	//CREATE GUI
	private void createGUI(){
		//SET LAYOUT
		setLayout(new BoxLayout(HomePanel.this, BoxLayout.Y_AXIS));
		
		//ADD BUTTONS TO GRID BAG LAYOUT
		gbc.gridx = 0;
		gbc.gridy = 0;
		buttonholderPanel.add(loginButton, gbc);
		
		gbc.insets = new Insets(0,10,0,0); //left padding
		gbc.gridx = 1;
		gbc.gridy = 0;
		buttonholderPanel.add(registerButton, gbc);

		gbc.insets = new Insets(10,0,0,0); //top padding
		gbc.gridx = 0;
		gbc.gridy = 1;
		gbc.gridwidth = 2;
		buttonholderPanel.add(guestButton, gbc);
		
		//ADD COMPONENTS TO HOME PANEL
		containerPanel.add(buttonholderPanel);
		logoPanel.add(logoLabel);
		add(Box.createGlue());
		add(logoPanel);
		add(containerPanel);
		add(Box.createGlue());
	}
	
	//ADD ACTIONS 
	private void addActions(){
		//LOGIN BUTTON ACTION
		loginButton.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent ae){
				//REMOVE HOME PANEL AND ADD LOGIN PANEL TO MAIN JFRAME
				clientGUI.remove(clientGUI.homePanel);
				clientGUI.loginPanel.usernameTextField.setText("");
				clientGUI.loginPanel.passwordPasswordField.setText("");
				clientGUI.add(clientGUI.loginPanel, BorderLayout.CENTER);
				clientGUI.revalidate();
				clientGUI.repaint();
			}
		});
		
		//REGISTER BUTTON ACTION
		registerButton.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent ae){
				//REMOVE HOME PANEL AND ADD REGISTER PANEL TO MAIN JFRAME
				clientGUI.remove(clientGUI.homePanel);
				clientGUI.registerPanel.firstnameTextField.setText("");
				clientGUI.registerPanel.lastnameTextField.setText("");
				clientGUI.registerPanel.usernameTextField.setText("");
				clientGUI.registerPanel.passwordPasswordField.setText("");
				clientGUI.registerPanel.repeatpasswordPasswordField.setText("");
				clientGUI.add(clientGUI.registerPanel, BorderLayout.CENTER);
				clientGUI.revalidate();
				clientGUI.repaint();
			}
		});
		
		//GUEST BUTTON ACTION
		guestButton.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent ae){
				Trie dictionary = new Trie();
		    	FileReader wl_reader = null;
				try {
					wl_reader = new FileReader("dictionary.txt");
				} catch (FileNotFoundException f) {
					System.out.println("Unable to open word list file");
				}
				
				BufferedReader wl_buff = new BufferedReader(wl_reader);	
				String word = "";
				// reading in the first word
				try {
					word = wl_buff.readLine().toLowerCase();
				} catch (IOException io) {
					System.out.println("Unable to read buffer of word list");
				}
				// reading in the rest of the words and adding to trie
				while (word != null) {
					word = word.toLowerCase();
					//System.out.println(word);
					dictionary.insert(word);
					try {
						word = wl_buff.readLine();
					} catch (IOException io) {
						System.out.println("Unable to read buffer of word list");
					}	
				}
		    	
		        Runnable r = new Runnable() {

		            @Override
		            public void run() {
		                SinglePlayerGUI cb =
		                        new SinglePlayerGUI(dictionary);

		                JFrame f = new JFrame("Single Player");
		                f.add(cb.getGui());
		                f.pack();
		                f.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		                f.setLocationByPlatform(true);
		                f.setSize(750, 500);
		                f.setResizable(false);
		                // ensures the frame is the minimum size it needs to be
		                // in order display the components within it
		                //f.pack();
		                // ensures the minimum size is enforced.
		                //f.setMinimumSize(f.getSize());
		                f.setVisible(true);
		            }
		        };
		        SwingUtilities.invokeLater(r);
			}
		});
	}
}
