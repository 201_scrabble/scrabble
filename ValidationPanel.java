package CSCI201_FinalProject_Scrabble;

import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import javax.imageio.ImageIO;
import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextArea;
import javax.swing.JTextField;

public class ValidationPanel extends JPanel {
	
	public static final long serialVersionUID = 1;
	
	//CLIENT GUI INSTANCE
	private ClientGUI clientGUI;
	//VALIDATION PANEL
	private JPanel logoPanel;
	private JLabel logoLabel;
	private JPanel containerPanel;
	private JPanel validationholderPanel;
	private GridBagConstraints gbc;
	protected JTextArea infoTextArea;
	private JLabel passphraseLabel;
	protected JTextField passphraseTextField;  
	private JButton validateButton;
	private JButton resendemailButton;
	private JButton guestButton;
	
	//ADD BACKGROUND 
	protected void paintComponent(Graphics g) {
		Dimension size = this.getSize();
		g.drawImage(Toolkit.getDefaultToolkit().getImage("resources/img/backgrounds/blue_panel.png"), 0, 0, size.width, size.height, this);
	}
	
	//CONSTRUCTOR
	public ValidationPanel(ClientGUI clientGUI){	
		this.clientGUI = clientGUI;
		instantiateComponents();
		createGUI();
		addActions();
	}
	
	//INSTANTIATE COMPONENTS
	private void instantiateComponents(){
		//LOGO PANEL
		logoPanel = new JPanel() {
			private static final long serialVersionUID = 1L;
			
			protected void paintComponent(Graphics g) {				
				Dimension size = this.getSize();
				g.drawImage(Toolkit.getDefaultToolkit().getImage("resources/img/backgrounds/blue_panel.png"), 0, 0, size.width, size.height, this);
			}
		};
		BufferedImage logoImage = null;
		try {
			logoImage = ImageIO.read(new File("resources/img/logo/logo.png"));
		} catch (IOException ioe) {
			System.out.println("ioe: " + ioe.getMessage());
		}
		logoLabel = new JLabel(new ImageIcon(logoImage));
		
		//CONTAINER PANEL
		containerPanel = new JPanel() {
			private static final long serialVersionUID = 1L;

			protected void paintComponent(Graphics g) {
				Dimension size = this.getSize();
				g.drawImage(Toolkit.getDefaultToolkit().getImage("resources/img/backgrounds/blue_panel.png"), 0, 0, size.width, size.height, this);
			}
		};
		
		//VALIDATION HOLDER PANEL
		validationholderPanel = new JPanel() {
			private static final long serialVersionUID = 1L;

			protected void paintComponent(Graphics g) {
				Dimension size = this.getSize();
				g.drawImage(Toolkit.getDefaultToolkit().getImage("resources/img/backgrounds/grey_panel.png"), 0, 0, size.width, size.height, this);
			} 
		};
		validationholderPanel.setLayout(new GridBagLayout());
		
		//VALIDATION LABELS, TEXT FIELDS, AND BUTTONS
		infoTextArea = new JTextArea();
		infoTextArea.setEditable(false);
		passphraseLabel = new JLabel("Validation Passphrase:");
		passphraseTextField = new JTextField(10);
		validateButton = new CustomJButton("Validate");
		resendemailButton = new CustomJButton("Resend Email");
		guestButton = new CustomJButton("Continue as Guest");
		
		//GRID BAG CONSTRAINTS
		gbc = new GridBagConstraints();
	}
	
	//CREATE GUI
	private void createGUI(){
		//SET LAYOUT
		setLayout(new BoxLayout(ValidationPanel.this, BoxLayout.Y_AXIS));
		
		//ADD COMPONENTS TO GRID BAG LAYOUT
		gbc.insets = new Insets(0,0,10,0); //bottom padding
		gbc.gridx = 0;
		gbc.gridy = 0;
		gbc.gridwidth = 2;
		validationholderPanel.add(infoTextArea, gbc);
		
		gbc.gridx = 0;
		gbc.gridy = 1;
		validationholderPanel.add(passphraseLabel, gbc);
		
		gbc.gridx = 0;
		gbc.gridy = 2;
		validationholderPanel.add(passphraseTextField, gbc);
		
		gbc.insets = new Insets(10,0,0,0); //top padding
		gbc.gridx = 0;
		gbc.gridy = 3;
		gbc.gridwidth = 2;
		validationholderPanel.add(validateButton, gbc);
		
		gbc.gridx = 0;
		gbc.gridy = 4;
		gbc.gridwidth = 2;
		validationholderPanel.add(resendemailButton, gbc);
		
		gbc.gridx = 0;
		gbc.gridy = 5;
		gbc.gridwidth = 2;
		validationholderPanel.add(guestButton, gbc);
		
		//ADD COMPONENTS TO LOGIN PANEL
		containerPanel.add(validationholderPanel);
		logoPanel.add(logoLabel);
		add(Box.createGlue());
		add(logoPanel);
		add(containerPanel);
		add(Box.createGlue());
	}
	
	//ADD ACTIONS 
	private void addActions(){
		//VALIDATE BUTTON ACTION
		validateButton.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent ae){
				//CHECK IF INPUT FIELDS ARE BLANK
				String passphrase = passphraseTextField.getText();
				if (passphrase.equals("")){
					JOptionPane.showMessageDialog(clientGUI, 
							"One or more input fields is blank." + "\n" + "Please try again.", 
							"Validation Failed", JOptionPane.ERROR_MESSAGE);
					return;
				}
				
		        //START CLIENT AND ATTEMPT TO CONNECT TO SERVER
				if(clientGUI.client.start()){
					clientGUI.client.sendMessageServer(new NetworkingMessage("validate", clientGUI.username + "|" + passphrase));
				}
				else {
					JOptionPane.showMessageDialog(clientGUI, 
							"Server cannot be reached." + "\n" + "Please check that the server is running and try again.", 
							"Validation Failed", JOptionPane.WARNING_MESSAGE);
					return;
				}
			}
		});
		
		//RESEND EMAIL BUTTON ACTION
		resendemailButton.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent ae){
				//START CLIENT AND ATTEMPT TO CONNECT TO SERVER
				if(clientGUI.client.start()){
					clientGUI.client.sendMessageServer(new NetworkingMessage("resendemail", clientGUI.username));
				}
				else {
					JOptionPane.showMessageDialog(clientGUI, 
							"Server cannot be reached." + "\n" + "Please check that the server is running and try again.", 
							"Validation Failed", JOptionPane.WARNING_MESSAGE);
					return;
				}
			}
		});
		
		//GUEST BUTTON ACTION
		guestButton.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent ae){
			}
		});
	}
}

