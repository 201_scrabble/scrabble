package CSCI201_FinalProject_Scrabble;

import java.util.Date;
import java.util.Properties;
import javax.mail.Authenticator;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
 
public class EmailSender {
 
    public void sendEmail(String host, String port,
            				final String userName, final String password, String toAddress,
            				String subject, String message) throws AddressException,
            				MessagingException {
 
        //SET SMTP SERVER PROPERTIES
        Properties properties = new Properties();
        properties.put("mail.smtp.host", host);
        properties.put("mail.smtp.port", port);
        properties.put("mail.smtp.auth", "true");
        properties.put("mail.smtp.starttls.enable", "true");
 
        //CREATE A NEW SESSION WITH AN AUTHENTICATOR
        Authenticator auth = new Authenticator() {
            public PasswordAuthentication getPasswordAuthentication() {
                return new PasswordAuthentication(userName, password);
            }
        };
 
        Session session = Session.getInstance(properties, auth);
 
        //CREATE A NEW EMAIL MESSAGE
        Message msg = new MimeMessage(session);
 
        msg.setFrom(new InternetAddress(userName));
        InternetAddress[] toAddresses = { new InternetAddress(toAddress) };
        msg.setRecipients(Message.RecipientType.TO, toAddresses);
        msg.setSubject(subject);
        msg.setSentDate(new Date());
        
        //SEND STYLED EMAIL MESSAGE
        msg.setContent(message, "text/html");
        Transport.send(msg);
    }
 
    public boolean sendEmailToUser(String firstname, String lastname, String username, String validationPassphrase) {
        //SMTP SERVER INFORMATION
        String host = "smtp.gmail.com";
        String port = "587";
        String mailFrom = "csci201scrabble@gmail.com";
        String password = "USCScrabble!";
 
        //OUTGOING MESSAGE INFORMATION
        String mailTo = username;
        String subject = "Welcome to Scrabble!";
        String message = "<body style=\"margin: 0; padding: 0;\">"
								+ "<table align=\"center\" border=\"1\" cellpadding=\"0\" cellspacing=\"0\" width=\"600\">"
								+ "<tr> <td align=\"center\" bgcolor=\"#ee4c50\" style=\"padding: 40px 0 30px 0;\">"
								+ "<img src= \"http://web-vassets.ea.com/Assets/Resources/Others/scrabble-ipad-news-item-asset.png?cb=1412974383\" width=\"500\" height=\"230\" style=\"display: block;\" />"
								+ "</td> </tr>"
								+ "<tr> <td bgcolor=\"#ffffff\" style=\"padding: 40px 30px 40px 30px;\">"
								+ "<table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" width=\"100%\">"
								+ "<tr> <td> Welcome to Scrabble " + firstname + " " + lastname + "! </td> </tr>"
								+ "<tr> <td style=\"padding: 20px 0 30px 0;\"> We're so happy to welcome you to the Scrabble family."
								+ " Enter the following validation passphrase in your game client to complete your account creation."
								+ "<br/><br/> <b>Validation Passphrase: " + validationPassphrase + "</b> </td> </tr>"
								+ "<tr> <td> Happy gaming! </td> </tr>"
								+ "</table>"
								+ "</td> </tr>"
								+ "<tr> <td bgcolor=\"#70bbd9\" style=\"padding: 30px 30px 30px 30px;\">"
								+ "Scrabble Final Project <br/> CSCI 201, University of Southern California, Spring 2016 <br/>"
								+ "To be used for educational purposes only; no copyright infringement intended."
								+ "</td> </tr>"
								+ "</table>"
								+ "</body>";
        //SEND EMAIL
        try {
            sendEmail(host, port, mailFrom, password, mailTo, subject, message);
            return true;
        } catch (Exception ex) {
            return false;
        }
    }
}
