package CSCI201_FinalProject_Scrabble;

import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import javax.imageio.ImageIO;
import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JTextField;

public class LoginPanel extends JPanel {
	public static final long serialVersionUID = 1;
	
	//CLIENT GUI INSTANCE
	private ClientGUI clientGUI;
	//LOGIN PANEL
	private JPanel logoPanel;
	private JLabel logoLabel;
	private JPanel containerPanel;
	private JPanel loginholderPanel;
	private GridBagConstraints gbc;
	private JLabel usernameLabel;
	private JLabel passwordLabel;
	protected JTextField usernameTextField;
	protected JPasswordField passwordPasswordField;
	private JButton loginButton;
	private JButton forgotpasswordButton;
	
	//ADD BACKGROUND 
	protected void paintComponent(Graphics g) {
		Dimension size = this.getSize();
		g.drawImage(Toolkit.getDefaultToolkit().getImage("resources/img/backgrounds/blue_panel.png"), 0, 0, size.width, size.height, this);
	}
	
	//CONSTRUCTOR
	public LoginPanel(ClientGUI clientGUI){	
		this.clientGUI = clientGUI;
		instantiateComponents();
		createGUI();
		addActions();
	}
	
	//INSTANTIATE COMPONENTS
	private void instantiateComponents(){
		//LOGO PANEL
		logoPanel = new JPanel() {
			private static final long serialVersionUID = 1L;
			
			protected void paintComponent(Graphics g) {				
				Dimension size = this.getSize();
				g.drawImage(Toolkit.getDefaultToolkit().getImage("resources/img/backgrounds/blue_panel.png"), 0, 0, size.width, size.height, this);
			}
		};
		BufferedImage logoImage = null;
		try {
			logoImage = ImageIO.read(new File("resources/img/logo/logo.png"));
		} catch (IOException ioe) {
			System.out.println("ioe: " + ioe.getMessage());
		}
		logoLabel = new JLabel(new ImageIcon(logoImage));
		
		//CONTAINER PANEL
		containerPanel = new JPanel() {
			private static final long serialVersionUID = 1L;

			protected void paintComponent(Graphics g) {
				Dimension size = this.getSize();
				g.drawImage(Toolkit.getDefaultToolkit().getImage("resources/img/backgrounds/blue_panel.png"), 0, 0, size.width, size.height, this);
			}
		};
		
		//LOGIN HOLDER PANEL
		loginholderPanel = new JPanel() {
			private static final long serialVersionUID = 1L;

			protected void paintComponent(Graphics g) {
				Dimension size = this.getSize();
				g.drawImage(Toolkit.getDefaultToolkit().getImage("resources/img/backgrounds/grey_panel.png"), 0, 0, size.width, size.height, this);
			} 
		};
		loginholderPanel.setLayout(new GridBagLayout());
		
		//LOGIN LABELS, TEXT FIELDS, AND BUTTONS
		usernameLabel = new JLabel("Username (Email):");
		passwordLabel = new JLabel("Password:");
		usernameTextField = new JTextField(10);
		passwordPasswordField = new JPasswordField(10);
		passwordPasswordField.setEchoChar('*');
		loginButton = new CustomJButton("Login");
		forgotpasswordButton = new CustomJButton("Forgot Password");
		
		//GRID BAG CONSTRAINTS
		gbc = new GridBagConstraints();
	}
	
	//CREATE GUI
	private void createGUI(){
		//SET LAYOUT
		setLayout(new BoxLayout(LoginPanel.this, BoxLayout.Y_AXIS));
		
		//ADD COMPONENTS TO GRID BAG LAYOUT
		gbc.gridx = 0;
		gbc.gridy = 0;
		loginholderPanel.add(usernameLabel, gbc);
		
		gbc.gridx = 1;
		gbc.gridy = 0;
		loginholderPanel.add(usernameTextField, gbc);
				
		gbc.gridx = 0;
		gbc.gridy = 1;
		loginholderPanel.add(passwordLabel, gbc);
		
		gbc.gridx = 1;
		gbc.gridy = 1;
		loginholderPanel.add(passwordPasswordField, gbc);
		
		gbc.insets = new Insets(10,0,0,0); //top padding
		gbc.gridx = 0;
		gbc.gridy = 2;
		gbc.gridwidth = 2;
		loginholderPanel.add(loginButton, gbc);
		
		gbc.gridx = 0;
		gbc.gridy = 3;
		gbc.gridwidth = 2;
		loginholderPanel.add(forgotpasswordButton, gbc);
		
		//ADD COMPONENTS TO LOGIN PANEL
		containerPanel.add(loginholderPanel);
		logoPanel.add(logoLabel);
		add(Box.createGlue());
		add(logoPanel);
		add(containerPanel);
		add(Box.createGlue());
	}
	
	//ADD ACTIONS 
	private void addActions(){
		//LOGIN BUTTON ACTION
		loginButton.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent ae){
				//CHECK IF INPUT FIELDS ARE BLANK
				String username = usernameTextField.getText();
				String password = new String(passwordPasswordField.getPassword());
				if (username.equals("") || password.equals("")){
					JOptionPane.showMessageDialog(clientGUI, 
							"One or more input fields is blank." + "\n" + "Please try again.", 
							"Log-in Failed", JOptionPane.ERROR_MESSAGE);
					return;
				}
				
				//HASH PASSWORD USING MD5
				String hashedPassword = "";
		        try {
		            MessageDigest md = MessageDigest.getInstance("MD5");
		            md.update(password.getBytes()); 
		            byte[] bytes = md.digest();
		            StringBuilder sb = new StringBuilder();
		            for(int i=0; i< bytes.length ;i++) {
		                sb.append(Integer.toString((bytes[i] & 0xff) + 0x100, 16).substring(1));
		            }
		            hashedPassword = sb.toString();
		        } catch (NoSuchAlgorithmException e) {
		            e.printStackTrace();
		        }
		        
		        //START CLIENT AND ATTEMPT TO CONNECT TO SERVER
		        clientGUI.startClient();
				if(clientGUI.client.start()){
					clientGUI.loggedIn = true;
					clientGUI.validationPanel.passphraseTextField.setText("");
					clientGUI.client.sendMessageServer(new NetworkingMessage("login", username + "|" + hashedPassword));
				}
				else {
					clientGUI.loggedIn = false;
					JOptionPane.showMessageDialog(clientGUI, 
							"Server cannot be reached." + "\n" + "Please check that the server is running and try again.", 
							"Log-in Failed", JOptionPane.WARNING_MESSAGE);
					return;
				}
			}
		});
		
		//FORGOT PASSWORD BUTTON ACTION
		forgotpasswordButton.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent ae){
				JOptionPane.showInputDialog(clientGUI, "What is your username (email)?", "Forgot Password?", JOptionPane.QUESTION_MESSAGE);
				//get val not blank
			}
		});
	}
}
