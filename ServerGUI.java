package CSCI201_FinalProject_Scrabble;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.Toolkit;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;

import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JScrollBar;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;

public class ServerGUI extends JFrame{
	
	public static final long serialVersionUID = 1;
	
	//SERVER 
	private Server server;
	private int port;
	//SERVER GUI COMPONENTS
	private JTextArea textArea;
	private JScrollPane scrollPane;

	//CONSTRUCTOR
	public ServerGUI(){
		super("Server");
		instantiateComponents();
		createGUI();
		addActions();
		setVisible(true);
	}
	
	//INITIALIZE COMPONENTS
	private void instantiateComponents(){
		//TEXT AREA
		textArea = new JTextArea();
		textArea.setLineWrap(true);
		textArea.setWrapStyleWord(true);
		textArea.setEditable(false);
		//SCROLLPANE
		scrollPane = new JScrollPane(textArea);
		scrollPane.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_ALWAYS);
		//SCROLLBAR CUSTOMIZATION
		JScrollBar scrollBar = scrollPane.getVerticalScrollBar();
		scrollBar.setPreferredSize(new Dimension(25, Integer.MAX_VALUE));
		scrollBar.setUI(new CustomScrollBarUI());
	}
	
	//CREATE GUI
	private void createGUI(){
		//GET SCREEN SIZE
		Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
		int screenWidth = (int) screenSize.getWidth();
		int screenHeight = (int) screenSize.getHeight();
		
		//SET APPLICATION ICON
		ImageIcon imgIcon = new ImageIcon("resources/img/icon/servericon.png");
		setIconImage(imgIcon.getImage());
		
		//SET SIZE AND LOCATION
		setSize(1000,500);
		setMinimumSize(new Dimension(1000, 500));
		setLocation(screenWidth/2 + (screenWidth/2-1000)/2,(screenHeight-500)/2);
			
		//SET LAYOUT
		setLayout(new BorderLayout());
			
		//SCROLLPANE
		add(scrollPane, BorderLayout.CENTER);
	}
	
	//ADD ACTIONS
	private void addActions() {
		//SET PROGRAM TO TERMINATE WHEN 'X' IS CLICKED
		addWindowListener(new WindowAdapter() {
			public void windowClosing(WindowEvent we) {
				//IF SERVER EXISTS
				if(server != null) {
					try {
						//CLOSE SERVER CONNECTION
						server.stop();
					} catch(Exception e) {
					}
					server = null;
				}
				dispose();
				System.exit(0);
			}
		});
		
		//PARSE SERVER CONFIG FILE TO DETERMINE PORT
		BufferedReader buffered_reader = null;
	    
	    try{
			String line;
			buffered_reader = new BufferedReader(new FileReader("resources/client_server_configuration/Server.config"));

			while ((line = buffered_reader.readLine()) != null) {
				if (line.startsWith("Port")){
					String[] parts = line.split("\\:");
					port = Integer.parseInt(parts[1].replaceAll("\\s",""));
				}
			}
		} catch(IOException exception){
			exception.printStackTrace();
		} finally{
			try{
				if (buffered_reader != null){
					buffered_reader.close();
				}
			} catch(IOException exception){
				exception.printStackTrace();
			}
		}	
	    
	    //START SERVER (START ACCEPTING INCOMING CONNECTIONS) 
		server = new Server(port, ServerGUI.this);
		new ServerRunning().start();
	}
	
	//ADD MESSAGE TO TEXT AREA
	public void addMessage(String message) {
		if (textArea.getText() != null && textArea.getText().trim().length() > 0) {
			textArea.append("\n" + message);
			revalidate();
			repaint();
		}
		else {
			textArea.setText(message);
			revalidate();
			repaint();
		}
	}
	
	//THREAD TO RUN SERVER
	class ServerRunning extends Thread {
		//CONSTRUCTOR
		public ServerRunning(){
		}
			
		//INFINITE LOOP
		public void run() {
			server.start(); 
			server = null;
		}
	}
}
