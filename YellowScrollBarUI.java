package annieson_CSCI201L_Scrabble;

import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Rectangle;
import java.awt.Toolkit;

import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.plaf.basic.BasicScrollBarUI;

public class YellowScrollBarUI extends BasicScrollBarUI {
	
	public YellowScrollBarUI() {
	}

		@Override
		protected void paintThumb(Graphics g, JComponent jc, Rectangle thumbBound) {        
			g.drawImage(Toolkit.getDefaultToolkit().getImage("img/scrollbar/yellowSlider.png"), thumbBound.x, thumbBound.y, thumbBound.width, thumbBound.height, null);
	
		}

		@Override
		protected void paintTrack(Graphics g, JComponent jc, Rectangle trackBound) {        
			g.drawImage(Toolkit.getDefaultToolkit().getImage("img/scrollbar/scrollPane.png"), trackBound.x, trackBound.y, trackBound.width, trackBound.height, null);

	    }
		    
		@Override
	    protected JButton createDecreaseButton(int orientation) {
	        JButton button = new JButton() {
	        private static final long serialVersionUID = 1L;

			protected void paintComponent(Graphics g) {
				super.paintComponent(g);
				Dimension size = getSize();
				
				g.drawImage(Toolkit.getDefaultToolkit().getImage("img/scrollbar/yellowSliderUp.png"), 0, 0, size.width, size.height, null);
				revalidate();
				repaint();
				}
	        };
	        return button;
	    }
		    
		@Override
		protected JButton createIncreaseButton(int orientation) {
		    JButton button = new JButton() {
			private static final long serialVersionUID = 1L;

			protected void paintComponent(Graphics g) {
				super.paintComponent(g);
				Dimension size = getSize();
				
				g.drawImage(Toolkit.getDefaultToolkit().getImage("img/scrollbar/yellowSliderDown.png"), 0, 0, size.width, size.height, null);
				revalidate();
				repaint();
				}
		    };
		    return button;
	    }

	}

