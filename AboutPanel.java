package CSCI201_FinalProject_Scrabble;

import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Toolkit;
import java.awt.image.BufferedImage;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

import javax.imageio.ImageIO;
import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.ImageIcon;
import javax.swing.JEditorPane;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollBar;
import javax.swing.JScrollPane;
import javax.swing.JTextPane;
import javax.swing.border.EmptyBorder;

public class AboutPanel extends JPanel {
	
	public static final long serialVersionUID = 1;
	
	//CLIENT GUI INSTANCE
	//private ClientGUI clientGUI;
	//ABOUT PANEL
	//LOGO
	private JPanel logoPanel;
	private JLabel logoLabel;
	//GAMEPLAY
	private JPanel gameplaycontainerPanel;
	private JPanel gameplaylabelPanel;
	private JLabel gameplayLabel;
	private String htmlText;
	private JEditorPane editorPane;
	private JScrollPane scrollPane;
	//DEVELOPERS
	private JPanel developerscontainerPanel, developersmaincontainerPanel;
	private JPanel developerslabelPanel;
	private JLabel developersLabel;
	String poojaString, cheboluString, annieString, songString, courtneyString, vuString, jennyString, zhengString;
	JPanel poojacontainerPanel, anniecontainerPanel, courtneycontainerPanel, jennycontainerPanel;
	JPanel poojaPanel, anniePanel, courtneyPanel, jennyPanel;
	JLabel[] poojaLabels, annieLabels, courtneyLabels, jennyLabels;
	private JTextPane poojatextPane, annietextPane, courtneytextPane, jennytextPane;
	private JScrollPane developersscrollPane;
	//DISCLAIMER
	private JPanel disclaimercontainerPanel;
	private JPanel disclaimerlabelPanel;
	private JLabel disclaimerLabel;
	private JTextPane textPane;
	
	//ADD BACKGROUND 
	protected void paintComponent(Graphics g) {
		Dimension size = this.getSize();
		g.drawImage(Toolkit.getDefaultToolkit().getImage("resources/img/backgrounds/blue_panel.png"), 0, 0, size.width, size.height, this);
	}
	
	//CONSTRUCTOR
	public AboutPanel(ClientGUI clientGUI){	
		//this.clientGUI = clientGUI;
		instantiateComponents();
		createGUI();
		addActions();
	}
	
	//INSTANTIATE COMPONENTS
	private void instantiateComponents(){
		//LOGO PANEL
		logoPanel = new JPanel() {
			private static final long serialVersionUID = 1L;
			
			protected void paintComponent(Graphics g) {				
				Dimension size = this.getSize();
				g.drawImage(Toolkit.getDefaultToolkit().getImage("resources/img/backgrounds/blue_panel.png"), 0, 0, size.width, size.height, this);
			}
		};
		BufferedImage logoImage = null;
		try {
			logoImage = ImageIO.read(new File("resources/img/logo/logo.png"));
		} catch (IOException ioe) {
			System.out.println("ioe: " + ioe.getMessage());
		}
		logoLabel = new JLabel(new ImageIcon(logoImage));
		
		//GAME PLAY CONTAINER PANEL
		gameplaycontainerPanel = new JPanel() {
			private static final long serialVersionUID = 1L;

			protected void paintComponent(Graphics g) {
				Dimension size = this.getSize();
				g.drawImage(Toolkit.getDefaultToolkit().getImage("resources/img/backgrounds/blue_panel.png"), 0, 0, size.width, size.height, this);
			}
		};
		gameplaycontainerPanel.setBorder(new EmptyBorder(20, 20, 20, 20));
		gameplaycontainerPanel.setSize(new Dimension(AboutPanel.this.getWidth(), 450));
		gameplaycontainerPanel.setPreferredSize(new Dimension((int)gameplaycontainerPanel.getPreferredSize().getWidth(), 450));
		
		//GAME PLAY LABEL
		gameplaylabelPanel = new JPanel(){
			private static final long serialVersionUID = 1L;

			protected void paintComponent(Graphics g) {
				Dimension size = this.getSize();
				g.drawImage(Toolkit.getDefaultToolkit().getImage("resources/img/backgrounds/grey_panel.png"), 0, 0, size.width, size.height, this);
			}
		};
		gameplayLabel = new JLabel("Game Play"); 
		
		//HTML TEXT
		htmlText = "";
		String line = "";
		FileReader fileReader = null;
		BufferedReader bufferedReader = null;
		try{
			fileReader = new FileReader("resources/ScrabbleRules.txt");
			bufferedReader = new BufferedReader(fileReader);
			while((line = bufferedReader.readLine()) != null){
				htmlText += line;
			}
		} catch (FileNotFoundException fnfe){
			System.out.println("FileNotFoundException: " + fnfe.getMessage());
		} catch (IOException ioe){
			System.out.println("IOException: " + ioe.getMessage());
		} finally {
			try {
				if (bufferedReader != null){
					bufferedReader.close();
				}
				if (fileReader != null){
					fileReader.close();
				}
			} catch (IOException ioe) {
				System.out.println("ioe closing file: " + ioe.getMessage());
			}
		}
		
		//EDITORPANE
		editorPane = new JEditorPane();
		String fontfamily = editorPane.getFont().getFamily();
		editorPane.setContentType("text/html");
        editorPane.setText("<html><body style=\"font-family: " + fontfamily + "\">" + htmlText + "</body></html>");
		editorPane.setEditable(false);
		//SCROLLPANE
		scrollPane = new JScrollPane(editorPane);
		scrollPane.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_ALWAYS);
		//SCROLLBAR CUSTOMIZATION
		JScrollBar scrollBar = scrollPane.getVerticalScrollBar();
		scrollBar.setPreferredSize(new Dimension(25, Integer.MAX_VALUE));
		scrollBar.setUI(new CustomScrollBarUI());
		
		//DEVELOPERS PANEL
		developerscontainerPanel = new JPanel() {
			private static final long serialVersionUID = 1L;

			protected void paintComponent(Graphics g) {
				Dimension size = this.getSize();
				g.drawImage(Toolkit.getDefaultToolkit().getImage("resources/img/backgrounds/blue_panel.png"), 0, 0, size.width, size.height, this);
			}
		};
		developerscontainerPanel.setBorder(new EmptyBorder(20, 20, 20, 20));
		developerslabelPanel = new JPanel(){
			private static final long serialVersionUID = 1L;

			protected void paintComponent(Graphics g) {
				Dimension size = this.getSize();
				g.drawImage(Toolkit.getDefaultToolkit().getImage("resources/img/backgrounds/grey_panel.png"), 0, 0, size.width, size.height, this);
			}
		};
		developersLabel = new JLabel("Developers");
		
		poojaString = "POOJA";
		cheboluString = "CHEBOLU";
		poojacontainerPanel = new JPanel();
		poojaPanel = new JPanel();
		poojaLabels = new JLabel[poojaString.length()];
		for(int i = 0; i < poojaString.length(); i++){
			char c = poojaString.charAt(i);
			poojaLabels[i] = new JLabel();
			poojaLabels[i].setIcon(new ImageIcon("resources/img/tiles/" + c + ".png"));
			poojaPanel.add(poojaLabels[i]);
		}
		poojaPanel.add(new JLabel(" "));
		poojaLabels = new JLabel[cheboluString.length()];
		for(int i = 0; i < cheboluString.length(); i++){
			char c = cheboluString.charAt(i);
			poojaLabels[i] = new JLabel();
			poojaLabels[i].setIcon(new ImageIcon("resources/img/tiles/" + c + ".png"));
			poojaPanel.add(poojaLabels[i]);
		}
		poojatextPane = new JTextPane();
		poojatextPane.setText("B.S. Computer Science and Business Administration" + "\n" + "Minor in Applied Computer Security" + "\n"
						+ "USC, Class of 2018");
		poojatextPane.setEditable(false);
		poojacontainerPanel.add(poojaPanel);
		poojacontainerPanel.add(poojatextPane);
		
		annieString = "ANNIE";
		songString = "SONG";
		anniecontainerPanel = new JPanel();
		anniePanel = new JPanel();
		annieLabels = new JLabel[annieString.length()];
		for(int i = 0; i < annieString.length(); i++){
			char c = annieString.charAt(i);
			annieLabels[i] = new JLabel();
			annieLabels[i].setIcon(new ImageIcon("resources/img/tiles/" + c + ".png"));
			anniePanel.add(annieLabels[i]);
		}
		anniePanel.add(new JLabel(" "));
		annieLabels = new JLabel[songString.length()];
		for(int i = 0; i < songString.length(); i++){
			char c = songString.charAt(i);
			annieLabels[i] = new JLabel();
			annieLabels[i].setIcon(new ImageIcon("resources/img/tiles/" + c + ".png"));
			anniePanel.add(annieLabels[i]);
		}
		annietextPane = new JTextPane();
		annietextPane.setText("B.S. Computer Science" + "\n" + "Minor in Cinematic Arts" + "\n"
						+ "USC, Class of 2018");
		annietextPane.setEditable(false);
		anniecontainerPanel.add(anniePanel);
		anniecontainerPanel.add(annietextPane);
		
		courtneyString = "COURTNEY";
		vuString = "VU";
		courtneycontainerPanel = new JPanel();
		courtneyPanel = new JPanel();
		courtneyLabels = new JLabel[courtneyString.length()];
		for(int i = 0; i < courtneyString.length(); i++){
			char c = courtneyString.charAt(i);
			courtneyLabels[i] = new JLabel();
			courtneyLabels[i].setIcon(new ImageIcon("resources/img/tiles/" + c + ".png"));
			courtneyPanel.add(courtneyLabels[i]);
		}
		courtneyPanel.add(new JLabel(" "));
		courtneyLabels = new JLabel[vuString.length()];
		for(int i = 0; i < vuString.length(); i++){
			char c = vuString.charAt(i);
			courtneyLabels[i] = new JLabel();
			courtneyLabels[i].setIcon(new ImageIcon("resources/img/tiles/" + c + ".png"));
			courtneyPanel.add(courtneyLabels[i]);
		}
		courtneytextPane = new JTextPane();
		courtneytextPane.setText("B.S. Computer Science and Business Administration" + "\n" + "USC, Class of 2018");
		courtneytextPane.setEditable(false);
		courtneycontainerPanel.add(courtneyPanel);
		courtneycontainerPanel.add(courtneytextPane);
		
		jennyString = "JENNIFER";
		zhengString = "ZHENG";
		jennycontainerPanel = new JPanel();
		jennyPanel = new JPanel();
		jennyLabels = new JLabel[jennyString.length()];
		for(int i = 0; i < jennyString.length(); i++){
			char c = jennyString.charAt(i);
			jennyLabels[i] = new JLabel();
			jennyLabels[i].setIcon(new ImageIcon("resources/img/tiles/" + c + ".png"));
			jennyPanel.add(jennyLabels[i]);
		}
		jennyPanel.add(new JLabel(" "));
		jennyLabels = new JLabel[zhengString.length()];
		for(int i = 0; i < zhengString.length(); i++){
			char c = zhengString.charAt(i);
			jennyLabels[i] = new JLabel();
			jennyLabels[i].setIcon(new ImageIcon("resources/img/tiles/" + c + ".png"));
			jennyPanel.add(jennyLabels[i]);
		}
		jennytextPane = new JTextPane();
		jennytextPane.setText("B.S. Computer Science and Business Administration" + "\n" + "USC, Class of 2018");
		jennytextPane.setEditable(false);
		jennycontainerPanel.add(jennyPanel);
		jennycontainerPanel.add(jennytextPane);
		
		developersmaincontainerPanel = new JPanel();
		developersmaincontainerPanel.add(poojacontainerPanel);
		developersmaincontainerPanel.add(anniecontainerPanel);
		developersmaincontainerPanel.add(courtneycontainerPanel);
		developersmaincontainerPanel.add(jennycontainerPanel);
		//DEVELOPERS SCROLLPANE
		developersscrollPane = new JScrollPane(developersmaincontainerPanel);
		developersscrollPane.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_ALWAYS);
		//SCROLLBAR CUSTOMIZATION
		JScrollBar developersscrollBar = developersscrollPane.getVerticalScrollBar();
		developersscrollBar.setPreferredSize(new Dimension(25, Integer.MAX_VALUE));
		developersscrollBar.setUI(new CustomScrollBarUI());
		//DISCLAIMER PANEL
		disclaimercontainerPanel = new JPanel() {
			private static final long serialVersionUID = 1L;

			protected void paintComponent(Graphics g) {
				Dimension size = this.getSize();
				g.drawImage(Toolkit.getDefaultToolkit().getImage("resources/img/backgrounds/blue_panel.png"), 0, 0, size.width, size.height, this);
			}
		};
		disclaimercontainerPanel.setBorder(new EmptyBorder(20, 20, 20, 20));
		disclaimerlabelPanel = new JPanel(){
			private static final long serialVersionUID = 1L;

			protected void paintComponent(Graphics g) {
				Dimension size = this.getSize();
				g.drawImage(Toolkit.getDefaultToolkit().getImage("resources/img/backgrounds/grey_panel.png"), 0, 0, size.width, size.height, this);
			}
		};
		disclaimerLabel = new JLabel("Disclaimer");
		textPane = new JTextPane();
		textPane.setText("Scrabble Final Project" + "\n" + "CSCI 201, University of Southern California, Spring 2016" + "\n"
						+ "To be used for educational purposes only; no copyright infringement intended.");
		textPane.setEditable(false);
	}
		
	//CREATE GUI
	private void createGUI(){
		//SET LAYOUT
		setLayout(new BoxLayout(AboutPanel.this, BoxLayout.Y_AXIS));
		gameplaycontainerPanel.setLayout(new BoxLayout(gameplaycontainerPanel, BoxLayout.Y_AXIS));
		poojacontainerPanel.setLayout(new BoxLayout(poojacontainerPanel, BoxLayout.Y_AXIS));
		anniecontainerPanel.setLayout(new BoxLayout(anniecontainerPanel, BoxLayout.Y_AXIS));
		courtneycontainerPanel.setLayout(new BoxLayout(courtneycontainerPanel, BoxLayout.Y_AXIS));
		jennycontainerPanel.setLayout(new BoxLayout(jennycontainerPanel, BoxLayout.Y_AXIS));
		developersmaincontainerPanel.setLayout(new BoxLayout(developersmaincontainerPanel, BoxLayout.Y_AXIS));
		developerscontainerPanel.setLayout(new BoxLayout(developerscontainerPanel, BoxLayout.Y_AXIS));
		disclaimercontainerPanel.setLayout(new BoxLayout(disclaimercontainerPanel, BoxLayout.Y_AXIS));
		
		//ADD COMPONENTS
		logoPanel.add(logoLabel);
		gameplaylabelPanel.add(gameplayLabel);
		gameplaycontainerPanel.add(gameplaylabelPanel);
		gameplaycontainerPanel.add(scrollPane);
		developerslabelPanel.add(developersLabel);
		developerscontainerPanel.add(developerslabelPanel);
		developerscontainerPanel.add(developersscrollPane);
		disclaimerlabelPanel.add(disclaimerLabel);
		disclaimercontainerPanel.add(disclaimerlabelPanel);
		disclaimercontainerPanel.add(textPane);
		add(Box.createGlue());
		add(logoPanel);
		add(Box.createGlue());
		add(gameplaycontainerPanel);
		add(Box.createGlue());
		add(developerscontainerPanel);
		add(Box.createGlue());
		add(disclaimercontainerPanel);
		add(Box.createGlue());
	}
		
	//ADD ACTIONS 
	private void addActions(){
	}
}