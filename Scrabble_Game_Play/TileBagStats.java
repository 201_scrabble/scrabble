package Scrabble_Game_Play;
import java.awt.BorderLayout;
import java.util.Vector;

import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JScrollPane;
import javax.swing.JTable;

public class TileBagStats extends JDialog {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private JLabel totalTiles;
	private JTable letterCounts;
	private JScrollPane jsp;
	private ScrabbleTileBag bag;
	
	
	public TileBagStats(ScrabbleTileBag bag) {
		this.bag = bag;
		instantiateComponents();
		createGUI();
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
	}
	
	public void instantiateComponents() {
		Vector<String> col_names = new Vector<String>();
		col_names.add("Letter");
		col_names.add("Number of tiles remaining");
		
		Vector<Vector> row_data = new Vector<Vector>();
		for (int i = 0; i < 26; i++) {
			Vector<String> row = new Vector<String>();
			char c = (char) ('A'+ i);
			int count = bag.getLetterCount(c);
			row.add(""+c);
			row.add(""+count);	
			row_data.add(row);
		}
		
		letterCounts = new JTable(row_data, col_names);
		letterCounts.setEnabled(false);
		jsp = new JScrollPane(letterCounts);
		totalTiles = new JLabel("Total tiles left in bag: " + bag.getSize());
	}
	
	public void createGUI() {
		setTitle("Tile Bag Statistics");
		setLayout(new BorderLayout());
		setSize(250,450);
		add(totalTiles, BorderLayout.NORTH);
		add(jsp, BorderLayout.SOUTH);
		pack();
		setVisible(true);
		
	}
}
