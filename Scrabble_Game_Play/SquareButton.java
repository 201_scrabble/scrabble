package Scrabble_Game_Play;
import java.awt.Dimension;
import java.awt.Insets;
import java.io.Serializable;

import javax.swing.JButton;

public class SquareButton extends JButton implements Serializable {
	private static final long serialVersionUID = 1L;
	private int x; // IN 2D ARRAY, SWAPPED FOR GRIDBAGLAYOUT
	private int y; // IN 2D ARRAY, SWAPPED FOR GRIDBAGLAYOUT
	
	public SquareButton() {
		super("");
        setBorder(null);
        setBorderPainted(false);
        setMargin(new Insets(0,0,0,0));
        setSize(new Dimension(300,300));
	}
	
	public void setXValue(int x) {
		this.x = x;
	}
	
	public void setYValue(int y) {
		this.y = y;
	}
	
	public int getXValue() {
		return x;
	}
	
	public int getYValue() {
		return y;
	}
}
