package Scrabble_Game_Play;

import javax.swing.Box;

import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import java.awt.BorderLayout;

import CSCI201_FinalProject_Scrabble.CustomJButton;

public class CheckDictionaryDialog extends JDialog {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Trie dictionary;
	private JLabel message;
	private JTextField textField;
	private CustomJButton checkButton;
	private CustomJButton cancelButton;

	public CheckDictionaryDialog(Trie dictionary) {
		this.dictionary = dictionary;
		instantiateComponents();
		createGUI();
		addActions();
	}
	
	public void instantiateComponents() {
		message = new JLabel("Please enter a word to search the dictionary with: ");
		textField = new JTextField();
		checkButton = new CustomJButton("Check Word");
		cancelButton = new CustomJButton("Cancel");
	}
	
	public void createGUI() {
		setTitle("Check Dictionary");
		setLayout(new BorderLayout());
		setSize(300,300);
		add(message, BorderLayout.NORTH);
		add(textField, BorderLayout.CENTER);
		
		Box box = Box.createHorizontalBox();
		box.add(cancelButton);
		box.add(checkButton);
		add(box, BorderLayout.SOUTH);
		
		pack();
		setVisible(true);
	}
	
	public void addActions() {
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		checkButton.addActionListener((event) -> {
			String word = textField.getText();
			
			for (int i = 0; i < word.length(); i++) {
				int c = (int) word.charAt(i);
				// lower case 97-122
				// upper case 65-90
				if (!(c >= 65 && c <= 90) && !(c >= 97 && c <= 122)) {
					JOptionPane.showConfirmDialog(null, "Please enter a word with only letters", "Error!", JOptionPane.DEFAULT_OPTION);
					return;
				}
			}
			
			if (dictionary.contains(word.toLowerCase())) {
				JOptionPane.showConfirmDialog(null, "This is a word in the dictionary", "Check Dictionary Result", JOptionPane.DEFAULT_OPTION);
			} else {
				JOptionPane.showConfirmDialog(null, "This is not a word in the dictionary", "Check Dictionary Result", JOptionPane.DEFAULT_OPTION);
			}
		});
		
		cancelButton.addActionListener((event) -> {
			dispose();
		});
	}
}
