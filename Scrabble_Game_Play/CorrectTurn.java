package Scrabble_Game_Play;
import java.io.Serializable;
import java.util.ArrayList;

public class CorrectTurn implements Serializable {
	private static final long serialVersionUID = 1L;
	
	private ArrayList<String> currWord;
	private int currScore;
	
	public CorrectTurn(ArrayList<String> currWord, int currScore) {
		this.currWord = currWord;
		this.currScore = currScore;
	}
	
	public void setCurrWord(ArrayList<String> currWord) {
		this.currWord = currWord;
	}
	
	public void setCurrScore(int currScore) {
		this.currScore = currScore;
	}
	
	public ArrayList<String> getCurrWord() {
		return currWord;
	}
	
	public int getCurrScore() {
		return currScore;
	}
}
