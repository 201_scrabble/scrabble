package Scrabble_Game_Play;
import java.awt.Insets;
import java.io.Serializable;

import javax.swing.ImageIcon;
import javax.swing.JButton;

class TileButton extends JButton implements Serializable {
	private static final long serialVersionUID = 1;
	//private Image tileImg;
	private ScrabbleTile tile;
	private boolean onBoard; 
	int x,y;
	
	public TileButton(ScrabbleTile tile) {
		super("");
		this.tile = tile;
        setBorder(null);
        setBorderPainted(false);
        setMargin( new Insets(0,0,0,0));
		ImageIcon icon = new ImageIcon(tile.getImage());
		setIcon(icon);
		onBoard = false;
		//tileImg = Toolkit.getDefaultToolkit().getImage(tile.getImage());
	}
	
	// DRAWING IMAGE ONTO THE BUTTON
//	protected void paintComponent(Graphics g) {
//		super.paintComponent(g);
//		setContentAreaFilled(false);
//		Dimension size = this.getSize();
//		g.drawImage(tileImg, 0, 0, size.width, size.height, null);
//		repaint();
//		revalidate();
//	}	

	public ScrabbleTile getTile() {
		return tile;
	}
	
	public boolean isOnBoard() {
		return onBoard;
	}
	
	public void setOnBoard(boolean onBoard) {
		this.onBoard = onBoard;
	}
	
	public void setXValue(int x) {
		this.x = x;
	}
	
	public void setYValue(int y) {
		this.y = y;
	}
	
	public int getXValue() {
		return x;
	}
	
	public int getYValue() {
		return y;
	}
}