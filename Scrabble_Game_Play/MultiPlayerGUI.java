package Scrabble_Game_Play;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.awt.Insets;
import java.awt.Rectangle;
import java.awt.Toolkit;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.imageio.ImageIO;
import javax.sound.sampled.AudioInputStream;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.Clip;
import javax.sound.sampled.LineUnavailableException;
import javax.sound.sampled.UnsupportedAudioFileException;
import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.ImageIcon;
import javax.swing.JComponent;
import javax.swing.JFileChooser;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.border.LineBorder;

import CSCI201_FinalProject_Scrabble.CustomJButton;

public class MultiPlayerGUI {

    private final JPanel gui = new JPanel() {
    	private static final long serialVersionUID = 1L;

		public void paintComponent(Graphics g) {
			g.drawImage(Toolkit.getDefaultToolkit().getImage("resources/img/backgrounds/blue_panel.png"), 0, 0, this);
			
		}
    };
    private final JPanel gamePanel = new JPanel(new GridLayout(1,2));
    private final JPanel gameOptions = new JPanel() {
    	private static final long serialVersionUID = 1L;

		public void paintComponent(Graphics g) {
			g.drawImage(Toolkit.getDefaultToolkit().getImage("resources/img/backgrounds/blue_panel.png"), 0, 0, this);
			
		}
    };
    protected SquareButton[][] scrabbleBoardSquares = new SquareButton[15][15];
    protected TileButton[][] currentPlay = new TileButton[15][15]; // WHEN SHOULD THIS BE CLEARED? AFTER PLAY HAS BEEN SUBMITTED + ACCEPTED?
    protected String[][] savedBoard = new String[15][15];
    protected String[][] blankTileBoard = new String[15][15];
    private JPanel scrabbleBoard;
    private TileRackGUI tileRack;
    private ScrabbleTileBag bag = new ScrabbleTileBag();
    private CustomJButton score, checkDictionary, viewTileBagStats, saveBoard, dealTiles, shuffleTiles, replaceTiles, passTurn, playWord, endGame;
    protected CustomJButton undoSelection;
    protected GridBagConstraints gbc = new GridBagConstraints();
    private boolean isFirstTurn;
    private int numTurns = 0;
    private boolean bagIsEmpty = false;
    private int numPassedTurns = 0;
    private Constants constants = new Constants();
    private GameLogic gameLogic;
    private JLabel turnLabel;
    
    private String username;
    private String versusPlayer;
    private boolean isMyTurn;
    private int myScore = 0;
	private int theirScore = 0;
	private int totalScore = 0;
    
    private static List<String> types = Arrays.asList( ImageIO.getWriterFileSuffixes() );
    private MusicPlayer mp; 
    
    public Trie dictionary;

    public MultiPlayerGUI(Trie dictionary, String username, boolean isFirstTurn, String versusPlayer) {
    	this.dictionary = dictionary;
    	this.isFirstTurn = isFirstTurn;
    	this.username = username;
    	this.versusPlayer = versusPlayer;
    	if (isFirstTurn)
    		isMyTurn = true;
    	else
    		isMyTurn = false;
    	gameLogic = new GameLogic(constants, this.dictionary);
    	mp = new MusicPlayer();
        initializeGUI();
    }

    public final void initializeGUI() {
        // set up the main GUI
    	gui.setLayout(new BorderLayout());
        gui.setBorder(new EmptyBorder(5, 5, 5, 5));

        scrabbleBoard = new JPanel(new GridBagLayout());
  
        scrabbleBoard.setBorder(new LineBorder(Color.BLACK));
        scrabbleBoard.setMaximumSize(new Dimension(100, 100));
        gamePanel.add(scrabbleBoard);

        // create the chess board squares
        Insets buttonMargin = new Insets(0,0,0,0);
        for (int ii = 0; ii < scrabbleBoardSquares.length; ii++) { // rows
            for (int jj = 0; jj < scrabbleBoardSquares[ii].length; jj++) { // cols
                SquareButton b = new SquareButton();
//                b.setBorder(null);
//                b.setBorderPainted(false);
//                b.setMargin(buttonMargin);
                // our chess pieces are 64x64 px in size, so we'll
                // 'fill this in' using a transparent icon..
                ImageIcon icon = null;
                if (ii == 7 && jj == 7)
                	icon = new ImageIcon("resources/img/gameplay/centerTile.png");
                else if ((ii == jj) && ((ii == 0) || (ii == 14)))
                	icon = new ImageIcon("resources/img/gameplay/tripleWord.png");
                else if ((ii == jj) && ((ii == 5) || (ii == 9)))
                	icon = new ImageIcon("resources/img/gameplay/tripleLetter.png");
                else if ((ii == jj) && ((ii == 6) || (ii == 8)))
                	icon = new ImageIcon("resources/img/gameplay/doubleLetter.png");
                else if (((14-ii) == jj) && (((14-ii) == 0) || ((14-ii) == 14)))
                	icon = new ImageIcon("resources/img/gameplay/tripleWord.png");
                else if (((14-ii) == jj) && (((14-ii) == 5) || ((14-ii) == 9)))
                	icon = new ImageIcon("resources/img/gameplay/tripleLetter.png");
                else if (((14-ii) == jj) && (((14-ii) == 6) || ((14-ii) == 8)))
                	icon = new ImageIcon("resources/img/gameplay/doubleLetter.png");
                else if (((ii == 0 || ii == 14) && (jj == 7)) || ((jj == 0 || jj == 14) && (ii == 7)))
                	icon = new ImageIcon("resources/img/gameplay/tripleWord.png");
                else if (((ii == 0 || ii == 14) && (jj == 3 || jj == 11)) || 
                		((jj == 0 || jj == 14) && (ii == 3 || ii == 11)))
                	icon = new ImageIcon("resources/img/gameplay/doubleLetter.png");
                else if (((ii == 1 || ii == 13) && (jj == 5 || (14-jj) == 5)) ||
                		((jj == 1 || jj == 13) && (ii == 5 || (14-ii) == 5)))
                	icon = new ImageIcon("resources/img/gameplay/tripleLetter.png");
                else if (((ii == 2 || ii == 12) && (jj == 6 || (14-jj) == 6)) ||
                		((jj == 2 || jj == 2) && (ii == 6 || (14-ii) == 6)))
                	icon = new ImageIcon("resources/img/gameplay/doubleLetter.png");
                else if (((ii == 7) && (jj == 3 || (14-jj) == 3)) ||
                		((jj == 7) && (ii == 3 || (14-ii) == 3)))
                	icon = new ImageIcon("resources/img/gameplay/doubleLetter.png");
                else if (ii == jj)
                	icon = new ImageIcon("resources/img/gameplay/doubleWord.png");
                else if ((14-ii) == jj)
                	icon = new ImageIcon("resources/img/gameplay/doubleWord.png");
                else
                	icon = new ImageIcon("resources/img/gameplay/blank.png");
                
                b.setIcon(icon);
                scrabbleBoardSquares[ii][jj] = b;
            }
        }

        // fill the black non-pawn piece row
//        gbc.fill = gbc.BOTH;
        gbc.anchor = GridBagConstraints.FIRST_LINE_START;
        gbc.weightx = 1.0;
        gbc.weighty = 1.0;
        gbc.fill = GridBagConstraints.BOTH;
        for (int ii = 0; ii < 15; ii++) {
        	gbc.gridy = ii;
            for (int jj = 0; jj < 15; jj++) {
            	gbc.gridx = jj;
            	
//            	System.out.println(ii + " " + jj);
            	scrabbleBoardSquares[ii][jj].setXValue(ii);
            	scrabbleBoardSquares[ii][jj].setYValue(jj);
            	scrabbleBoard.add(scrabbleBoardSquares[ii][jj], gbc);
            	
//            	System.out.println(scrabbleBoardSquares[jj][ii].getX() + " " + scrabbleBoardSquares[jj][ii].getY() );
//            	System.out.println(scrabbleBoardSquares[jj][ii].getWidth() + scrabbleBoardSquares[jj][ii].getHeight());
            }
        }
        
        for (int ii = 0; ii < 15; ii++) {
            for (int jj = 0; jj < 15; jj++) {
            	addActions(scrabbleBoardSquares[ii][jj], ii, jj);
            }
        }
        
        tileRack = new TileRackGUI(this);
    
        
        score = new CustomJButton("Score");
        checkDictionary = new CustomJButton("Check Dictionary");
        viewTileBagStats = new CustomJButton("View Tile Bag Stats");
        saveBoard = new CustomJButton("Save Board");
        dealTiles = new CustomJButton("Deal Tiles");
        shuffleTiles = new CustomJButton("Shuffle Tiles");
        replaceTiles = new CustomJButton("Replace Tiles");
        passTurn = new CustomJButton("Pass Turn");
        playWord = new CustomJButton("Play Word");
        endGame = new CustomJButton("End Game");
        undoSelection = new CustomJButton("Undo Selection of Tile");
        undoSelection.setEnabled(false);
        
        BoxLayout bl = new BoxLayout(gameOptions, BoxLayout.Y_AXIS);
        gameOptions.setLayout(bl);
        gameOptions.add(score);
        gameOptions.add(Box.createVerticalGlue());
        gameOptions.add(saveBoard);
        gameOptions.add(Box.createVerticalGlue());
        gameOptions.add(checkDictionary);
        gameOptions.add(Box.createVerticalGlue());
        gameOptions.add(viewTileBagStats);
        gameOptions.add(Box.createVerticalGlue());
        gameOptions.add(dealTiles);
        gameOptions.add(Box.createVerticalGlue());
        gameOptions.add(shuffleTiles);
        gameOptions.add(Box.createVerticalGlue());
        gameOptions.add(replaceTiles);
        gameOptions.add(Box.createVerticalGlue());
        gameOptions.add(passTurn);
        gameOptions.add(Box.createVerticalGlue());
        gameOptions.add(undoSelection);
        gameOptions.add(Box.createVerticalGlue());
        gameOptions.add(playWord);
        gameOptions.add(Box.createVerticalGlue());
        gameOptions.add(endGame);
        
        if (isMyTurn)
        	turnLabel = new JLabel("It's your turn!");
        else
        	turnLabel = new JLabel("It's not your turn. Waiting for the other player's move!");
        
        gamePanel.add(gameOptions);
        gui.add(gamePanel, BorderLayout.CENTER);
        gui.add(tileRack, BorderLayout.SOUTH);
        gui.add(turnLabel, BorderLayout.NORTH);
        
        
        addActions();
        
        // disable all the buttons
        if (!isMyTurn) {
        	this.enableScrabbleBoard(scrabbleBoard, false);
        	this.enableComponents(gameOptions, false);
        	this.enableComponents(tileRack, false);
        }

    }
    
    public void addActions() {
		dealTiles.addActionListener((event) -> {
			tileRack.setRack(bag.dealTiles());
			dealTiles.setEnabled(false);
		});
		
		shuffleTiles.addActionListener((event) -> {
			ArrayList<ScrabbleTile> shuffled = new ArrayList<ScrabbleTile>();
			ArrayList<ScrabbleTile> rack = tileRack.getTiles();
			for (ScrabbleTile st: rack) {
				shuffled.add(st);
			}
			shuffled = bag.shuffleTiles(shuffled);
			tileRack.resetRack(shuffled);
		});
		
		replaceTiles.addActionListener((event) -> {
			//System.out.println("BEFORE");
			//bag.printBag();	
			tileRack.resetRack(bag.replaceAllTiles(tileRack.getTiles()));
			numTurns++;
			numPassedTurns++;
			if (numPassedTurns == 6) {
				JOptionPane.showConfirmDialog(null, "Game ended! Total Score: " + totalScore + " | Number of Turns: " + numTurns, "Game Over!", JOptionPane.DEFAULT_OPTION);
				gui.setVisible(false);
			}
			//System.out.println("AFTER");
			//bag.printBag();		
		
			isMyTurn = false;
			turnLabel.setText("It's not your turn. Waiting for the other player's move!");
			gui.repaint();
			gui.revalidate();
			clearPlayedTiles();
        	this.enableScrabbleBoard(scrabbleBoard, false);
        	this.enableComponents(gameOptions, false);
        	this.enableComponents(tileRack, false);
			// SEND PASS TURN MESSAGE
//		    private ScrabbleTileBag bag = new ScrabbleTileBag();
//		    private int numTurns = 0;
//		    private int numPassedTurns = 0;
//		    
//		    private String username;

			
		});   	
		
		undoSelection.addActionListener((event) -> {
			tileRack.resetMostRecentlyClicked();
		});
		
		passTurn.addActionListener((event) -> {
			numTurns++;
			numPassedTurns++;
			if (numPassedTurns == 6) {
				JOptionPane.showConfirmDialog(null, "Game ended! Total Score: " + totalScore + " | Number of Turns: " + numTurns, "Game Over!", JOptionPane.DEFAULT_OPTION);
				gui.setVisible(false);
			}
			

			
			// SEND MESSAGE TO CLIENT WITH INFORMATION ABOUT THIS PLAY: + your username
//			MAKE SURE TO CLEAR THE USER'S SCRABBLE BOARD GUI, IN CASE THEY LEFT TILES ON THE BOARD BEFORE PRESSING THIS BUTTON
			isMyTurn = false;
			turnLabel.setText("It's not your turn. Waiting for the other player's move!");
			gui.repaint();
			gui.revalidate();
			clearPlayedTiles();
        	this.enableScrabbleBoard(scrabbleBoard, false);
        	this.enableComponents(gameOptions, false);
        	this.enableComponents(tileRack, false);
			
			// SEND PASS TURN MESSAGE
//		    private ScrabbleTileBag bag = new ScrabbleTileBag();
//		    private int numTurns = 0;
//		    private int numPassedTurns = 0;
//		    
//		    private String username;


		});
		
		viewTileBagStats.addActionListener((event) -> {
			TileBagStats tbs = new TileBagStats(bag);
		});
		
		checkDictionary.addActionListener((event) -> {
			CheckDictionaryDialog cdd = new CheckDictionaryDialog(dictionary);
		});
		
		score.addActionListener((event) -> {
			JOptionPane.showConfirmDialog(null, "Your score: " + myScore + " " + versusPlayer + " 's Score: " + theirScore, username + " VS. " + versusPlayer, JOptionPane.DEFAULT_OPTION);
		});
		
		playWord.addActionListener((event) -> {
			CorrectTurn ct = null;
			
			if (isFirstTurn) {
				ct = GameLogic.checkFirstTurnConfig(currentPlay, savedBoard);
			} else {
				ct = GameLogic.checkIfValidConfiguration(currentPlay, savedBoard, tileRack, blankTileBoard);
			}
			
			
			//is word
			//fill in savedBoard
			if (ct != null) {
				
				numTurns++;
				numPassedTurns = 0;
				myScore += ct.getCurrScore();
				totalScore += ct.getCurrScore();
				try {
					AudioInputStream audioInputStream = AudioSystem.getAudioInputStream(new File("resources/sounds/CorrectAnswer.wav").getAbsoluteFile());
					Clip clip = AudioSystem.getClip();
					clip.open(audioInputStream);
					clip.start();
				} catch (UnsupportedAudioFileException uafe) {
					uafe.getMessage();
				} catch (IOException ioe) {
					ioe.getMessage();
				} catch (LineUnavailableException lue) {
					lue.getMessage();
				}
				String combined_words = "";
				for (String s: ct.getCurrWord())
					combined_words += s + " | ";
				JOptionPane.showConfirmDialog(null, "Played Word(s): " + combined_words + "Word Score: " + ct.getCurrScore() + 
						" | Your Score: " + myScore, "Successful Turn: " + numTurns, JOptionPane.DEFAULT_OPTION);
				
				for (int i = 0; i < 15; i++) {
					for (int j = 0; j < 15; j++) {
						if (currentPlay[i][j] != null && savedBoard[i][j] == null) {
							savedBoard[i][j] = "" + currentPlay[i][j].getTile().getLetter();
						}
					}
				}
				printSavedBoard();
				
				if (isFirstTurn) {
					isFirstTurn = false;
				} 
				System.out.println("Tile rack size " + tileRack.getRack().size());
				if (!bagIsEmpty || !(tileRack.getRack().size() == 0)) {
					if (!bagIsEmpty) {
						int add = 7-tileRack.getRack().size();
						for(int i = 0; i < add; i++) {
							System.out.println("adding tile");
							ScrabbleTile st = bag.requestATile();
							if (st != null) {
								TileButton tb = new TileButton(st);
								tileRack.getTiles().add(st);
								tileRack.getRack().add(tb);
								tileRack.add(tb);
								tileRack.addActionsForMulti(tb);
								tileRack.repaint();
								tileRack.revalidate();
							} else {
								bagIsEmpty = true;
							}
						}
					}
				} else {
					JOptionPane.showConfirmDialog(null, "Game ended! Total Score: " + totalScore + " | Number of Turns: " + numTurns, "Game Over!", JOptionPane.DEFAULT_OPTION);
					gui.setVisible(false);
				}
				
				// IFF GAME HAS NOT ENDED
				isMyTurn = false;
				turnLabel.setText("It's not your turn. Waiting for the other player's move!");
				gui.repaint();
				gui.revalidate();
	        	this.enableScrabbleBoard(scrabbleBoard, false);
	        	this.enableComponents(gameOptions, false);
	        	this.enableComponents(tileRack, false);
				// SEND MESSAGE TO CLIENT WITH INFORMATION ABOUT THIS PLAY: + your username
//			    protected SquareButton[][] scrabbleBoardSquares = new SquareButton[15][15];
//			    protected String[][] savedBoard = new String[15][15];
//			    protected String[][] blankTileBoard = new String[15][15];
	        	//TileButton[][] currentPlay
//			    private ScrabbleTileBag bag = new ScrabbleTileBag();
//				CorrectTurn ct	 
	      

	    
				currentPlay = null;
				currentPlay = new TileButton[15][15];
				//checking to see if it is a word
				
			} else { 
				try {
					AudioInputStream audioInputStream1 = AudioSystem.getAudioInputStream(new File("resources/sounds/WrongAnswer.wav").getAbsoluteFile());
			
					Clip clip = AudioSystem.getClip();
					clip.open(audioInputStream1);
					clip.start();
				} catch (UnsupportedAudioFileException uafe) {
					uafe.getMessage();
				} catch (IOException ioe) {
					ioe.getMessage();
				} catch (LineUnavailableException lue) {
					lue.getMessage();
				}
				JOptionPane.showConfirmDialog(null, "Sorry, you played an incorrect word. Please try again!", "Failed Turn", JOptionPane.DEFAULT_OPTION);
			}
		});
		
		saveBoard.addActionListener((event) -> {
			BufferedImage img = createImage(scrabbleBoard);
			
			JFileChooser jfc = new JFileChooser();
		    

		    int i = jfc.showSaveDialog(null);
		  
		    
		    if (i == JFileChooser.APPROVE_OPTION) {
			
		    	String save_file = "";
	    		File f = new File(jfc.getSelectedFile() + "");
	    		save_file = jfc.getSelectedFile() + "";
		
		    	if (f.exists()) {
		    		int val = JOptionPane.showConfirmDialog(null, "Assignment.txt already exists. Do you want to replace it?", 
		    				"Confirm Save as", JOptionPane.YES_NO_OPTION, JOptionPane.WARNING_MESSAGE);
		    		if (val == JOptionPane.NO_OPTION) {
		    			return; 
		    		}
		    	} 
		    	
				try {
					writeImage(img, save_file);
				} catch (IOException ioe) {
					System.out.println(ioe.getMessage());
				}
		    }
			
		});
    }
   
    public void addActions(SquareButton button, int row, int col) {
    	
    	button.addActionListener((event) -> {
    		System.out.println("Adding action listeners: " + row + " " + col);
    		System.out.println(scrabbleBoard.getComponentAt(row, col) instanceof CustomJButton);
//    		if (scrabbleBoard.getComponentAt(row, col) instanceof SquareButton) {
//    		if (scrabbleBoard.getComponentAt(row,  col) instanceof CustomJButton) {
   
//    			System.out.println(currentPlay[row][col]== null);
//    			if (currentPlay[row][col] == null) {
    				System.out.println("action listener for " + row + " " + col);
    				TileButton mostRecent = tileRack.getMostRecentlyClicked();
    				if (mostRecent != null) {
    					if (mostRecent.getTile().getScore() == 0) {
    						Object[] options = {'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N',
    								'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z'};
    						char s = (char) JOptionPane.showInputDialog(null, "Choose a letter for your blank tile: ",
    								"Select Letter", JOptionPane.PLAIN_MESSAGE, null, options, 'A');
    						if (s != ' ') {
    							String image = "resources/img/gameplay/" + s + ".jpg";
    							mostRecent.getTile().setImage(image);
    							mostRecent.getTile().setChar(s);
    							ImageIcon icon = new ImageIcon(image);
    							mostRecent.setIcon(icon);
    							tileRack.repaint();
    							tileRack.revalidate();
    						
    							blankTileBoard[row][col] = "" + s;
    							//System.out.println(button.getXValue() + " " + button.getYValue() + " " + s);
    						}
    					}
    					
	    				currentPlay[row][col] = mostRecent;
	    				mostRecent.setOnBoard(true);
	    				//Point p  = scrabbleBoard.getLocation();
	    				System.out.println("Adding most recently clicked to board: " + mostRecent.getTile().getLetter());
	    				
	    				gbc.gridx = col;
	    				gbc.gridy = row;
	    				//System.out.println(button.getXValue() + " " + button.getYValue());
	    				System.out.println("Grid bag layout (x,y) coordinates: " + gbc.gridx + " "  + gbc.gridy);
	    				
	    				mostRecent.setXValue(row);
	    				mostRecent.setYValue(col);
	    				
	    				
	    				scrabbleBoard.add(mostRecent, gbc);
	    				scrabbleBoard.remove(button);
	    				
	    				scrabbleBoardSquares[row][col] = null;
	   	    				
	    				//scrabbleBoard.setComponentZOrder(mostRecent, 200);
	    				gui.repaint();
	    				gui.revalidate();
	    				tileRack.resetMostRecentlyClicked();
	    				tileRack.remove(mostRecent);
	    				tileRack.getTiles().remove(mostRecent.getTile());
	    				tileRack.getRack().remove(mostRecent);
    				}
 //   			} else {
    				
 //   			}
 //   		} else {
    			
 //   		}
    	});
    }
    
    public final JComponent getscrabbleBoard() {
        return scrabbleBoard;
    }

    public final JComponent getGui() {
        return gui;
    }
    
    
    // FOR TESTING PURPOSES
    public void printSavedBoard() {
    	System.out.println("PRINTING OUT SAVED BOARD");
    	for (int i = 0; i < 15; i++) {
    		for (int j = 0; j < 15; j++) {
    			if (savedBoard[i][j] != null)
    				System.out.println(i + " " + j + " " + savedBoard[i][j]);
    		}
    	}
    }
    
	/*  http://www.camick.com/java/source/ScreenImage.java
	 *  Create a BufferedImage for Swing components.
	 *  The entire component will be captured to an image.
	 *
	 *  @param  component Swing component to create image from
	 *  @return	image the image for the given region
	*/
	public static BufferedImage createImage(JComponent component)
	{
		Dimension d = component.getSize();

		if (d.width == 0 || d.height == 0)
		{
			d = component.getPreferredSize();
			component.setSize( d );
		}

		Rectangle region = new Rectangle(0, 0, d.width, d.height);
		if (! component.isDisplayable())
		{
			Dimension dim = component.getSize();

			if (dim.width == 0 || dim.height == 0)
			{
				dim = component.getPreferredSize();
				component.setSize( dim );
			}

			layoutComponent( component );
		}

		BufferedImage image = new BufferedImage(region.width, region.height, BufferedImage.TYPE_INT_RGB);
		Graphics2D g2d = image.createGraphics();

		//  Paint a background for non-opaque components,
		//  otherwise the background will be black

		if (! component.isOpaque())
		{
			g2d.setColor( component.getBackground() );
			g2d.fillRect(region.x, region.y, region.width, region.height);
		}

		g2d.translate(-region.x, -region.y);
		component.paint( g2d );
		g2d.dispose();
		return image;
	}
	
	// http://www.camick.com/java/source/ScreenImage.java
	static void layoutComponent(Component component)
	{
		synchronized (component.getTreeLock())
		{
			component.doLayout();

    	    if (component instanceof Container)
        	{
            	for (Component child : ((Container)component).getComponents())
	            {
    	            layoutComponent(child);
        	    }
	        }
    	}
	}
	
	//http://stackoverflow.com/questions/10985734/java-swing-enabling-disabling-all-components-in-jpanel
    public void enableComponents(Container container, boolean enable) {
    	
        Component[] components = container.getComponents();
        for (Component component : components) {
            component.setEnabled(enable);
            if (component instanceof Container) {
                enableComponents((Container)component, enable);
            }
        }
    }
    
    public void enableScrabbleBoard(JPanel board, boolean enable) {
    	if (enable) {
    		for (int i = 0; i < 15; i++) {
    			for (int j = 0; j < 15; j++) {
    				if (scrabbleBoardSquares[i][j] != null)
    					scrabbleBoardSquares[i][j].setEnabled(true);
    			}
    		}
    	} else {
    		enableComponents(board, false);
    	}
    }
    
	// SEND MESSAGE TO CLIENT WITH INFORMATION ABOUT THIS PLAY: + your username
//    protected SquareButton[][] scrabbleBoardSquares = new SquareButton[15][15];
//    protected String[][] savedBoard = new String[15][15];
//    protected String[][] blankTileBoard = new String[15][15];
//    private ScrabbleTileBag bag = new ScrabbleTileBag();
//    private int totalScore = 0;
//		private int myScore = 0;
//    private int numTurns = 0;
//    private int numPassedTurns = 0;
    
    public void clearPlayedTiles() {
		for (int i = 0; i < 15; i++) {
			for (int j = 0; j < 15; j++) {
				if (savedBoard[i][j] == null && currentPlay[i][j] != null) {
					TileButton button = currentPlay[i][j];
					int x = button.getXValue();
					int y = button.getYValue();
					System.out.println("Removing button from " + x + " " + y + " back to tile rack");
					
					SpaceType type = GameLogic.checkSpaceType(x, y);
					SquareButton sb = new SquareButton();
					ImageIcon icon = null;
					
					if (type == SpaceType.BLANK) {
						icon = new ImageIcon("resources/img/gameplay/blank.png");
					} else if (type == SpaceType.CENTER) {
						icon = new ImageIcon("resources/img/gameplay/centerTile.png");
					} else if (type == SpaceType.D_LETTER) {
						icon = new ImageIcon("resources/img/gameplay/doubleLetter.png");
					} else if (type == SpaceType.D_WORD) {
						icon = new ImageIcon("resources/img/gameplay/doubleWord.png");
					} else if (type == SpaceType.T_LETTER) {
						icon = new ImageIcon("resources/img/gameplay/tripleLetter.png");
					} else { // T_WORD
						icon = new ImageIcon("resources/img/gameplay/tripleWord.png");
					}
					sb.setIcon(icon);
					sb.setXValue(x);
					sb.setYValue(y);
					scrabbleBoardSquares[x][y] = sb;
					// set currentPlay[row][col] to null
					currentPlay[x][y] = null;
					
					if(button.getTile().getScore() == 0) {
						blankTileBoard[x][y] = null;
						button.getTile().setChar('-');
						ImageIcon icon2 = new ImageIcon("resources/img/gameplay/blankTile.png");
						button.setIcon(icon2);
						button.getTile().setImage("resources/img/gameplay/blankTile.png");
					}
					
					tileRack.rack.add(button);
					tileRack.tiles.add(button.getTile());
					tileRack.add(button);
					button.setOnBoard(false);
					button.setXValue(0);
					button.setYValue(0);
					// RESET X AND Y OF BUTTON??????
					getscrabbleBoard().remove(button);
					gbc.gridx = y;
					gbc.gridy = x;
					getscrabbleBoard().add(sb, gbc);
					addActions(sb, x, y);
					getGui().repaint();
					getGui().validate();
					tileRack.repaint();
					tileRack.revalidate();
				}
			 }
		}
    }
	// SEND PASS TURN MESSAGE
//    private ScrabbleTileBag bag = new ScrabbleTileBag();
//    private int numTurns = 0;
//    private int numPassedTurns = 0;
//    
//    private String username;
    public void recievePassedTurn(ScrabbleTileBag bag) {
    	numTurns++;
    	numPassedTurns++;
    	this.bag = bag;
    	isMyTurn = true;
    	turnLabel.setText("It's your turn!");
		gui.repaint();
		gui.revalidate();
		JOptionPane.showConfirmDialog(null, versusPlayer + " passed their turn! # of passed turns: " + numPassedTurns, "Recieving " + versusPlayer + "'s Move", JOptionPane.DEFAULT_OPTION);
    	this.enableScrabbleBoard(scrabbleBoard, true);
    	this.enableComponents(gameOptions, true);
    	this.enableComponents(tileRack, true);
    }
    
	// SEND MESSAGE TO CLIENT WITH INFORMATION ABOUT THIS PLAY: + your username
//    protected SquareButton[][] scrabbleBoardSquares = new SquareButton[15][15];
//    protected String[][] savedBoard = new String[15][15];
//    protected String[][] blankTileBoard = new String[15][15];
//		TileButton[][] currentplay
//    private ScrabbleTileBag bag = new ScrabbleTileBag();
//	CorrectTurn ct	  
    public void recievePlayedTurn(CorrectTurn turn, ScrabbleTileBag bag, 
    			TileButton[][] theirPlay, SquareButton[][] scrabbleBoardSquares, String[][] savedBoard, String[][] blankTileBoard) {
    	numTurns++;
    	theirScore += turn.getCurrScore();
    	totalScore += turn.getCurrScore();
    	this.bag = bag;
    	
    	isMyTurn = true;
    	turnLabel.setText("It's your turn!");
    	
    	for (int i = 0; i < 15; i++) {
    		for (int j = 0; j < 15; j++) {
    			if (this.scrabbleBoardSquares[i][j] != null && scrabbleBoardSquares[i][j] == null) {
    				scrabbleBoard.remove(scrabbleBoardSquares[i][j]);
    				this.scrabbleBoardSquares[i][j] = null;
    			} 
    			if (this.savedBoard[i][j] == null && savedBoard[i][j] != null) {
    				this.savedBoard[i][j] = savedBoard[i][j];
    			}
    			if (this.blankTileBoard[i][j] == null && blankTileBoard[i][j] != null) {
    				this.blankTileBoard[i][j] = blankTileBoard[i][j];
    			}
    			
    			if (theirPlay[i][j] != null) {
    				TileButton button = theirPlay[i][j];
    				ScrabbleTile tile2 = new ScrabbleTile(button.getTile().getLetter(), button.getTile().getScore(), button.getTile().getImage());
    				TileButton button2 = new TileButton(tile2);
    				button2.setIcon(new ImageIcon(tile2.getImage()));
    				int x = button.getXValue();
    				int y = button.getYValue();
    				
    				button2.setXValue(x);
    				button2.setYValue(y);
    				button2.setOnBoard(true);
    				gbc.gridx = y;
    				gbc.gridy = x;
    				scrabbleBoard.add(button2, gbc);
    				//scrabbleBoardSquares[x][y] = null;
    				button2.setEnabled(false);
    			}
    		}
    	}

		tileRack.resetMostRecentlyClicked();

		gui.repaint();
		gui.revalidate();
		
		String combined_words = "";
		for (String s: turn.getCurrWord())
			combined_words += s + " | ";
		JOptionPane.showConfirmDialog(null, versusPlayer+ "'s Played Word(s): " + combined_words + "Word Score: " + turn.getCurrScore() + 
				" | Their Score: " + theirScore, "Successful Turn: " + numTurns, JOptionPane.DEFAULT_OPTION);
    	this.enableScrabbleBoard(scrabbleBoard, true);
    	this.enableComponents(gameOptions, true);
    	this.enableComponents(tileRack, true);
    }
	
	/**
	 *  Write a BufferedImage to a File.
	 *
	 *  @param	 image image to be written
	 *  @param	 fileName name of file to be created
	 *  @exception IOException if an error occurs during writing
	*/
	public static void writeImage(BufferedImage image, String fileName)
		throws IOException
	{
		if (fileName == null) return;

		int offset = fileName.lastIndexOf( "." );

		if (offset == -1)
		{
			String message = "file suffix was not specified";
			throw new IOException( message );
		}
		String type = fileName.substring(offset + 1);
		if (types.contains(type))
		{
			ImageIO.write(image, type, new File( fileName ));
		}
		else
		{
			JOptionPane.showConfirmDialog(null, "Invalid image file type", 
    				"Screenshot Error", JOptionPane.DEFAULT_OPTION, JOptionPane.WARNING_MESSAGE);
			throw new IOException("Invalid image file type");
		}


	}
}