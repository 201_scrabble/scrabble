package Scrabble_Game_Play;

import java.util.ArrayList;
import java.util.Vector;

public final class GameLogic {
	private static Constants constants;
	private static Trie dictionary;
	
	public GameLogic(Constants constants, Trie dictionary) {
		this.constants = constants;
		this.dictionary = dictionary;
	}
    // TAKES IN ROW,COL
    // RETURNS SPACETYPE FROM OG SCRABBLE BOARD
    public static SpaceType checkSpaceType(int ii, int jj) {
    	if (ii == 7 && jj == 7)
    		return SpaceType.CENTER;
    	else if ((ii == jj) && ((ii == 0) || (ii == 14)))
        	return SpaceType.T_WORD;
        else if ((ii == jj) && ((ii == 5) || (ii == 9)))
        	return SpaceType.T_LETTER;
        else if ((ii == jj) && ((ii == 6) || (ii == 8)))
        	return SpaceType.D_LETTER;
        else if (((14-ii) == jj) && (((14-ii) == 0) || ((14-ii) == 14)))
        	return SpaceType.T_WORD;
        else if (((14-ii) == jj) && (((14-ii) == 5) || ((14-ii) == 9)))
        	return SpaceType.T_LETTER;
        else if (((14-ii) == jj) && (((14-ii) == 6) || ((14-ii) == 8)))
        	return SpaceType.D_LETTER;
        else if (((ii == 0 || ii == 14) && (jj == 7)) || ((jj == 0 || jj == 14) && (ii == 7)))
        	return SpaceType.T_WORD;
        else if (((ii == 0 || ii == 14) && (jj == 3 || jj == 11)) || 
        		((jj == 0 || jj == 14) && (ii == 3 || ii == 11)))
        	return SpaceType.D_LETTER;
        else if (((ii == 1 || ii == 13) && (jj == 5 || (14-jj) == 5)) ||
        		((jj == 1 || jj == 13) && (ii == 5 || (14-ii) == 5)))
        	return SpaceType.T_LETTER;
        else if (((ii == 2 || ii == 12) && (jj == 6 || (14-jj) == 6)) ||
        		((jj == 2 || jj == 2) && (ii == 6 || (14-ii) == 6)))
        	return SpaceType.D_LETTER;
        else if (((ii == 7) && (jj == 3 || (14-jj) == 3)) ||
        		((jj == 7) && (ii == 3 || (14-ii) == 3)))
        	return SpaceType.D_LETTER;
        else if (ii == jj)
        	return SpaceType.D_WORD;
        else if ((14-ii) == jj)
        	return SpaceType.D_WORD;
        else
        	return SpaceType.BLANK;
    }
    
    public static CorrectTurn checkFirstTurnConfig(TileButton[][] currentPlay, String[][] savedBoard) {
    	ArrayList<Location> locs = new ArrayList<Location>();
    	boolean centerTileFilled = false;
    	for (int i = 0; i<15; i++) {
    		for (int j = 0; j<15; j++) {
    		
    			if (currentPlay[i][j] != null) {
    				if (i == 7 && j ==7) {
        				centerTileFilled = true;
        			}
    				locs.add(new Location(i,j));
    				System.out.println("Adding to locs: " + currentPlay[i][j].getTile().getLetter() + " (" + i +","+j+")");
    			}
    		}
    	}
    	if (!centerTileFilled) {
    		return null;
    	}
    	
    	if (locs.isEmpty()) return null;
    	
    	boolean sameRow = true;
    	boolean sameCol = true;
    	int curRow = locs.get(0).getRow();
    	int curCol = locs.get(0).getCol();
    	for (int i = 1; i < locs.size(); i++) {
    		if (curRow != locs.get(i).getRow())
    			sameRow = false;
    		if (curCol != locs.get(i).getCol())
    			sameCol = false;
    	}
    	
    	if (!sameRow && !sameCol) return null;
    	else {
    		if (sameRow && !sameCol) {
    			// SORTING IN COL ORDER
    			System.out.println("SAME ROW FOR FIRST TURN");
    			Location temp = null;
    			for (int i = 0; i < locs.size(); i++) {
    				for (int j = 1; j < locs.size()-i; j++) {
    					if (locs.get(j-1).getCol() > locs.get(j).getCol()) {
    						temp = locs.get(j-1);
    						locs.set(j-1, locs.get(i));
    						locs.set(j, temp);
    					}
    				}
    			}
    			
    			// CHECKING FOR GAPS
    			for (int i = 0; i < locs.size()-1; i++) {
    				if((locs.get(i).getCol()+1) == locs.get(i+1).getCol()) {
    					continue;
    				} else {
    					return null;
    				}
    			}
    			int score = 0;
    			String word = "";
    			boolean tripleW = false;
    			boolean doubleW = true;
    			for (int i = 0; i < locs.size(); i++) {
    				int r = locs.get(i).getRow();
    				int c = locs.get(i).getCol();
    				TileButton t = currentPlay[r][c];
    				word += t.getTile().getLetter(); 
    				if (t.getTile().getScore() == 0) {
    					score += 0;
    				}
    				// CHECK FOR BLANK TILE
    				else {
	    				SpaceType type = checkSpaceType(r,c);
						if (type == SpaceType.BLANK) {
							score += t.getTile().getScore();
						} else if (type == SpaceType.CENTER) {
							score += t.getTile().getScore();
							doubleW = true;
						} else if (type == SpaceType.D_LETTER) {
							score += 2*t.getTile().getScore();
						} else if (type == SpaceType.D_WORD) {
							score += t.getTile().getScore();
							doubleW = true;
						} else if (type == SpaceType.T_LETTER) {
							score += 3*t.getTile().getScore();
						} else { // T_WORD
							score += t.getTile().getScore();
							tripleW = true;
						}
    				}
    			}
    			if (doubleW) score = score*2;
    			if (tripleW) score = score*3;
    			
    			System.out.println("Checking: " + word.toLowerCase());
    			if (dictionary.contains(word.toLowerCase())) {
    				System.out.println("word is found in dictionary");
    				for (int i = 0; i < word.length(); i++) {
    					savedBoard[locs.get(i).getRow()][locs.get(i).getCol()] = ""+word.charAt(i);
    				}
    				ArrayList<String> words = new ArrayList<String>();
    				words.add(word);
    				return new CorrectTurn(words, score);
    			} else {
    				System.out.println("Word is NOT found in dictionary");
    				return null;
    			}
    			
    			
    		} else if (!sameRow && sameCol) {
    			System.out.println("SAME COL FOR FIRST TURN");
    			// SORTING IN ROW ORDER
    			Location temp = null;
    			for (int i = 0; i < locs.size(); i++) {
    				for (int j = 1; j < locs.size()-i; j++) {
    					if (locs.get(j-1).getRow() > locs.get(j).getRow()) {
    						temp = locs.get(j-1);
    						locs.set(j-1, locs.get(i));
    						locs.set(j, temp);
    					}
    				}
    			}
    			for (int i = 0; i < locs.size()-1; i++) {
    				if((locs.get(i).getRow()+1) == locs.get(i+1).getRow()) {
    					continue;
    				} else {
    					return null;
    				}
    			}
    			int score = 0;
    			String word = "";
    			boolean tripleW = false;
    			boolean doubleW = true;
    			for (int i = 0; i < locs.size(); i++) {
    				int r = locs.get(i).getRow();
    				int c = locs.get(i).getCol();
    				TileButton t = currentPlay[r][c];
    				word += t.getTile().getLetter();
    				if (t.getTile().getScore() == 0) {
    					
    				} else {
    				
	    				SpaceType type = checkSpaceType(r,c);
						if (type == SpaceType.BLANK) {
							score += t.getTile().getScore();
						} else if (type == SpaceType.CENTER) {
							score += t.getTile().getScore();
							doubleW = true;
						} else if (type == SpaceType.D_LETTER) {
							score += 2*t.getTile().getScore();
						} else if (type == SpaceType.D_WORD) {
							score += t.getTile().getScore();
							doubleW = true;
						} else if (type == SpaceType.T_LETTER) {
							score += 3*t.getTile().getScore();
						} else { // T_WORD
							score += t.getTile().getScore();
							tripleW = true;
						}
    				}
    			}
    			if (doubleW) score = score*2;
    			if (tripleW) score = score*3;
    			
    			System.out.println("Checking: " + word.toLowerCase());
    			if (dictionary.contains(word.toLowerCase())) {
    				System.out.println("word is found in dictionary");
    				for (int i = 0; i < word.length(); i++) {
    					savedBoard[locs.get(i).getRow()][locs.get(i).getCol()] = ""+word.charAt(i);
    				}
    				ArrayList<String> words = new ArrayList<String>();
    				words.add(word);
     				return new CorrectTurn(words, score);
    			} else {
    				System.out.println("word is NOT found in dictionary");
    				return null;
    			}
    			
    		} else { // WHAT IF THERE'S ONLY ONE TILE? THEN SAMEROW AND SAMECOL ARE BOTH TRUE
    			return null;
    		}
    	}
    }
     
    public static CorrectTurn checkIfValidConfiguration(TileButton[][] currentPlay, String[][] savedBoard, TileRackGUI tileRack, String[][] blankTileBoard) {
    	ArrayList<Location> locs = new ArrayList<Location>();
    	
    	for (int i = 0; i<15; i++) {
    		for (int j = 0; j<15; j++) {
    			if (currentPlay[i][j] != null) {
    				System.out.println("Adding to locs: " + currentPlay[i][j].getTile().getLetter() + " (" + i +","+j+")");
    				locs.add(new Location(i,j));
    				
    			}
    		}
    	}
    	
    	if (locs.isEmpty()) return null;
    	
    	
    	
    	boolean sameRow = true;
    	boolean sameCol = true;
    	int curRow = locs.get(0).getRow();
    	int curCol = locs.get(0).getCol();
    	for (int i = 1; i < locs.size(); i++) {
 
    		if (curRow != locs.get(i).getRow())
    			sameRow = false;
    		if (curCol != locs.get(i).getCol())
    			sameCol = false;
    	}
    	
    	if (!sameRow && !sameCol) { System.out.println("Not the same row or col, returning false"); return null; }
    	else {
 
			CorrectTurn verticalCorrect = getAllVerticalWords(locs, currentPlay, savedBoard, blankTileBoard);
			CorrectTurn horizontalCorrect = getAllHorizontalWords(locs, currentPlay, savedBoard, blankTileBoard);
//			for (String s: verticalWords) {
//				System.out.println("Vertical word: " + s);
//			}
//			for (String s: horizontalWords) {
//				System.out.println("Horizontal word: " + s);
//			}
//			
			if (verticalCorrect == null || horizontalCorrect == null) {
				return null;	
			}
			
			ArrayList<String> combined = new ArrayList<String>();
			for (String s: verticalCorrect.getCurrWord()) { combined.add(s); System.out.println("Adding: " + s + " to combined"); }
			for (String s: horizontalCorrect.getCurrWord()) { combined.add(s); System.out.println("Adding: " + s + " to combined"); }
			int combined_score = verticalCorrect.getCurrScore() + horizontalCorrect.getCurrScore(); 
			if(tileRack.getRack().size() == 0) {
				combined_score += 50;
			}
			return new CorrectTurn(combined, combined_score);
    	}

    }

	public static CorrectTurn getAllVerticalWords(ArrayList<Location> locs, TileButton[][] currentPlay, String[][] savedBoard, String[][] blankTileBoard) {
		
		ArrayList<Location> verticalNeighbors = new ArrayList<Location>();
		for(int i = 0; i < locs.size(); i++) {
			if (!containsLocation(verticalNeighbors, locs.get(i).getRow(), locs.get(i).getCol()))
					verticalNeighbors.add(new Location(locs.get(i).getRow(),locs.get(i).getCol()));
			// SOUTH
			if ((locs.get(i).getRow()+1) < 15 && savedBoard[locs.get(i).getRow()+1][locs.get(i).getCol()] != null) {
				
				if (!containsLocation(verticalNeighbors, locs.get(i).getRow()+1, locs.get(i).getCol()))
					verticalNeighbors.add(new Location(locs.get(i).getRow()+1,locs.get(i).getCol()));
				
				int south = locs.get(i).getRow()+2;
				while (south < 15 && savedBoard[south][locs.get(i).getCol()] != null) {
					if (!containsLocation(verticalNeighbors, south, locs.get(i).getCol()))
						verticalNeighbors.add(new Location(south,locs.get(i).getCol()));
					south++;
				}
			}
			// NORTH
			if ((locs.get(i).getRow()-1) >= 0 && savedBoard[locs.get(i).getRow()-1][locs.get(i).getCol()] != null) {
				if (!containsLocation(verticalNeighbors, locs.get(i).getRow()-1, locs.get(i).getCol()))
					verticalNeighbors.add(new Location(locs.get(i).getRow()-1,locs.get(i).getCol()));
				
				int north = locs.get(i).getRow()-2;
				while (north >= 0 && savedBoard[north][locs.get(i).getCol()] != null) {
					if (!containsLocation(verticalNeighbors, north, locs.get(i).getCol()))
						verticalNeighbors.add(new Location(north,locs.get(i).getCol()));
					north--;
				}
			}		
		}
		
		Location temp = null;
		for (int i = 0; i < verticalNeighbors.size(); i++) {
			for (int j = 1; j < verticalNeighbors.size()-i; j++) {
				
				if (verticalNeighbors.get(j-1).getCol() > verticalNeighbors.get(j).getCol()) {
					temp = verticalNeighbors.get(j-1);
					verticalNeighbors.set(j-1, verticalNeighbors.get(i));
					verticalNeighbors.set(j, temp);
				}
				
			}
		}
		
		Vector<Vector<Location> > verticalWords = new Vector<Vector<Location> >(); 
		int index = 0;
		int curCol = verticalNeighbors.get(0).getCol();

		while (index < verticalNeighbors.size()) {
			Vector<Location> currWord = new Vector<Location>();
			while (index < verticalNeighbors.size() && verticalNeighbors.get(index).getCol() == curCol) {
				currWord.add(verticalNeighbors.get(index));
				index++;
			}
			verticalWords.add(currWord);
			if (index < verticalNeighbors.size())
				curCol = verticalNeighbors.get(index).getCol();
		}
		
		for (int i = 0; i < verticalWords.size(); i++) {
			System.out.println("Sorting vertical words by row");
			Vector<Location> currWord = verticalWords.get(i);
			Location temp2 = null;
			for (int j = 0; j < currWord.size(); j++) {
				for (int k = 1; k < currWord.size()-j; k++) {
					if (currWord.get(k-1).getRow() > currWord.get(k).getRow()) {
						System.out.println("k-1's row" + currWord.get(k-1).getRow() + " k's row " +currWord.get(k).getRow() );
						temp2 = currWord.get(k-1);
						currWord.set(k-1, currWord.get(k));
						currWord.set(k, temp2);
					}
				}
			}
		}
		
		Vector<String> words = new Vector<String>();
		ArrayList<ArrayList<Location> > word_locs = new ArrayList<ArrayList<Location> >();
		for (int i = 0; i < verticalWords.size(); i++) {
			Vector<Location> currWord = verticalWords.get(i);
			ArrayList<Location> currLocs = new ArrayList<Location>();
			String word = "";
			for (int j = 0; j < currWord.size(); j++) {

				if (savedBoard[currWord.get(j).getRow()][currWord.get(j).getCol()] != null) {
					word += savedBoard[currWord.get(j).getRow()][currWord.get(j).getCol()];
				} else if (currentPlay[currWord.get(j).getRow()][currWord.get(j).getCol()] != null)  {
					word += "" + currentPlay[currWord.get(j).getRow()][currWord.get(j).getCol()].getTile().getLetter();
				}
				currLocs.add(new Location(currWord.get(j).getRow(), currWord.get(j).getCol()));
				System.out.println("adding to curr locs: " + currWord.get(j).getRow() + " " +  currWord.get(j).getCol());
			}
			if (word.length() > 1) {
				words.add(word);
				word_locs.add(currLocs);
			}
		}	
		
		ArrayList<String> correct_words = new ArrayList<String>();
		int sum_score = 0;
		for (int i = 0 ; i < words.size(); i++) {
			if (dictionary.contains(words.get(i).toLowerCase())) {
				ArrayList<Location> currLocs = word_locs.get(i);
				int s = calculateScore(words.get(i),currLocs, blankTileBoard, savedBoard);
				sum_score += s;
				correct_words.add(words.get(i));
				System.out.println("Scoring: " + words.get(i) + " " + s);
			} else {
				return null;
			}
		}
		return new CorrectTurn(correct_words,sum_score);
	}
	
	public static CorrectTurn getAllHorizontalWords(ArrayList<Location> locs, TileButton[][] currentPlay, String[][] savedBoard, String[][] blankTileBoard) {
		
		ArrayList<Location> horizontalNeighbors = new ArrayList<Location>();
		for(int i = 0; i < locs.size(); i++) {
			if (!containsLocation(horizontalNeighbors, locs.get(i).getRow(), locs.get(i).getCol()))
				horizontalNeighbors.add(new Location(locs.get(i).getRow(),locs.get(i).getCol()));
			// EAST
			if ((locs.get(i).getCol()+1) < 15 && savedBoard[locs.get(i).getRow()][locs.get(i).getCol()+1] != null) {
				if (!containsLocation(horizontalNeighbors, locs.get(i).getRow(), locs.get(i).getCol()+1))
					horizontalNeighbors.add(new Location(locs.get(i).getRow(),locs.get(i).getCol()+1));
				
				int east = locs.get(i).getCol()+2;
				while (east < 15 && savedBoard[locs.get(i).getRow()][east] != null) {
					if (!containsLocation(horizontalNeighbors, locs.get(i).getRow(), east))
						horizontalNeighbors.add(new Location(locs.get(i).getRow(), east));
					east++;
				}
			}
			// WEST
			if ((locs.get(i).getCol()-1) >= 0 && savedBoard[locs.get(i).getRow()][locs.get(i).getCol()-1] != null) {
				if (!containsLocation(horizontalNeighbors, locs.get(i).getRow(), locs.get(i).getCol()-1))
					horizontalNeighbors.add(new Location(locs.get(i).getRow(),locs.get(i).getCol()-1));
				System.out.println("adding west horizontal neighbor: " + locs.get(i).getRow() + " " + (locs.get(i).getCol()-1));
				int west = locs.get(i).getCol()-2;
				while (west >= 0 && savedBoard[locs.get(i).getRow()][west] != null) {
					if (!containsLocation(horizontalNeighbors, locs.get(i).getRow(), west))
						horizontalNeighbors.add(new Location(locs.get(i).getRow(), west));
					west--;
				}
			}		
		}
		
		Location temp = null;
		for (int i = 0; i < horizontalNeighbors.size(); i++) {
			for (int j = 1; j < horizontalNeighbors.size()-i; j++) {
				
				if (horizontalNeighbors.get(j-1).getRow() > horizontalNeighbors.get(j).getRow()) {
					temp = horizontalNeighbors.get(j-1);
					horizontalNeighbors.set(j-1, horizontalNeighbors.get(i));
					horizontalNeighbors.set(j, temp);
				}
				
			}
		}
		
		Vector<Vector<Location> > horizontalWords = new Vector<Vector<Location> >(); 
		int index = 0;
		int curRow = horizontalNeighbors.get(0).getRow();

		while (index < horizontalNeighbors.size()) {
			Vector<Location> currWord = new Vector<Location>();
			while (index < horizontalNeighbors.size() && horizontalNeighbors.get(index).getRow() == curRow) {
				currWord.add(horizontalNeighbors.get(index));
				index++;
			}
			horizontalWords.add(currWord);
			if (index < horizontalNeighbors.size())
				curRow = horizontalNeighbors.get(index).getRow();
		}
		
		for (int i = 0; i < horizontalWords.size(); i++) {
			Vector<Location> currWord = horizontalWords.get(i);
			Location temp2 = null;
			for (int j = 0; j < currWord.size(); j++) {
				for (int k = 1; k < currWord.size()-j; k++) {
					if (currWord.get(k-1).getCol() > currWord.get(k).getCol()) {
						temp2 = currWord.get(k-1);
						currWord.set(k-1, currWord.get(k));
						currWord.set(k, temp2);
					}
				}
			
			}
		}
		
		Vector<String> words = new Vector<String>();
		ArrayList<ArrayList<Location> > word_locs = new ArrayList<ArrayList<Location> >();
		for (int i = 0; i < horizontalWords.size(); i++) {
			Vector<Location> currWord = horizontalWords.get(i);
			ArrayList<Location> currLocs = new ArrayList<Location>();
			String word = "";
			for (int j = 0; j < currWord.size(); j++) {
				if (savedBoard[currWord.get(j).getRow()][currWord.get(j).getCol()] != null) {
					word += savedBoard[currWord.get(j).getRow()][currWord.get(j).getCol()];
				} else if (currentPlay[currWord.get(j).getRow()][currWord.get(j).getCol()] != null)  {
					word += "" + currentPlay[currWord.get(j).getRow()][currWord.get(j).getCol()].getTile().getLetter();
				}
				currLocs.add(new Location(currWord.get(j).getRow(), currWord.get(j).getCol()));
				System.out.println("adding to curr locs: " + currWord.get(j).getRow() + " " +  currWord.get(j).getCol());
			}
			if (word.length() > 1) {
				words.add(word);
				word_locs.add(currLocs);
			}
		}	
		
		ArrayList<String> correct_words = new ArrayList<String>();
		int sum_score = 0;
		for (int i = 0 ; i < words.size(); i++) {
			if (dictionary.contains(words.get(i).toLowerCase())) {
				ArrayList<Location> currLocs = word_locs.get(i);
				int s = calculateScore(words.get(i),currLocs, blankTileBoard, savedBoard);
				sum_score += s;
				correct_words.add(words.get(i));
				System.out.println("Scoring: " + words.get(i) + " " + s);
			} else {
				return null;
			}
		}
		return new CorrectTurn(correct_words,sum_score);
	}
	
	public static boolean containsLocation(ArrayList<Location> locs, int row, int col) {
		for (Location l: locs) {
			if (l.getRow() == row && l.getCol() == col)
				return true;
		}
		return false;
	}
	
	public static int calculateScore(String word, ArrayList<Location> letter_locs, String[][] blankTileBoard, String[][] savedBoard) {
		int score = 0;
		boolean tripleW = false;
		boolean doubleW = false;
		int numDouble = 0;
		int numTriple = 0;
		System.out.println("PRINTING OUT LETTER LOCS");
		for(int i = 0; i < letter_locs.size(); i++) {
			System.out.println(letter_locs.get(i).getRow() + " " + letter_locs.get(i).getCol());
		}
		System.out.println("word is: " + word);
		for (int i = 0; i < word.length(); i++) {
			int r = letter_locs.get(i).getRow();
			int c = letter_locs.get(i).getCol();
			// CHECK FOR BLANK TILE
			
			SpaceType type = checkSpaceType(r,c);
			if (blankTileBoard[r][c] == null) {
				System.out.println("Letter is: " + blankTileBoard[r][c]);
				
				if (type == SpaceType.BLANK) {
					score += constants.letterScores.get(""+word.charAt(i));
				} else if (type == SpaceType.D_LETTER && savedBoard[r][c] == null) {
					score += 2*constants.letterScores.get(""+word.charAt(i));
				} else if (type == SpaceType.D_WORD && savedBoard[r][c] == null) {
					score += constants.letterScores.get(""+word.charAt(i));
					doubleW = true;
					numDouble++;
				} else if (type == SpaceType.T_LETTER && savedBoard[r][c] == null) {
					score += 3*constants.letterScores.get(""+word.charAt(i));
				} else if (type == SpaceType.T_WORD && savedBoard[r][c] == null){ // T_WORD
					score += constants.letterScores.get(""+word.charAt(i));
					tripleW = true;
					numTriple++;
				} else {
					score += constants.letterScores.get(""+word.charAt(i));
				}
			} else {
				if (type == SpaceType.D_WORD && savedBoard[r][c] == null) {
					doubleW = true;
					numDouble++;
				} else if (type == SpaceType.T_WORD && savedBoard[r][c] == null) {
					tripleW = true;
					numTriple++;
				}
				
			}
		}
		if (doubleW) {
			for (int i = 0; i < numDouble; i++)
				score = score*2;
		}
		if (tripleW) {
			for (int i = 0; i < numTriple; i++)
				score = score*3;
		}
		
		return score;
	}
	
	
}
