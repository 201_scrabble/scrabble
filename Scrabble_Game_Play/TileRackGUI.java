package Scrabble_Game_Play;
import java.awt.Graphics;
import java.awt.Toolkit;
import java.util.ArrayList;

import javax.swing.ImageIcon;
import javax.swing.JOptionPane;
import javax.swing.JPanel;

class TileRackGUI extends JPanel {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	protected ArrayList<TileButton> rack;
	protected ArrayList<ScrabbleTile> tiles;
	private TileButton mostRecent; // should this be reset after shuffling/replacing rack???
	private SinglePlayerGUI sbg;
	private MultiPlayerGUI mpg;
	
	public TileRackGUI(SinglePlayerGUI sbg) {
		super();
		rack = new ArrayList<TileButton>();
		tiles = new ArrayList<ScrabbleTile>();
		mostRecent = null;
		this.sbg = sbg;
		this.mpg = null;
	}
	
	public TileRackGUI(MultiPlayerGUI mpg) {
		super();
		rack = new ArrayList<TileButton>();
		tiles = new ArrayList<ScrabbleTile>();
		mostRecent = null;
		this.mpg = mpg;
		this.sbg = null;
	}
	
	public void setRack(ArrayList<ScrabbleTile> st) {
		for (ScrabbleTile tile : st) {
			TileButton tb = new TileButton(tile);
			rack.add(tb);
			tiles.add(tile);
			add(tb);
		}
		
		repaint();
		revalidate();
		for (TileButton button: rack) {
			if (sbg != null)
				addActionsForSingle(button);
			else if (mpg != null)
				addActionsForMulti(button);
		}
	}
	
	public void resetRack(ArrayList<ScrabbleTile> st) {
		rack.clear();
		tiles.clear();
		this.removeAll();
		
		for (ScrabbleTile tile : st) {
			TileButton tb = new TileButton(tile);
			rack.add(tb);
			tiles.add(tile);
			add(tb);
		}
		repaint();
		revalidate();
		for (TileButton button: rack) {
			if (sbg != null)
				addActionsForSingle(button);
			else if (mpg != null)
				addActionsForMulti(button);
		}
	}
	
	public void addActionsForSingle(TileButton button) {
			button.addActionListener((event) -> {
				if (!button.isOnBoard()) {
					System.out.println("Button isn't on board!");
					mostRecent = button;
					sbg.undoSelection.setEnabled(true);
				} else if (sbg.savedBoard[button.getXValue()][button.getYValue()] == null){
					if (mostRecent == null) {
						System.out.println("Most recent is null");
						Object[] options = {"Move back", "Do not move back"};
						int val = JOptionPane.showOptionDialog(null, "Move this tile back to the tile rack?", "Clicked on scrabble tile on the board", JOptionPane.YES_NO_OPTION, 
								JOptionPane.QUESTION_MESSAGE, null, options, options[0]);
						if (val == JOptionPane.YES_OPTION) {
							System.out.println("Clicked on tile already on the board. Wants to move it back to rack");
							// get row and col of button
							int x = button.getXValue();
							int y = button.getYValue();
							System.out.println("Removing button from " + x + " " + y + " back to tile rack");
							
							SpaceType type = GameLogic.checkSpaceType(x, y);
							SquareButton sb = new SquareButton();
							ImageIcon icon = null;
							
							if (type == SpaceType.BLANK) {
								icon = new ImageIcon("resources/img/gameplay/blank.png");
							} else if (type == SpaceType.CENTER) {
								icon = new ImageIcon("resources/img/gameplay/centerTile.png");
							} else if (type == SpaceType.D_LETTER) {
								icon = new ImageIcon("resources/img/gameplay/doubleLetter.png");
							} else if (type == SpaceType.D_WORD) {
								icon = new ImageIcon("resources/img/gameplay/doubleWord.png");
							} else if (type == SpaceType.T_LETTER) {
								icon = new ImageIcon("resources/img/gameplay/tripleLetter.png");
							} else { // T_WORD
								icon = new ImageIcon("resources/img/gameplay/tripleWord.png");
							}
							sb.setIcon(icon);
							sb.setXValue(x);
							sb.setYValue(y);
							sbg.scrabbleBoardSquares[x][y] = sb;
							// set currentPlay[row][col] to null
							sbg.currentPlay[x][y] = null;
							
							if(button.getTile().getScore() == 0) {
								sbg.blankTileBoard[x][y] = null;
								button.getTile().setChar('-');
								ImageIcon icon2 = new ImageIcon("resources/img/gameplay/blankTile.png");
								button.setIcon(icon2);
								button.getTile().setImage("resources/img/gameplay/blankTile.png");
							}
							
							rack.add(button);
							tiles.add(button.getTile());
							add(button);
							button.setOnBoard(false);
							button.setXValue(0);
							button.setYValue(0);
							// RESET X AND Y OF BUTTON??????
							sbg.getscrabbleBoard().remove(button);
							sbg.gbc.gridx = y;
							sbg.gbc.gridy = x;
							sbg.getscrabbleBoard().add(sb, sbg.gbc);
							sbg.addActions(sb, x, y);
							sbg.getGui().repaint();
							sbg.getGui().validate();
							repaint();
							revalidate();
						}
					} else {
						System.out.println("most recent is not null?");
						Object[] options = {"Replace", "Do not replace"};
						int val = JOptionPane.showOptionDialog(null, "Replace this tile with the last selected tile?", "Clicked on scrabble tile on the board", JOptionPane.YES_NO_OPTION, 
								JOptionPane.QUESTION_MESSAGE, null, options, options[0]);
						if (val == JOptionPane.YES_OPTION) {
							System.out.println("Clicked on tile already on the board. Wants to replace it with another tile");
							// get the location of button 
							int x = button.getXValue();
							int y = button.getYValue();
							// remove button from scrabble board
							sbg.getscrabbleBoard().remove(button);
							
							if (mostRecent.getTile().getLetter() == '-') {
	    						Object[] options2 = {'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N',
	    								'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z'};
	    						char s = (char) JOptionPane.showInputDialog(null, "Choose a letter for your blank tile: ",
	    								"Select Letter", JOptionPane.PLAIN_MESSAGE, null, options2, 'A');
	    						if (s != ' ') {
	    							String image = "resources/img/gameplay/" + s + ".jpg";
	    							mostRecent.getTile().setImage(image);
	    							mostRecent.getTile().setChar(s);
	    							ImageIcon icon = new ImageIcon(image);
	    							mostRecent.setIcon(icon);
	    							repaint();
	    							revalidate();
	    							sbg.blankTileBoard[mostRecent.getXValue()][mostRecent.getYValue()] = "" + s;
	    						}
	    					}
								
							sbg.currentPlay[x][y] = mostRecent;
							// add mostrecent to scrabble board in that location
							sbg.gbc.gridx = y;
							sbg.gbc.gridy = x;
							sbg.getscrabbleBoard().add(mostRecent, sbg.gbc);
							// set mostrecent onBoard to true
							mostRecent.setOnBoard(true);
							
							// repaint/revalidate
							sbg.getGui().repaint();
							sbg.getGui().revalidate();
							
							
							if(button.getTile().getScore() == 0) {
								if (mostRecent.getTile().getScore() != 0) {
									sbg.blankTileBoard[x][y] = null;
								}
								button.getTile().setChar('-');
								ImageIcon icon2 = new ImageIcon("resources/img/gameplay/blankTile.png");
								button.setIcon(icon2);
								button.getTile().setImage("resources/img/gameplay/blankTile.png");
							}
							
							// ADD BUTTON BACK TO RACK
							rack.add(button);
							tiles.add(button.getTile());
							add(button);						
							button.setOnBoard(false);
							button.setXValue(0);
							button.setYValue(0);
							
							// REMOVE MOST RECENT FROM RACK
							rack.remove(mostRecent);
							tiles.remove(mostRecent.getTile());
							remove(mostRecent);
							mostRecent.setXValue(x);
							mostRecent.setYValue(y);
							
							resetMostRecentlyClicked();
							
							repaint();
							revalidate();
						}						
					}
					
				}
			});
		
	}
	
	public void addActionsForMulti(TileButton button) {
		button.addActionListener((event) -> {
			if (!button.isOnBoard()) {
				System.out.println("Button isn't on board!");
				mostRecent = button;
				mpg.undoSelection.setEnabled(true);
			} else if (mpg.savedBoard[button.getXValue()][button.getYValue()] == null){
				if (mostRecent == null) {
					System.out.println("Most recent is null");
					Object[] options = {"Move back", "Do not move back"};
					int val = JOptionPane.showOptionDialog(null, "Move this tile back to the tile rack?", "Clicked on scrabble tile on the board", JOptionPane.YES_NO_OPTION, 
							JOptionPane.QUESTION_MESSAGE, null, options, options[0]);
					if (val == JOptionPane.YES_OPTION) {
						System.out.println("Clicked on tile already on the board. Wants to move it back to rack");
						// get row and col of button
						int x = button.getXValue();
						int y = button.getYValue();
						System.out.println("Removing button from " + x + " " + y + " back to tile rack");
						
						SpaceType type = GameLogic.checkSpaceType(x, y);
						SquareButton sb = new SquareButton();
						ImageIcon icon = null;
						
						if (type == SpaceType.BLANK) {
							icon = new ImageIcon("resources/img/gameplay/blank.png");
						} else if (type == SpaceType.CENTER) {
							icon = new ImageIcon("resources/img/gameplay/centerTile.png");
						} else if (type == SpaceType.D_LETTER) {
							icon = new ImageIcon("resources/img/gameplay/doubleLetter.png");
						} else if (type == SpaceType.D_WORD) {
							icon = new ImageIcon("resources/img/gameplay/doubleWord.png");
						} else if (type == SpaceType.T_LETTER) {
							icon = new ImageIcon("resources/img/gameplay/tripleLetter.png");
						} else { // T_WORD
							icon = new ImageIcon("resources/img/gameplay/tripleWord.png");
						}
						sb.setIcon(icon);
						sb.setXValue(x);
						sb.setYValue(y);
						mpg.scrabbleBoardSquares[x][y] = sb;
						// set currentPlay[row][col] to null
						mpg.currentPlay[x][y] = null;
						
						if(button.getTile().getScore() == 0) {
							mpg.blankTileBoard[x][y] = null;
							button.getTile().setChar('-');
							ImageIcon icon2 = new ImageIcon("resources/img/gameplay/blankTile.png");
							button.setIcon(icon2);
							button.getTile().setImage("resources/img/gameplay/blankTile.png");
						}
						
						rack.add(button);
						tiles.add(button.getTile());
						add(button);
						button.setOnBoard(false);
						button.setXValue(0);
						button.setYValue(0);
						// RESET X AND Y OF BUTTON??????
						mpg.getscrabbleBoard().remove(button);
						mpg.gbc.gridx = y;
						mpg.gbc.gridy = x;
						mpg.getscrabbleBoard().add(sb, mpg.gbc);
						mpg.addActions(sb, x, y);
						mpg.getGui().repaint();
						mpg.getGui().validate();
						repaint();
						revalidate();
					}
				} else {
					System.out.println("most recent is not null?");
					Object[] options = {"Replace", "Do not replace"};
					int val = JOptionPane.showOptionDialog(null, "Replace this tile with the last selected tile?", "Clicked on scrabble tile on the board", JOptionPane.YES_NO_OPTION, 
							JOptionPane.QUESTION_MESSAGE, null, options, options[0]);
					if (val == JOptionPane.YES_OPTION) {
						System.out.println("Clicked on tile already on the board. Wants to replace it with another tile");
						// get the location of button 
						int x = button.getXValue();
						int y = button.getYValue();
						// remove button from scrabble board
						mpg.getscrabbleBoard().remove(button);
						
						if (mostRecent.getTile().getLetter() == '-') {
    						Object[] options2 = {'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N',
    								'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z'};
    						char s = (char) JOptionPane.showInputDialog(null, "Choose a letter for your blank tile: ",
    								"Select Letter", JOptionPane.PLAIN_MESSAGE, null, options2, 'A');
    						if (s != ' ') {
    							String image = "resources/img/gameplay/" + s + ".jpg";
    							mostRecent.getTile().setImage(image);
    							mostRecent.getTile().setChar(s);
    							ImageIcon icon = new ImageIcon(image);
    							mostRecent.setIcon(icon);
    							repaint();
    							revalidate();
    							mpg.blankTileBoard[mostRecent.getXValue()][mostRecent.getYValue()] = "" + s;
    						}
    					}
							
						mpg.currentPlay[x][y] = mostRecent;
						// add mostrecent to scrabble board in that location
						mpg.gbc.gridx = y;
						mpg.gbc.gridy = x;
						mpg.getscrabbleBoard().add(mostRecent, mpg.gbc);
						// set mostrecent onBoard to true
						mostRecent.setOnBoard(true);
						
						// repaint/revalidate
						mpg.getGui().repaint();
						mpg.getGui().revalidate();
						
						
						if(button.getTile().getScore() == 0) {
							if (mostRecent.getTile().getScore() != 0) {
								mpg.blankTileBoard[x][y] = null;
							}
							button.getTile().setChar('-');
							ImageIcon icon2 = new ImageIcon("resources/img/gameplay/blankTile.png");
							button.setIcon(icon2);
							button.getTile().setImage("resources/img/gameplay/blankTile.png");
						}
						
						// ADD BUTTON BACK TO RACK
						rack.add(button);
						tiles.add(button.getTile());
						add(button);						
						button.setOnBoard(false);
						button.setXValue(0);
						button.setYValue(0);
						
						// REMOVE MOST RECENT FROM RACK
						rack.remove(mostRecent);
						tiles.remove(mostRecent.getTile());
						remove(mostRecent);
						mostRecent.setXValue(x);
						mostRecent.setYValue(y);
						
						resetMostRecentlyClicked();
						
						repaint();
						revalidate();
					}						
				}
				
			}
		});
	
}
	
	public ArrayList<ScrabbleTile> getTiles() {
		return tiles;
	}
	
	public ArrayList<TileButton> getRack() {
		return rack;
	}
	
	public TileButton getMostRecentlyClicked() {
		return mostRecent;
	}
	
	public void resetMostRecentlyClicked() {
		mostRecent = null;
		if (sbg != null)
			sbg.undoSelection.setEnabled(false);
		else if (mpg != null)
			mpg.undoSelection.setEnabled(false);
	}
	
	public void paintComponent(Graphics g) {
		g.drawImage(Toolkit.getDefaultToolkit().getImage("resources/img/backgrounds/blue_panel.png"), 0, 0, this);
		
	}
}
