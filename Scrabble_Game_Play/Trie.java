package Scrabble_Game_Play;
import java.util.ArrayList;

public class Trie {
	private Node root;
	
	private class Node {
		char key;
		Node next[] = new Node[26];
		boolean leaf = false;
	} 
	
	public Trie() {
		root = new Node();
		root.key = 0;
	}
	
	// goes down the tree to return the Node that holds the given string
	// returns null if string does not exist or is not a leaf node
	public Node find(String str) {
		Node temp = root;
		int index = 0;
		//Node last = null;
		while (temp != null && index < str.length()) {
			char c = str.charAt(index);
			int i = (int) c - 97;
			temp = temp.next[i];
			index++;
		}
		
		if (temp != null && index == str.length() && temp.leaf) {
			return temp;
		} else {
			return null;
		}
	}
	
	// if a word exists in the Trie
	public boolean contains(String str) {
		return find(str) != null;
	}
	
	public void insert(String str) {
		int i = 0;
		int index = 0;
		Node n = root;
		while (i < str.length()) {
			index = ((int) str.charAt(i)) - 97;
			if (n.next[index] == null) {
				n.next[index] = new Node();
				n.next[index].key = str.charAt((i));
			}
			if ((i+1) == str.length()) {
				n.next[index].leaf = true;
			}
			i++;
			n = n.next[index];
		}
	}
	
	// testing purposes, prints the next[] array for the root node
	public void print() {
		for (int i = 0; i < 26; i++) {
			if (root.next[i] != null) {
				char c = (char) (i + 97);
				System.out.println(c);
			} else {
				System.out.println(-1);
			}
		}
	}
	
	// helper function for prefixOf()
	// fills in the given array list with strings that are children of str
	public void findPrefixes(Node n, ArrayList<String> p, String str) {
		if (n.leaf)
			p.add(str);
		for (int i = 0; i < 26; i++) {
			if (n.next[i] != null) {
				findPrefixes(n.next[i], p, str + ((char) (i + 97)));
			}
		}
	}	
	
	public ArrayList<String> prefixOf(String str) {
		Node temp = root;
		int index = 0;
		while (temp != null && index < str.length()) {
			char c = str.charAt(index);
			int i = (int) c - 97;
			temp = temp.next[i];
			index++;
		}
		
		if (temp == null || index != str.length()) {
			//System.out.println("Node is null");
			temp =  null;
		}
		
		ArrayList<String> prefixes = new ArrayList<String>();
		if (temp != null) {
			findPrefixes(temp, prefixes, str);
		}
		return prefixes;
	}
	
}	
